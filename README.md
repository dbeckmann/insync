Local image sync app
=================

Note:
-----

Please note this app is still under development and not yet finished.

About
-----

This app can be used to synchronize local device images to a network storage device or PCs through SMB protocol.
The idea is to have all new images automatically synchronized when the device is at his "home" wifi and the NSD is available.

It uses a clean architecture approach to create use cases that will be injected in the components that want to use them through Dagger2.