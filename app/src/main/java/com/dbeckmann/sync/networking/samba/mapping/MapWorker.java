package com.dbeckmann.sync.networking.samba.mapping;

import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by daniel on 24.08.2016.
 */
public class MapWorker implements Runnable {

    private String mIpAddress;
    private MapNetworkWorkerCallback mapNetworkWorkerCallback;

    private int mPort = 445;
    private int mTimeout = 1000;

    public MapWorker(String ipAdress, MapNetworkWorkerCallback callback) {
        mIpAddress = ipAdress;
        mapNetworkWorkerCallback = callback;
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(mIpAddress, mPort);
            socket.connect(inetSocketAddress, mTimeout);
            socket.close();

            mapNetworkWorkerCallback.onMapWorkerSuccess(mIpAddress);
        } catch (Exception e) {
            mapNetworkWorkerCallback.onMapWorkerError(mIpAddress, e);
            //e.printStackTrace();
        }
    }

    public interface MapNetworkWorkerCallback {
        void onMapWorkerSuccess(String ipAddress);
        void onMapWorkerError(String ipAddress, Exception e);
    }
}
