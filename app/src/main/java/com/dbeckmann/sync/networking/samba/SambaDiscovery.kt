package com.dbeckmann.sync.networking.samba

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.DiscoveryRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.networking.samba.mapping.MapNetwork
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.schedulers.Schedulers
import jcifs.smb.NtlmPasswordAuthentication
import jcifs.smb.SmbFile
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by daniel on 22.04.2018.
 */
@Singleton
class SambaDiscovery @Inject constructor(
        private val configuration: Configuration
) : DiscoveryRepository {

    private var mapNetwork: Observable<List<Host>>? = null

    companion object {
        private const val TAG = "SambaRepository"
    }

    override fun mapNetwork(ipRange: String): Executor<List<Host>> {
        return Executor { callback ->
            if (mapNetwork == null) {
                mapNetwork = Observable.create(ObservableOnSubscribe<List<Host>> {
                    val mapNetwork = MapNetwork()
                    val hosts = mapNetwork.map(ipRange)

                    it.onNext(hosts)
                })
                        .subscribeOn(Schedulers.io())
                        .cache()
            }

            mapNetwork?.subscribe(
                    {
                        callback.onSuccess(it)
                        mapNetwork = null
                    },
                    {
                        Logger.d(TAG, "error mapping network $it")
                        callback.onError(it)
                    }
            )
        }
    }

    override fun mapNetwork(): Executor<List<Host>> {
        val ip = configuration.getCurrentIp()
        return mapNetwork(ip.substring(0, ip.lastIndexOf(".")).trim())
    }

    override fun resolveHosts(storages: List<StorageEntity>): Executor<List<Host>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun authenticate(storage: StorageEntity): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun authenticate(host: Host, username: String, auth: String?): Executor<Unit> {
        return Executor { callback ->
            try {
                val passwordAuth = NtlmPasswordAuthentication(null, username, auth)
                val share = SmbFile("smb://" + host.ipAddress, passwordAuth)
                share.listFiles()

                callback.onSuccess(Unit)
            } catch (e: Throwable) {
                callback.onError(e)
            }
        }
    }
}