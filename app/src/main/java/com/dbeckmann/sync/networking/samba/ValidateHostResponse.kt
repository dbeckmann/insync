package com.dbeckmann.sync.networking.samba

/**
 * Created by daniel on 24.07.2017.
 */

class ValidateHostResponse(val hostname: String, val ipAddress: String)
