package com.dbeckmann.sync.networking.samba.mapping.response;

/**
 * Created by daniel on 28.08.2016.
 */
public class ResolveNameResponse {
    private String mHostName;
    private String mMacAddress;
    private boolean mNeedsAuth = true;
    private boolean mIsReachable = true;

    public ResolveNameResponse(String hostName, String macAddress, boolean needsAuth, boolean isReachable) {
        mHostName = hostName;
        mMacAddress = macAddress;
        mNeedsAuth = needsAuth;
        mIsReachable = isReachable;
    }

    public String getHostName() {
        return mHostName;
    }

    public String getMacAddress() {
        return mMacAddress;
    }

    public boolean isNeedsAuth() {
        return mNeedsAuth;
    }

    public boolean isReachable() {
        return mIsReachable;
    }
}
