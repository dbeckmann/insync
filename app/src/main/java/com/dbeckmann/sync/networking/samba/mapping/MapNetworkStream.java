package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.sync.networking.samba.mapping.response.ResolveNameResponse;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by daniel on 24.08.2016.
 */
public class MapNetworkStream implements MapResolveWorker.MapResolveWorkerCallback {
    private int mMaxWorker = 20;
    private ThreadPoolExecutor mExecutor;
    private CountDownLatch mCountDownLatch;

    private int mNextOctet = 1;
    private int mLastOctet = 254;
    private String mIpRange;

    private ArrayList<MappedHost> mMappedHosts = new ArrayList<>();

    private MapNetworkStreamStatusCallback mMapNetworkStatusCallback;

    public ArrayList<MappedHost> map(String ipRange) {
        mIpRange = ipRange;

        mCountDownLatch = new CountDownLatch(mLastOctet);
        mExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(mMaxWorker);

        System.out.println("------ start scan ------");

        for (int i=0; i <= mMaxWorker; i++) {
            addWorker();
        }

        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("------ finished scan ------");

        if (mMapNetworkStatusCallback != null) {
            mMapNetworkStatusCallback.onCompleted();
        }

        return mMappedHosts;
    }

    synchronized private void updateHostList(String ipAddress, ResolveNameResponse response) {
        MappedHost mappedHost = new MappedHost(ipAddress, response.getHostName(), response.isNeedsAuth(), true);
        mMappedHosts.add(mappedHost);

        if (mMapNetworkStatusCallback != null) {
            mMapNetworkStatusCallback.onHostFound(mappedHost);
        }
    }

    synchronized private void addWorker() {
        if (mNextOctet <= mLastOctet) {
            String ipAddress = mIpRange + "." + mNextOctet;
            mExecutor.execute(new MapResolveWorker(ipAddress, this));
            mNextOctet++;
        }
    }

    private void workerFinished() {
        mCountDownLatch.countDown();
        addWorker();

        if (mNextOctet == (mLastOctet + 1) && !mExecutor.isShutdown()) {
            System.out.println("shutdown executor");
            mExecutor.shutdown();
        }
    }

    @Override
    public void onMapResolveWorkerSuccess(String ipAddress, ResolveNameResponse response) {
        System.out.println("scan " + ipAddress + " - SMB port found !");
        updateHostList(ipAddress, response);
        workerFinished();
    }

    @Override
    public void onMapResolveWorkerError(String ipAddress, Exception e) {
        System.out.println("scan " + ipAddress + " - nothing found");
        workerFinished();
    }

    public void setMapNetworkStatusCallback(MapNetworkStreamStatusCallback callback) {
        mMapNetworkStatusCallback = callback;
    }

    public interface MapNetworkStreamStatusCallback {
        void onCompleted();
        void onHostFound(MappedHost mappedHost);
    }
}
