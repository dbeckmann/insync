package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.sync.data.greendao.Storage;
import com.dbeckmann.sync.networking.samba.mapping.response.HostReachableResponse;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by daniel on 06.09.2016.
 */
public class HostReachable implements HostReachebleWorker.HostReachableWorkerCallback {
    private int mMaxWorker = 5;
    private ThreadPoolExecutor mExecutor;
    private CountDownLatch mCountDownLatch;

    private int mNextEntry = 0;
    private ArrayList<MappedHost> mMappedHosts = new ArrayList<>();
    private ArrayList<Storage> mStorages;


    public ArrayList<MappedHost> resolve(ArrayList<Storage> storages) {
        mStorages = storages;

        mCountDownLatch = new CountDownLatch(mStorages.size());
        mExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(mMaxWorker);

        System.out.println("------ start reachable ------");

        for (int i=0; i <= mMaxWorker; i++) {
            if (i < mStorages.size()) {
                addWorker();
            } else {
                break;
            }
        }

        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("------ finished reachable ------");

        System.out.println("size: " + mMappedHosts.size());

        return mMappedHosts;
    }

    synchronized private void updateHostList(String hostName, String ipAddress, boolean needsAuth, boolean isReachable) {
        System.out.println("grrr " + hostName + " " + ipAddress + " " + needsAuth + " " + isReachable);

        mMappedHosts.add(new MappedHost(ipAddress, hostName, needsAuth, isReachable));
    }

    synchronized private void addWorker() {
        if (mNextEntry < mStorages.size()) {
            System.out.println("addWorker: " + mStorages.get(mNextEntry));
            mExecutor.execute(new HostReachebleWorker(mStorages.get(mNextEntry), this));
            mNextEntry++;
        }
    }

    private void workerFinished() {
        mCountDownLatch.countDown();
        addWorker();

        if (mNextEntry == mStorages.size() && !mExecutor.isShutdown()) {
            System.out.println("shutdown executor");
            mExecutor.shutdown();
        }
    }

    @Override
    public void onHostReachableSuccess(String hostname, HostReachableResponse response) {
        System.out.println("onHostReachableSuccess " + hostname);
        updateHostList(response.getHostName(), response.getIpAddress(), response.isNeedsAuth(), response.isReachable());
        workerFinished();
    }

    @Override
    public void onHostReachableError(String hostname, Exception e) {
        workerFinished();
    }
}
