package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.domain.model.Host;

/**
 * Created by daniel on 24.08.2016.
 */
public class MappedHost extends Host {

    public MappedHost(String ipAddress, String hostName, boolean needsAuth, boolean isReachable) {
        super(ipAddress, hostName, null, needsAuth, isReachable, null, null);
    }

    public Boolean isNeedsAuth() {
        return isSecured();
    }
}
