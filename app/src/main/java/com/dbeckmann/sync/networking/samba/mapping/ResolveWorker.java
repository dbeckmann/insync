package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.sync.networking.samba.mapping.response.ResolveNameResponse;

import java.net.InetAddress;
import java.util.Arrays;

import jcifs.netbios.NbtAddress;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbFile;

/**
 * Created by daniel on 24.08.2016.
 */
public class ResolveWorker implements Runnable {

    private static final byte[] EMPTY_MAC = {0,0,0,0,0,0};

    private String mIpAddress;
    private ResolveWorkerCallback mResolveWorkerCallback;

    public ResolveWorker(String mIpAdress, ResolveWorkerCallback mResolveWorkerCallback) {
        this.mIpAddress = mIpAdress;
        this.mResolveWorkerCallback = mResolveWorkerCallback;
    }

    @Override
    public void run() {
        // first resolve via NbtAddress resolve
        String hostName = getHostNameViaNbtAddress(mIpAddress);

        // if not successful try InetAddress to resolve
        if (hostName.equals(mIpAddress)) {
            hostName = getHostNameViaInetAddress(mIpAddress);
        }

        // resolve mac address, if any
        String macAddress = getMacAddressViaNbtAddress(mIpAddress);

        // check if host needs authentication
        Boolean needsAuth = false;
        Boolean isReachable = true;

        try {
            SmbFile[] files = (new SmbFile("smb://" + mIpAddress).listFiles());
        } catch (Exception e) {
            e.printStackTrace();

            if (e instanceof SmbAuthException) {
                needsAuth = true;
            } else {
                needsAuth = true;
                isReachable = false;
            }
        }

        mResolveWorkerCallback.onResolveWorkerSuccess(mIpAddress, new ResolveNameResponse(hostName, macAddress, needsAuth, isReachable));
    }

    private String getHostNameViaInetAddress(String ipAddress) {
        try {
            InetAddress inetAddress = NbtAddress.getByName(ipAddress).getInetAddress();
            return inetAddress.getHostName();
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return ipAddress;
    }

    private String getHostNameViaNbtAddress(String ipAddress) {
        String hostname = ipAddress;
        try {
            NbtAddress[] nbtAddresses = NbtAddress.getAllByAddress(ipAddress);

            for (NbtAddress nbtAddress : nbtAddresses) {
                if (!nbtAddress.isGroupAddress()) {
                    hostname = nbtAddress.getHostName();
                    break;
                }
            }

            return hostname;
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return hostname;
    }

    private String getMacAddressViaNbtAddress(String ipAddress) {
        String macAddress = null;
        try {
            NbtAddress[] nbtAddresses = NbtAddress.getAllByAddress(ipAddress);

            for (NbtAddress nbtAddress : nbtAddresses) {
                if (!nbtAddress.isGroupAddress()) {
                    byte[] mac = nbtAddress.getMacAddress();
                    if (mac != null && mac.length > 0 && !Arrays.equals(mac, EMPTY_MAC)) {
                        macAddress = byteToHex(mac);
                    }
                    break;
                }
            }

            return macAddress;
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return macAddress;
    }

    public interface ResolveWorkerCallback {
        void onResolveWorkerSuccess(String ipAddress, ResolveNameResponse response);
        void onResolveWorkerError(String ipAddress, Exception e);
    }

    private String byteToHex(byte[] bytes) {
        if (bytes != null) {
            StringBuilder sb = new StringBuilder(18);
            for (byte b : bytes) {
                if (sb.length() > 0)
                    sb.append(':');
                sb.append(String.format("%02x", b));
            }

            return sb.toString();
        } else {
            return null;
        }
    }
}
