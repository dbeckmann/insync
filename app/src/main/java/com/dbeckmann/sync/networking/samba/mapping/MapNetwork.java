package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.domain.model.Host;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by daniel on 24.08.2016.
 */
public class MapNetwork implements MapWorker.MapNetworkWorkerCallback {
    private int mMaxWorker = 20;
    private ThreadPoolExecutor mExecutor;
    private CountDownLatch mCountDownLatch;

    private int mNextOctet = 1;
    private int mLastOctet = 254;
    private String mIpRange;

    private ArrayList<String> mHostList = new ArrayList<String>();

    private MapNetworkStatusCallback mMapNetworkStatusCallback;

    public ArrayList<Host> map(String ipRange) {
        mIpRange = ipRange;

        mCountDownLatch = new CountDownLatch(mLastOctet);
        mExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(mMaxWorker);

        System.out.println("------ start scan ------");

        for (int i=0; i <= mMaxWorker; i++) {
            addWorker();
        }

        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("------ finished scan ------");

        ArrayList<Host> mappedHostArrayList;
        ResolveHostNames resolveHostNames = new ResolveHostNames();
        mappedHostArrayList = resolveHostNames.resolve(mHostList);

        return mappedHostArrayList;
    }

    synchronized private void updateHostList(String ipAddress) {
        mHostList.add(ipAddress);
    }

    synchronized private void addWorker() {
        if (mNextOctet <= mLastOctet) {
            String ipAddress = mIpRange + "." + mNextOctet;
            mExecutor.execute(new MapWorker(ipAddress, this));
            mNextOctet++;
        }
    }

    private void workerFinished() {
        mCountDownLatch.countDown();
        addWorker();

        if (mNextOctet == (mLastOctet + 1) && !mExecutor.isShutdown()) {
            System.out.println("shutdown executor");
            mExecutor.shutdown();
        }

        if (mMapNetworkStatusCallback != null) {
            mMapNetworkStatusCallback.onSingleWorkerFinished(mLastOctet, (mLastOctet - (int) mCountDownLatch.getCount()));
        }
    }

    @Override
    public void onMapWorkerSuccess(String ipAddress) {
        System.out.println("scan " + ipAddress + " - SMB port found !");
        updateHostList(ipAddress);
        workerFinished();
    }

    @Override
    public void onMapWorkerError(String ipAddress, Exception e) {
        System.out.println("scan " + ipAddress + " - nothing found");
        workerFinished();
    }

    public void setMapNetworkStatusCallback(MapNetworkStatusCallback callback) {
        mMapNetworkStatusCallback = callback;
    }

    public interface MapNetworkStatusCallback {
        void onFinished();
        void onSingleWorkerFinished(int all, int processed);
    }
}
