package com.dbeckmann.sync.networking.samba.mapping.response;

/**
 * Created by daniel on 25.08.2016.
 */
public class MapNetworkStateResponse {
    private int mNumAll;
    private int mProcessed;

    public MapNetworkStateResponse(int numAll, int processed) {
        mNumAll = numAll;
        mProcessed = processed;
    }

    public int getNumAll() {
        return mNumAll;
    }

    public int getProcessed() {
        return mProcessed;
    }
}
