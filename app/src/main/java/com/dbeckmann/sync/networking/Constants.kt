package com.dbeckmann.sync.networking

/**
 * Created by daniel on 30.04.2018.
 */
class Constants {
    companion object {
        const val REMOTE_CONFIG_FILE_NAME = "remote_config.cfg"
        const val REMOTE_BACKUP_FOLDER_NAME = "WIPandroidBackup"
    }
}