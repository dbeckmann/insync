package com.dbeckmann.sync.networking.samba.mapping.response;

/**
 * Created by daniel on 06.09.2016.
 */
public class HostReachableResponse {
    private String mHostName;
    private String mIpAddress;
    private boolean mNeedsAuth = true;
    private boolean mIsReachable = false;

    public HostReachableResponse(String hostName, String ipAddress, boolean isReachable, boolean needsAuth) {
        mHostName = hostName;
        mIpAddress = ipAddress;
        mIsReachable = isReachable;
        mNeedsAuth = needsAuth;
    }

    public String getHostName() {
        return mHostName;
    }

    public String getIpAddress() {
        return mIpAddress;
    }

    public boolean isReachable() {
        return mIsReachable;
    }

    public boolean isNeedsAuth() {
        return mNeedsAuth;
    }
}
