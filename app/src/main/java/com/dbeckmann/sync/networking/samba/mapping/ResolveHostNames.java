package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.domain.model.Host;
import com.dbeckmann.sync.networking.samba.mapping.response.ResolveNameResponse;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by daniel on 24.08.2016.
 */
public class ResolveHostNames implements ResolveWorker.ResolveWorkerCallback {

    private int mMaxWorker = 5;
    private ThreadPoolExecutor mExecutor;
    private CountDownLatch mCountDownLatch;

    private int mNextEntry = 0;
    private ArrayList<String> mIpAddressList = new ArrayList<String>();

    private ArrayList<Host> mappedHostArrayList = new ArrayList();

    public ArrayList<Host> resolve(ArrayList<String> ipAddressList) {
        mIpAddressList = ipAddressList;

        mCountDownLatch = new CountDownLatch(mIpAddressList.size());
        mExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(mMaxWorker);

        System.out.println("------ start resolve ------");

        for (int i=0; i <= mMaxWorker; i++) {
            if (i < mIpAddressList.size()) {
                addWorker();
            } else {
                break;
            }
        }

        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("------ finished resolve ------");

        return mappedHostArrayList;
    }

    synchronized private void updateHostList(String ipAddress, String hostName, String macAddress, boolean needsAuth, boolean isReachable) {
        mappedHostArrayList.add(new Host(ipAddress, hostName, macAddress, isReachable, needsAuth, null, null));
    }

    synchronized private void addWorker() {
        if (mNextEntry < mIpAddressList.size()) {
            mExecutor.execute(new ResolveWorker(mIpAddressList.get(mNextEntry), this));
            mNextEntry++;
        }
    }

    private void workerFinished() {
        mCountDownLatch.countDown();
        addWorker();

        if (mNextEntry == mIpAddressList.size() && !mExecutor.isShutdown()) {
            System.out.println("shutdown executor");
            mExecutor.shutdown();
        }
    }

    @Override
    public void onResolveWorkerSuccess(String ipAddress, ResolveNameResponse response) {
        System.out.println("ip: " + ipAddress + " hostname: " + response.getHostName());
        updateHostList(ipAddress, response.getHostName(), response.getMacAddress(), response.isNeedsAuth(), response.isReachable());
        workerFinished();
    }

    @Override
    public void onResolveWorkerError(String ipAddress, Exception e) {
        System.out.println("ip: " + ipAddress + " no host name resolved");
        workerFinished();
    }
}
