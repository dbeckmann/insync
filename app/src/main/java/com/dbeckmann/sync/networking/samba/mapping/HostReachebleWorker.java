package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.sync.data.crypto.CryptoManager;
import com.dbeckmann.sync.data.greendao.Storage;
import com.dbeckmann.sync.networking.samba.mapping.response.HostReachableResponse;

import java.net.UnknownHostException;

import jcifs.UniAddress;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbFile;

/**
 * Created by daniel on 06.09.2016.
 */
public class HostReachebleWorker implements Runnable {
    private Storage mStorage;
    private HostReachableWorkerCallback mHostReachableWorkerCallback;

    public HostReachebleWorker(Storage storage, HostReachableWorkerCallback mResolveWorkerCallback) {
        this.mStorage = storage;
        this.mHostReachableWorkerCallback = mResolveWorkerCallback;
    }

    @Override
    public void run() {
        boolean isReachable = true;
        boolean needsAuth = false;
        String ipAddress = null;

        try {
            UniAddress uniAddress = UniAddress.getByName(mStorage.getHostName());
            ipAddress = uniAddress.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            isReachable = false;
        }

        if (isReachable) {
            // check if host needs authentication
            try {
                if (mStorage.getUsername() == null || mStorage.getUsername().isEmpty()) {
                    SmbFile[] files = (new SmbFile("smb://" + mStorage.getHostName()).listFiles());
                } else {
                    String username = CryptoManager.getInstance().decrypt(mStorage.getUsername());
                    String password = CryptoManager.getInstance().decrypt(mStorage.getPassword());

                    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);

                    SmbFile[] files = (new SmbFile("smb://" + mStorage.getHostName(), auth).listFiles());
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (e instanceof SmbAuthException) {
                    needsAuth = true;
                }
            }
        }

        mHostReachableWorkerCallback.onHostReachableSuccess(mStorage.getHostName(), new HostReachableResponse(mStorage.getHostName(), ipAddress, isReachable, needsAuth));
    }


    public interface HostReachableWorkerCallback {
        void onHostReachableSuccess(String hostname, HostReachableResponse response);
        void onHostReachableError(String hostname, Exception e);
    }

}
