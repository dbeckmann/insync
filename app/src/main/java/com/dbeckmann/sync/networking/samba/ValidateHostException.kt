package com.dbeckmann.sync.networking.samba

/**
 * Created by daniel on 24.07.2017.
 */

class ValidateHostException(message: String) : Exception(message)
