package com.dbeckmann.sync.networking.samba.mapping.response;

import com.dbeckmann.sync.networking.samba.mapping.MappedHost;

import java.util.ArrayList;

/**
 * Created by daniel on 06.09.2016.
 */
public class ResolveHostResponse {
    private ArrayList<MappedHost> mMappedHosts;

    public ResolveHostResponse(ArrayList<MappedHost> mappedHosts) {
        mMappedHosts = mappedHosts;
    }

    public ArrayList<MappedHost> getMappedHosts() {
        return mMappedHosts;
    }
}
