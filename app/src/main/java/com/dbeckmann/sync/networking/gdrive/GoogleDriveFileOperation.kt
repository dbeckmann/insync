package com.dbeckmann.sync.networking.gdrive

import android.content.Context
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.domain.repository.FileOperationRepository
import com.dbeckmann.interactor.usecase.Executor
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.drive.Drive
import com.google.android.gms.drive.DriveResourceClient
import java.io.File

class GoogleDriveFileOperation(
        context: Context)
    : FileOperationRepository {

    private var signInAccount: GoogleSignInAccount? = null
    private var driveResourceClient: DriveResourceClient? = null

    companion object {
        private const val TAG = "GoogleDriveFileOperation"
        const val IDENTIFIER = "GDRIVE"
    }

    init {
        signInAccount = GoogleSignIn.getLastSignedInAccount(context)
        signInAccount?.also {
            driveResourceClient = Drive.getDriveResourceClient(context, it)
        }
    }

    override fun getRootPath(): Path {
        return Path(
                name = "/",
                fullpath = "/",
                isDirectory = true,
                isWritable = false
        )
    }

    override fun getIdentifier(): String {
        return IDENTIFIER
    }

    override fun getStorageSpace(path: Path): Executor<StorageSpace> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getStorageSpace(storage: StorageEntity): Executor<StorageSpace> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun listPath(path: Path): Executor<List<Path>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createFolder(targetPath: Path, folderName: String): Executor<Path> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun writeConfigFile(path: Path, data: ByteArray): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun readConfigFile(path: Path): Executor<ByteArray> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasNoConfigFile(path: Path): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun copyFileToRemote(targetPath: Path, file: File): Executor<Path> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isAvailable(): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}