package com.dbeckmann.sync.networking.samba

import com.dbeckmann.sync.data.Logger
import jcifs.UniAddress
import jcifs.netbios.NbtAddress
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

/**
 * Created by daniel on 24.07.2017.
 */

class ValidateHost(
        private var ipAddress: String,
        private var hostName: String) {

    companion object {
        private const val TAG = "ValidateHost"
        private const val PORT = 445
        private const val TIMEOUT = 1000
    }

    @Throws(ValidateHostException::class)
    fun validate(): ValidateHostResponse {
        Logger.d(TAG,"validate host $hostName at $ipAddress")

        val isReachable = isSambaReachable(ipAddress)
        val resolvedHostname: String

        if (isReachable) {
            resolvedHostname = resolveHostName()

            if (resolvedHostname == hostName) {
                return ValidateHostResponse(resolvedHostname, ipAddress)
            }
        }

        // try to resolve ip by hostname
        val ipAddress: String? = getIpAddress()

        // check if samba port is open on resolved ip
        ipAddress?.let {
            if (isSambaReachable(it)) {
                return ValidateHostResponse(hostName, it)
            }
        }

        throw ValidateHostException("no valid host found: $hostName ip: ${this.ipAddress}")
    }

    private fun isSambaReachable(ipAddress: String): Boolean {
        return try {
            val socket = Socket()
            val inetSocketAddress = InetSocketAddress(ipAddress, PORT)
            socket.connect(inetSocketAddress, TIMEOUT)
            socket.close()
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    private fun resolveHostName(): String {
        var hostname: String

        hostname = getHostNameViaNbtAddress(ipAddress)

        if (hostname == ipAddress) {
            hostname = getHostNameViaInetAddress(ipAddress)
        }

        if (hostname == ipAddress) {
            Logger.d(TAG,"could not resolve a hostname. will use ip address as fallback")
        }

        return hostname
    }

    private fun getIpAddress(): String? {
        // lookup through standard implementation (should be DNS)
        var ipAddress = getIpAddressViaInetAddress(this.hostName)

        // lookup with jcifs implementation: resolve hostnames on LANs and WANs that support a mixture of NetBIOS/WINS and DNS resolvable hosts.
        // this should give best results
        if (ipAddress == null) {
            ipAddress = getIpAddressViaUniAddress(this.hostName)
        }

        // jcifs NetBIOS name resolve implementation, can give weird results
        // my VirtualBox LAN adapter with NetBIOS enabled will always come as only name lookup result, which is wrong
        if (ipAddress == null) {
            ipAddress = getIpAddressViaNbtAddress(this.hostName)
        }

        // the host name could not be resolved
        return if (ipAddress == null) {
            null
        } else {
            ipAddress
        }
    }

    private fun getIpAddressViaInetAddress(hostName: String): String? {
        try {
            val inetAddress = InetAddress.getByName(hostName)
            return inetAddress.hostAddress
        } catch (e: UnknownHostException) {
            Logger.d(TAG,"ip via InetAddress not resolved")
            return null
        }

    }

    private fun getIpAddressViaUniAddress(hostName: String): String? {
        try {
            val uniAddress = UniAddress.getByName(hostName, true)
            return uniAddress.hostAddress
        } catch (e: UnknownHostException) {
            Logger.d(TAG, "ip via UniAddress not resolved")
            return null
        }

    }

    private fun getIpAddressViaNbtAddress(hostName: String): String? {
        try {
            val nbtAddress = NbtAddress.getByName(hostName)
            return nbtAddress.hostAddress
        } catch (e: UnknownHostException) {
            Logger.d(TAG,"ip via NbtAddress not resolved")
            return null
        }

    }

    private fun getHostNameViaInetAddress(ipAddress: String): String {
        try {
            val inetAddress = NbtAddress.getByName(ipAddress).inetAddress
            return inetAddress.hostName
        } catch (e: Exception) {
            Logger.d(TAG,"hostname via InetAddress not resolved")
        }

        return ipAddress
    }

    private fun getHostNameViaNbtAddress(ipAddress: String): String {
        var hostname = ipAddress
        try {
            val nbtAddresses = NbtAddress.getAllByAddress(ipAddress)

            for (nbtAddress in nbtAddresses) {
                if (!nbtAddress.isGroupAddress) {
                    hostname = nbtAddress.hostName
                    break
                }
            }

            return hostname
        } catch (e: Exception) {
            Logger.d(TAG,"hostname via NbtAddress not resolved")
        }

        return hostname
    }
}
