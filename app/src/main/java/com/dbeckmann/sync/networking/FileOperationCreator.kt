package com.dbeckmann.sync.networking

import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.FileOperationRepository
import com.dbeckmann.sync.networking.samba.SambaFileOperation

/**
 * Created by daniel on 01.05.2018.
 */
class FileOperationCreator : StorageFileOperationFactory {

    private val objectCache: HashMap<StorageEntity, FileOperationRepository> = hashMapOf()

    override fun fromStorage(storage: StorageEntity): FileOperationRepository {
        return when (storage.driverIdentifier) {
            SambaFileOperation.IDENTIFIER -> {
                getCached(storage) ?: addToCache(storage, SambaFileOperation(storage, SambaFileOperation.fromCredentials(storage.username, storage.auth)))
            }
            else -> throw IllegalArgumentException("storage identifier not found")
        }
    }

    private fun getCached(storage: StorageEntity) : FileOperationRepository? {
        return try {
            objectCache.keys
                    .first { it.id == storage.id }
                    .let {
                        objectCache[it]
                    }
        } catch (e: Exception) {
            null
        }
    }

    private fun addToCache(storage: StorageEntity, repository: FileOperationRepository) : FileOperationRepository {
        objectCache[storage] = repository
        return repository
    }
}