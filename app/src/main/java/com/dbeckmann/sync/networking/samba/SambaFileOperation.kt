package com.dbeckmann.sync.networking.samba

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.domain.repository.FileOperationRepository
import com.dbeckmann.domain.util.PathUtil
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.networking.Constants
import jcifs.smb.NtlmPasswordAuthentication
import jcifs.smb.SmbFile
import java.io.File
import java.io.FileNotFoundException

/**
 * Created by daniel on 30.04.2018.
 */
class SambaFileOperation(
        private val storage: StorageEntity,
        private val passwordAuthentication: NtlmPasswordAuthentication?
) : FileOperationRepository {

    private var connectIpAddress: String = storage.hostIpAddress
    private var hostName: String = storage.host
    private var isHostValidated: Boolean = false

    companion object {
        private const val TAG = "SambaFileOperation"
        const val IDENTIFIER = "SAMBA"

        fun fromCredentials(username: String?, password: String?): NtlmPasswordAuthentication? {

            return if (username == null) {
                null
            } else NtlmPasswordAuthentication(null,
                    username,
                    password
            )
        }
    }

    override fun getRootPath(): Path {
        return Path(
                name = "/",
                fullpath = "/",
                isDirectory = true,
                isWritable = false
                )
    }

    override fun getIdentifier(): String {
        return IDENTIFIER
    }

    override fun getStorageSpace(path: Path): Executor<StorageSpace> = Executor { callback ->
        try {
            val pathFile = createSmbFile(path)

            // we need the share to get max disk value
            val sharePath = Path(
                    name = "/" + pathFile.share,
                    fullpath = "/" + pathFile.share,
                    isDirectory = true,
                    isSecured = true,
                    isWritable = false
            )

            val shareRoot = createSmbFile(sharePath)
            callback.onSuccess(StorageSpace(shareRoot.diskFreeSpace, shareRoot.length()))
        } catch (e: Throwable) {
            callback.onError(e)
        }
    }

    override fun getStorageSpace(storage: StorageEntity): Executor<StorageSpace> {
        return getStorageSpace(PathUtil.rootFolderToPath(storage, getRootPath()))
    }

    override fun listPath(path: Path): Executor<List<Path>> {
        return object : Executor<List<Path>> {
            override fun execute(callback: ExecutorCallback<List<Path>>) {
                try {
                    val target = createSmbFile(path)
                    val files: Array<SmbFile> = target.listFiles()

                    val pathFiles: MutableList<Path> = mutableListOf()

                    files.filter {
                        it.isDirectory &&
                                !it.name.endsWith("$/") &&
                                !it.isHidden &&
                                !it.name.startsWith(".") &&
                                it.name != "Default.migrated/"
                    }.forEach {
                        val newPath = Path(
                                name = it.name.replace("/", ""),
                                fullpath = cleanCanonicalPath(it.canonicalPath),
                                parentPath = path,
                                isDirectory = it.isDirectory,
                                isSecured = isSecured(it),
                                isWritable = isWritable(it),
                                isReadable = it.canRead(),
                                size = it.length(),
                                modifiedAt = it.lastModified(),
                                createdAt = it.createTime()
                        )

                        pathFiles.add(newPath)
                    }

                    callback.onSuccess(pathFiles)
                } catch (e: Throwable) {
                    e.printStackTrace()
                    callback.onError(e)
                }
            }
        }
    }

    override fun createFolder(targetPath: Path, folderName: String): Executor<Path> {
        return object : Executor<Path> {
            override fun execute(callback: ExecutorCallback<Path>) {
                try {
                    callback.onSuccess(mkdirs(targetPath, folderName))
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    private fun mkdirs(targetPath: Path, folderName: String) : Path {
        if (folderName.endsWith(".")) {
            throw IllegalArgumentException("folder can not end with .")
        }

        val smbFolderFile = createSmbFile(getSmbFullPath(targetPath) + folderName)

        if (!smbFolderFile.exists()) {
            smbFolderFile.mkdirs()
        }

        return Path(
                name = smbFolderFile.name,
                fullpath = cleanCanonicalPath(smbFolderFile.canonicalPath),
                parentPath = targetPath,
                isDirectory = true,
                isSecured = isSecured(smbFolderFile),
                isWritable = smbFolderFile.canWrite(),
                isReadable = smbFolderFile.canRead(),
                size = smbFolderFile.length(),
                modifiedAt = smbFolderFile.lastModified(),
                createdAt = smbFolderFile.createTime()
        )
    }

    override fun copyFileToRemote(targetPath: Path, file: File): Executor<Path> {
        return object : Executor<Path> {
            override fun execute(callback: ExecutorCallback<Path>) {
                try {
                    val updatedPath = Path(
                            name = targetPath.name,
                            fullpath = targetPath.fullpath + Constants.REMOTE_BACKUP_FOLDER_NAME + "/"
                    )

                    val outSmbFile = createSmbFile(getSmbFullPath(updatedPath) + getLocalPathFromFullpath(file.absolutePath) + file.name)

                    mkdirs(updatedPath, getLocalPathFromFullpath(file.absolutePath))

                    file.inputStream().use {input ->
                        outSmbFile.outputStream.use {out ->
                            //input.copyTo(it)
                            var bytesCopied: Long = 0
                            val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
                            var bytes = input.read(buffer)

                            while (bytes >= 0) {
                                out.write(buffer, 0, bytes)
                                bytesCopied += bytes
                                bytes = input.read(buffer)

                                println("bytes copied: $bytesCopied")
                            }
                        }
                    }

                    outSmbFile.lastModified = file.lastModified()

                    val newFilePath = Path(
                            name = outSmbFile.name,
                            fullpath = cleanCanonicalPath(outSmbFile.canonicalPath),
                            parentPath = targetPath,
                            isDirectory = false,
                            isSecured = isSecured(outSmbFile),
                            isWritable = outSmbFile.canWrite(),
                            isReadable = outSmbFile.canRead(),
                            size = outSmbFile.length(),
                            modifiedAt = outSmbFile.lastModified(),
                            createdAt = outSmbFile.createTime()
                    )

                    callback.onSuccess(newFilePath)
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    override fun writeConfigFile(path: Path, data: ByteArray): Executor<Unit> {
        return object : Executor<Unit> {
            override fun execute(callback: ExecutorCallback<Unit>) {
                try {
                    val configFile = createSmbFile(createConfigPath(path))

                    data.inputStream().use { input ->
                        configFile.outputStream.use { out ->
                            input.copyTo(out)
                        }
                    }

                    callback.onSuccess(Unit)
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    override fun readConfigFile(path: Path): Executor<ByteArray> {
        return object : Executor<ByteArray> {
            override fun execute(callback: ExecutorCallback<ByteArray>) {
                try {
                    val configFile = createSmbFile(createConfigPath(path))

                    if (configFile.exists()) {
                        callback.onSuccess(configFile.inputStream.readBytes())
                    } else {
                        callback.onError(FileNotFoundException())
                    }
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    override fun hasNoConfigFile(path: Path): Executor<Unit> {
        return object : Executor<Unit> {
            override fun execute(callback: ExecutorCallback<Unit>) {
                try {
                    val configFile = createSmbFile(createConfigPath(path))

                    if (configFile.exists()) {
                        callback.onError(FileAlreadyExistsException(File("/")))
                    } else {
                        callback.onSuccess(Unit)
                    }
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    override fun isAvailable(): Executor<Unit> {
        return object : Executor<Unit> {
            override fun execute(callback: ExecutorCallback<Unit>) {
                try {
                    createSmbFile(getRootPath()).connect()
                    callback.onSuccess(Unit)
                } catch (e: Throwable) {
                    callback.onError(e)
                }
            }
        }
    }

    private fun createSmbFile(path: Path) : SmbFile {
        return if (passwordAuthentication != null) {
            SmbFile(getSmbFullPath(path), passwordAuthentication)
        } else {
            SmbFile(getSmbFullPath(path))
        }
    }

    private fun createSmbFile(smbUri: String) : SmbFile {
        return if (passwordAuthentication != null) {
            SmbFile(smbUri, passwordAuthentication)
        } else {
            SmbFile(smbUri)
        }
    }

    private fun getSmbFullPath(path: Path): String {
        validateHostAndUpdateConnection()
        return "smb://" + connectIpAddress + path.fullpath
    }

    private fun validateHostAndUpdateConnection() {
        if (!isHostValidated) {
            Logger.d(TAG, "need to validate host")
            val response = ValidateHost(connectIpAddress, hostName).validate()
            connectIpAddress = response.ipAddress
            hostName = response.hostname

            isHostValidated = true
        }
    }

    private fun cleanCanonicalPath(canonicalPath: String): String {
        return canonicalPath.replace("smb://$connectIpAddress", "")
    }

    private fun isSecured(file: SmbFile): Boolean {
        return try {
            file.security
            false
        } catch (e: Exception) {
            true
        }
    }

    private fun isWritable(file: SmbFile): Boolean {
        return file.share != file.name.replace("/", "")
    }

    private fun getLocalPathFromFullpath(fullPath: String): String {
        return fullPath.let {
            it.replace("/storage/emulated/0/", "").let {
                it.substring(0, it.lastIndexOf("/") + 1)
            }
        }
    }

    private fun createConfigPath(path: Path) : Path {
        return Path(
                name = Constants.REMOTE_CONFIG_FILE_NAME,
                fullpath = "${getRootPath().fullpath}/${path.fullpath}/${Constants.REMOTE_BACKUP_FOLDER_NAME}/${Constants.REMOTE_CONFIG_FILE_NAME}",
                parentPath = getRootPath(),
                isDirectory = false
        )
    }
}