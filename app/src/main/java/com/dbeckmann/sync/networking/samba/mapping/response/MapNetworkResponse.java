package com.dbeckmann.sync.networking.samba.mapping.response;

import com.dbeckmann.sync.networking.samba.mapping.MappedHost;

import java.util.ArrayList;

/**
 * Created by daniel on 25.08.2016.
 */
public class MapNetworkResponse {
    private ArrayList<MappedHost> mMappedHosts;

    public MapNetworkResponse(ArrayList<MappedHost> mappedHosts) {
        mMappedHosts = mappedHosts;
    }

    public ArrayList<MappedHost> getMappedHosts() {
        return mMappedHosts;
    }
}
