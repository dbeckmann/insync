package com.dbeckmann.sync.networking.samba.mapping;

import com.dbeckmann.sync.networking.samba.mapping.response.ResolveNameResponse;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import jcifs.netbios.NbtAddress;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbFile;

/**
 * Created by daniel on 22.07.2017.
 */

public class MapResolveWorker implements Runnable {

    private String mIpAddress;
    private MapResolveWorkerCallback mCallback;

    private int mPort = 445;
    private int mTimeout = 1000;

    public MapResolveWorker(String ipAdress, MapResolveWorkerCallback callback) {
        mIpAddress = ipAdress;
        mCallback = callback;
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(mIpAddress, mPort);
            socket.connect(inetSocketAddress, mTimeout);
            socket.close();

            // first resolve via NbtAddress resolve
            String hostName = getHostNameViaNbtAddress(mIpAddress);

            // if not successful try InetAddress to resolve
            if (hostName.equals(mIpAddress)) {
                hostName = getHostNameViaInetAddress(mIpAddress);
            }

            // check if host needs authentication
            boolean needsAuth = false;
            boolean isReachable = true;

            try {
                SmbFile[] files = (new SmbFile("smb://" + mIpAddress).listFiles());
                System.out.println(files.length);
            } catch (Exception e) {
                e.printStackTrace();

                if (e instanceof SmbAuthException) {
                    needsAuth = true;
                } else {
                    needsAuth = true;
                    isReachable = false;
                }
            }

            mCallback.onMapResolveWorkerSuccess(mIpAddress, new ResolveNameResponse(hostName, null, needsAuth, isReachable));
        } catch (Exception e) {
            mCallback.onMapResolveWorkerError(mIpAddress, e);
        }
    }

    private String getHostNameViaInetAddress(String ipAddress) {
        try {
            InetAddress inetAddress = NbtAddress.getByName(ipAddress).getInetAddress();
            return inetAddress.getHostName();
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return ipAddress;
    }

    private String getHostNameViaNbtAddress(String ipAddress) {
        String hostname = ipAddress;
        try {
            NbtAddress[] nbtAddresses = NbtAddress.getAllByAddress(ipAddress);

            for (NbtAddress nbtAddress : nbtAddresses) {
                if (!nbtAddress.isGroupAddress()) {
                    hostname = nbtAddress.getHostName();
                    break;
                }
            }

            return hostname;
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return hostname;
    }

    public interface MapResolveWorkerCallback {
        void onMapResolveWorkerSuccess(String ipAddress, ResolveNameResponse response);
        void onMapResolveWorkerError(String ipAddress, Exception e);
    }
}
