package com.dbeckmann.sync.data.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class Label {

    @Id(autoincrement = true)
    private Long id;

    @Index
    private String label;

    // relations
    @Index
    private long localFileId;
    @ToOne(joinProperty = "localFileId")
    private LocalFile localFile;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 40777009)
    private transient LabelDao myDao;
    @Generated(hash = 884424319)
    public Label(Long id, String label, long localFileId) {
        this.id = id;
        this.label = label;
        this.localFileId = localFileId;
    }
    @Generated(hash = 2137109701)
    public Label() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLabel() {
        return this.label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public long getLocalFileId() {
        return this.localFileId;
    }
    public void setLocalFileId(long localFileId) {
        this.localFileId = localFileId;
    }
    @Generated(hash = 120695424)
    private transient Long localFile__resolvedKey;
    /** To-one relationship, resolved on first access. */
    @Generated(hash = 290969208)
    public LocalFile getLocalFile() {
        long __key = this.localFileId;
        if (localFile__resolvedKey == null
                || !localFile__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            LocalFileDao targetDao = daoSession.getLocalFileDao();
            LocalFile localFileNew = targetDao.load(__key);
            synchronized (this) {
                localFile = localFileNew;
                localFile__resolvedKey = __key;
            }
        }
        return localFile;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 349612622)
    public void setLocalFile(@NotNull LocalFile localFile) {
        if (localFile == null) {
            throw new DaoException(
                    "To-one property 'localFileId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.localFile = localFile;
            localFileId = localFile.getId();
            localFile__resolvedKey = localFileId;
        }
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 692607636)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getLabelDao() : null;
    }
}
