package com.dbeckmann.sync.data.mapping

import android.os.Parcel
import android.os.Parcelable
import com.dbeckmann.domain.model.StorageEntity

/**
 * Created by daniel on 01.05.2018.
 */
class StorageParcelable (
        id: Long?,
        displayName: String,
        host: String,
        connectionAddress: String,
        hostIpAddress: String,
        hostMacAddress: String? = null,
        homeWifiSsid: String? = null,
        homeWifiBssid: String? = null,
        username: String? = null,
        auth: String? = null,
        rootFolder: String? = null,
        cacheMaxSpace: Long = 0,
        cacheUsedSpace: Long = 0,
        cacheUsedBackupSpace: Long = 0,
        lastBackupAt: Long = 0,
        isEnabled: Boolean = true,
        driverIdentifier: String
) : StorageEntity(
        id = id,
        displayName = displayName,
        host = host,
        connectionAddress = connectionAddress,
        hostIpAddress = hostIpAddress,
        hostMacAddress = hostMacAddress,
        homeWifiBssid = homeWifiBssid,
        homeWifiSsid = homeWifiSsid,
        username = username,
        auth = auth,
        rootFolder = rootFolder,
        cacheMaxSpace = cacheMaxSpace,
        cacheUsedSpace = cacheUsedSpace,
        cacheUsedBackupSpace = cacheUsedBackupSpace,
        lastBackupAt = lastBackupAt,
        isEnabled = isEnabled,
        driverIdentifier = driverIdentifier
), Parcelable {
    constructor(parcel: Parcel) : this(
            id = parcel.readLong(),
            displayName = parcel.readString(),
            host = parcel.readString(),
            connectionAddress = parcel.readString(),
            hostIpAddress = parcel.readString(),
            hostMacAddress = parcel.readString(),
            homeWifiSsid = parcel.readString(),
            homeWifiBssid = parcel.readString(),
            username = parcel.readString(),
            auth = parcel.readString(),
            rootFolder = parcel.readString(),
            cacheMaxSpace = parcel.readLong(),
            cacheUsedSpace = parcel.readLong(),
            cacheUsedBackupSpace = parcel.readLong(),
            lastBackupAt = parcel.readLong(),
            isEnabled = parcel.readInt() != 0,
            driverIdentifier = parcel.readString())

    companion object {
        fun fromStorage(storage: StorageEntity) : StorageParcelable {
            return StorageParcelable(
                    id = storage.id,
                    displayName = storage.displayName,
                    host = storage.host,
                    connectionAddress = storage.connectionAddress,
                    hostIpAddress = storage.hostIpAddress,
                    hostMacAddress = storage.hostMacAddress,
                    homeWifiBssid = storage.homeWifiBssid,
                    homeWifiSsid = storage.homeWifiSsid,
                    username = storage.username,
                    auth = storage.auth,
                    rootFolder = storage.rootFolder,
                    cacheMaxSpace = storage.cacheMaxSpace,
                    cacheUsedSpace = storage.cacheUsedSpace,
                    cacheUsedBackupSpace = storage.cacheUsedBackupSpace,
                    lastBackupAt = storage.lastBackupAt,
                    isEnabled = storage.isEnabled,
                    driverIdentifier = storage.driverIdentifier
                    )
        }

        @JvmField
        val CREATOR : Parcelable.Creator<StorageParcelable> = object : Parcelable.Creator<StorageParcelable> {
            override fun createFromParcel(parcel: Parcel): StorageParcelable {
                return StorageParcelable(parcel)
            }

            override fun newArray(size: Int): Array<StorageParcelable?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id ?: -1) // TODO fix this !
        parcel.writeString(displayName)
        parcel.writeString(host)
        parcel.writeString(connectionAddress)
        parcel.writeString(hostIpAddress)
        parcel.writeString(hostMacAddress)
        parcel.writeString(homeWifiSsid)
        parcel.writeString(homeWifiBssid)
        parcel.writeString(username)
        parcel.writeString(auth)
        parcel.writeString(rootFolder)
        parcel.writeLong(cacheMaxSpace)
        parcel.writeLong(cacheUsedSpace)
        parcel.writeLong(cacheUsedBackupSpace)
        parcel.writeLong(lastBackupAt)
        parcel.writeInt(if (isEnabled) 1 else 0)
        parcel.writeString(driverIdentifier)
    }

    override fun describeContents(): Int {
        return 0
    }
}
