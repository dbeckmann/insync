package com.dbeckmann.sync.data.backup.label

import android.content.Context
import android.net.Uri
import com.dbeckmann.sync.data.Logger
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions
import io.reactivex.Single

class MlKitProcessor(val context: Context) : ImageLabelProcessor() {

    companion object {
        private const val TAG = "MlKitProcessor"
    }

    private val labeler: FirebaseVisionImageLabeler

    init {
        val options = FirebaseVisionOnDeviceImageLabelerOptions.Builder().apply {
            setConfidenceThreshold(0.7f)
        }.build()

        labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler(options)
    }

    override fun process(): List<String>? {
        return try {
            processImageSingle().blockingGet()
        } catch (e: Exception) {
            null
        }
    }

    private fun processImageSingle(): Single<List<String>> {
        return Single.create { e ->
            val image = FirebaseVisionImage.fromFilePath(context, Uri.parse(filePath))
            val labels = mutableListOf<String>()

            labeler.processImage(image)
                    .addOnSuccessListener { result ->
                        result.forEach {
                            println("mlkit: ${it.confidence} -> ${it.text}")
                            labels.add(it.text)
                        }

                        e.onSuccess(labels)
                    }
                    .addOnFailureListener {
                        Logger.e(TAG, "mlkit failed with $it")
                        e.onError(it)
                    }
        }
    }
}