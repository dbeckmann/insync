package com.dbeckmann.sync.data.persistance;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daniel on 23.07.2017.
 */

public class RemoteConfigurationFile {

    public static final String KEY_DEVICE_MODEL = "device_model";
    public static final String KEY_ANDROID_ID = "android_id";
    public static final String KEY_STORAGE_ID = "storage_id";
    public static final String KEY_LAST_BACKUP_AT = "last_backup_at";
    public static final String KEY_NUM_BACKUP_FILES = "num_backup_files";

    private String mDeviceModel;
    private String mAndroidId;
    private long mStorageId;
    private long mLastBackupAt;
    private int mNumBackupdFiles;

    public RemoteConfigurationFile(String deviceModel, String androidId, long storageId, long lastBackupAt, int numBackupdFiles) {
        mDeviceModel = deviceModel;
        mAndroidId = androidId;
        mStorageId = storageId;
        mLastBackupAt = lastBackupAt;
        mNumBackupdFiles = numBackupdFiles;
    }

    public RemoteConfigurationFile(JSONObject jsonObject) throws JSONException {
        mDeviceModel = jsonObject.getString(KEY_DEVICE_MODEL);
        mAndroidId = jsonObject.getString(KEY_ANDROID_ID);
        mStorageId = jsonObject.getLong(KEY_STORAGE_ID);
        mLastBackupAt = jsonObject.getLong(KEY_LAST_BACKUP_AT);
        mNumBackupdFiles = jsonObject.getInt(KEY_NUM_BACKUP_FILES);
    }

    public String getDeviceModel() {
        return mDeviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        mDeviceModel = deviceModel;
    }

    public String getAndroidId() {
        return mAndroidId;
    }

    public void setAndroidId(String androidId) {
        mAndroidId = androidId;
    }

    public long getStorageId() {
        return mStorageId;
    }

    public void setStorageId(long storageId) {
        mStorageId = storageId;
    }

    public long getLastBackupAt() {
        return mLastBackupAt;
    }

    public void setLastBackupAt(long lastBackupAt) {
        mLastBackupAt = lastBackupAt;
    }

    public int getNumBackupdFiles() {
        return mNumBackupdFiles;
    }

    public void setNumBackupdFiles(int numBackupdFiles) {
        mNumBackupdFiles = numBackupdFiles;
    }

    public JSONObject toJsonObject() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(KEY_DEVICE_MODEL, getDeviceModel());
            jsonObject.put(KEY_ANDROID_ID, getAndroidId());
            jsonObject.put(KEY_STORAGE_ID, getStorageId());
            jsonObject.put(KEY_NUM_BACKUP_FILES, getNumBackupdFiles());
            jsonObject.put(KEY_LAST_BACKUP_AT, getLastBackupAt());

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static class Builder {
        private String mDeviceModel;
        private String mAndroidId;
        private long mStorageId;
        private long mLastBackupAt = 0;
        private int mNumBackupdFiles = 0;


        public Builder() {
        }

        public Builder setDeviceModel(String deviceModel) {
            mDeviceModel = deviceModel;
            return this;
        }

        public Builder setAndroidId(String androidId) {
            if (androidId != null) {
                mAndroidId = androidId;
            } else {
                mAndroidId = "UNKNOWN";
            }

            return this;
        }

        public Builder setStorageId(long storageId) {
            mStorageId = storageId;
            return this;
        }

        public Builder setLastBackupAt(long lastBackupAt) {
            mLastBackupAt = lastBackupAt;
            return this;
        }

        public Builder setNumBackupFiles(int num) {
            mNumBackupdFiles = num;
            return this;
        }

        public RemoteConfigurationFile build() {
            return new RemoteConfigurationFile(
                    mDeviceModel,
                    mAndroidId,
                    mStorageId,
                    mLastBackupAt,
                    mNumBackupdFiles
                    );
        }
    }
}
