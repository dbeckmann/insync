package com.dbeckmann.sync.data.task

import android.content.Context
import android.database.ContentObserver
import android.net.Uri
import android.provider.MediaStore
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by daniel on 23.05.2018.
 */
class MediastoreContentObserver @Inject constructor(
        context: Context
) {

    companion object {
        val mediaStoreChangedSubject: PublishSubject<MediaStoreChangePublish> = PublishSubject.create()
    }

    init {
        context.contentResolver.registerContentObserver(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                true,
                object : ContentObserver(null) {
                    override fun onChange(selfChange: Boolean, uri: Uri?) {
                        uri?.let {
                            handleChange(it)
                        }
                    }
                }
        )
    }

    private fun handleChange(uri: Uri) {
        mediaStoreChangedSubject.onNext(MediaStoreChangePublish(uri))

        println("updates ${uri.encodedPath} -> ${uri.encodedQuery} -> ${uri.encodedAuthority}")
    }
}

data class MediaStoreChangePublish(
        val uri: Uri
)