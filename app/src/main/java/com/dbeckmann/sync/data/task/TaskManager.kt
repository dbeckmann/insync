package com.dbeckmann.sync.data.task

import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType.UNMETERED
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSettingEntity
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.views.storagesetting.StorageSettingPreferenceDataStore
import java.util.concurrent.TimeUnit

class TaskManager {

    companion object {
        private const val TAG = "TaskManager"
        private const val BACKUP_REQUEST_NAME = "autoBackupRequest_"
        private const val WORK_TAG = "autoBackup"
    }

    init {
        Logger.d(TAG, "initialized")

        storageSettingsChangedListener()
    }

    fun stopTask(storage: StorageEntity) {
        WorkManager.getInstance().cancelAllWorkByTag(createRequestName(storage))
        WorkManager.getInstance().cancelUniqueWork(createRequestName(storage))
    }

    private fun storageSettingsChangedListener() {
        StorageSettingPreferenceDataStore.storageSettingChangedSubject.subscribe {
            Logger.d(TAG, "storage settings changed for ${it.displayName}")
            setWorkerForStorage(it)
        }
    }

    private fun setWorkerForStorage(storage: StorageEntity) {
        storage.setting?.also { setting ->

            // cancel scheduled tasks if necessary
            if (!storage.isEnabled || setting.backupMode == StorageSettingEntity.AUTO_BACKUP_MODE_NEVER) {
                stopTask(storage)
                return@also
            }

            // prepare task with settings
            val constraintBuilder = Constraints.Builder()

            if (setting.isOnlyCharging) {
                Logger.d(TAG, "set requires charging true")
                constraintBuilder.setRequiresCharging(true)
            }

            if (setting.isOnlyWifi) {
                Logger.d(TAG, "set requires unmetered network true")
                constraintBuilder.setRequiredNetworkType(UNMETERED)
            }

            val autoBackupRequest = PeriodicWorkRequestBuilder<AutoBackupStorageWorker>(1, TimeUnit.HOURS)
                    .setConstraints(constraintBuilder.build())
                    .setInputData(workDataOf(Pair(AutoBackupStorageWorker.KEY_STORAGE_ID, storage.id)))
                    .addTag(WORK_TAG)
                    .build()

            WorkManager.getInstance().enqueueUniquePeriodicWork(
                    createRequestName(storage),
                    ExistingPeriodicWorkPolicy.REPLACE,
                    autoBackupRequest)
        }
    }

    private fun createRequestName(storage: StorageEntity) : String {
        return BACKUP_REQUEST_NAME + storage.id
    }
}