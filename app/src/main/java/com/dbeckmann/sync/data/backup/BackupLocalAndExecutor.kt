package com.dbeckmann.sync.data.backup

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.IBinder
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.sync.data.Logger
import javax.inject.Inject

/**
 * Created by daniel on 12.05.2018.
 */
class BackupLocalAndExecutor @Inject constructor(
        private var context: Context
) : BackupRepository, UseCaseExecutor {
    private val queue: MutableList<ExecutorQueueEntry> = mutableListOf()

    companion object {
        const val TAG = "BackupLocalAndExecutor"
        private var backupService: BackupService? = null
        private var serviceConnection: ServiceConnection? = null

        fun unbindService(context: Context) {
            Logger.d(TAG, "unbind service")

            backupService = null

            try {
                context.unbindService(serviceConnection)
            } catch (e: Exception) {
                Logger.e(TAG, "service unbind failed")
                e.printStackTrace()
            }
        }
    }

    override fun indexFiles(): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.indexFiles()
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    override fun findLocalFilesToBackup(storage: StorageEntity): Executor<List<LocalFileEntity>> = Executor { callback ->
        /*backupService?.let {
            it.findLocalFilesToBackup(storage)
        } ?: callback.onError(IllegalStateException("service not ready"))*/
    }

    override fun findLocalFilesToBackup(storage: StorageEntity,
                                        packages: List<String>): Executor<List<LocalFileEntity>> = Executor { callback ->
        /*backupService?.let {
            it.findLocalFilesToBackup(storage, packages)
        } ?: callback.onError(IllegalStateException("service not ready"))*/
    }

    override fun backupFiles(storage: StorageEntity, files: List<LocalFileEntity>): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.backupFiles(storage, files)
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    override fun processBackupForAllStorages(): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.processBackupForAllStorages()
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    override fun processBackupForStorage(storage: StorageEntity): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.processBackupForStorage(storage)
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    fun stopBackupForAllStorages(): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.stopBackupForAllStorages()
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    override fun backupForStorage(storage: StorageEntity): Executor<Unit> = Executor { callback ->
        backupService?.let {
            it.backupForStorage(storage)
        } ?: callback.onError(IllegalStateException("service not ready"))
    }

    override fun <Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        if (backupService == null) {
            queue.add(ExecutorQueueEntry(executor, callback as UseCaseCallback<Any?>))
            initManagingPrintService()
        } else {
            executor.execute(ExecutorCallback(
                    {
                        callback.onSuccess(it)
                    },
                    {
                        callback.onError(it)
                    }
            ))
        }
    }

    override fun <Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        run(executor, callback)
    }

    override fun stop() {
        //
    }

    override fun isDestroyed(): Boolean = false

    private fun initManagingPrintService() {
        val serviceIntent = Intent(context, BackupService::class.java)

        if (Build.VERSION.SDK_INT < 26) {
            context.startService(serviceIntent)
        } else {
            context.startForegroundService(serviceIntent)
        }

        serviceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                Logger.d(TAG, "onServiceDisconnected")
                backupService = null
            }

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                Logger.d(TAG, "onServiceConnected")
                var binder: BackupService.BackupServiceBinder = service as BackupService.BackupServiceBinder
                backupService = binder.getService()

                queue.forEach {
                    run(it.executor, it.callback)
                }

                queue.clear()
            }
        }

        context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }
}

data class ExecutorQueueEntry(
        val executor: Executor<Any?>,
        val callback: UseCaseCallback<Any?>
)