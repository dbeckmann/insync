package com.dbeckmann.sync.data.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

/**
 * Created by daniel on 05.07.17.
 */
@Entity
public class LocalFileBackup {
	@Id(autoincrement = true)
	private Long id;
	
	private long dateCreated;
	
	// relations
	private long localFileId;
	@ToOne(joinProperty = "localFileId")
	private LocalFile localFile;
	
	private long storageId;
	@ToOne(joinProperty = "storageId")
	private Storage storage;

	/** Used to resolve relations */
	@Generated(hash = 2040040024)
	private transient DaoSession daoSession;

	/** Used for active entity operations. */
	@Generated(hash = 106848751)
	private transient LocalFileBackupDao myDao;
	@Generated(hash = 561855056)
	public LocalFileBackup(Long id, long dateCreated, long localFileId,
			long storageId) {
		this.id = id;
		this.dateCreated = dateCreated;
		this.localFileId = localFileId;
		this.storageId = storageId;
	}
	@Generated(hash = 80134711)
	public LocalFileBackup() {
	}
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getDateCreated() {
		return this.dateCreated;
	}
	public void setDateCreated(long dateCreated) {
		this.dateCreated = dateCreated;
	}
	public long getLocalFileId() {
		return this.localFileId;
	}
	public void setLocalFileId(long localFileId) {
		this.localFileId = localFileId;
	}
	public long getStorageId() {
		return this.storageId;
	}
	public void setStorageId(long storageId) {
		this.storageId = storageId;
	}
	@Generated(hash = 120695424)
	private transient Long localFile__resolvedKey;
	/** To-one relationship, resolved on first access. */
	@Generated(hash = 290969208)
	public LocalFile getLocalFile() {
		long __key = this.localFileId;
		if (localFile__resolvedKey == null || !localFile__resolvedKey.equals(__key)) {
			final DaoSession daoSession = this.daoSession;
			if (daoSession == null) {
				throw new DaoException("Entity is detached from DAO context");
			}
			LocalFileDao targetDao = daoSession.getLocalFileDao();
			LocalFile localFileNew = targetDao.load(__key);
			synchronized (this) {
				localFile = localFileNew;
				localFile__resolvedKey = __key;
			}
		}
		return localFile;
	}
	/** called by internal mechanisms, do not call yourself. */
	@Generated(hash = 349612622)
	public void setLocalFile(@NotNull LocalFile localFile) {
		if (localFile == null) {
			throw new DaoException(
					"To-one property 'localFileId' has not-null constraint; cannot set to-one to null");
		}
		synchronized (this) {
			this.localFile = localFile;
			localFileId = localFile.getId();
			localFile__resolvedKey = localFileId;
		}
	}
	@Generated(hash = 1770166036)
	private transient Long storage__resolvedKey;
	/** To-one relationship, resolved on first access. */
	@Generated(hash = 1723919968)
	public Storage getStorage() {
		long __key = this.storageId;
		if (storage__resolvedKey == null || !storage__resolvedKey.equals(__key)) {
			final DaoSession daoSession = this.daoSession;
			if (daoSession == null) {
				throw new DaoException("Entity is detached from DAO context");
			}
			StorageDao targetDao = daoSession.getStorageDao();
			Storage storageNew = targetDao.load(__key);
			synchronized (this) {
				storage = storageNew;
				storage__resolvedKey = __key;
			}
		}
		return storage;
	}
	/** called by internal mechanisms, do not call yourself. */
	@Generated(hash = 1596245866)
	public void setStorage(@NotNull Storage storage) {
		if (storage == null) {
			throw new DaoException(
					"To-one property 'storageId' has not-null constraint; cannot set to-one to null");
		}
		synchronized (this) {
			this.storage = storage;
			storageId = storage.getId();
			storage__resolvedKey = storageId;
		}
	}
	/**
	 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
	 * Entity must attached to an entity context.
	 */
	@Generated(hash = 128553479)
	public void delete() {
		if (myDao == null) {
			throw new DaoException("Entity is detached from DAO context");
		}
		myDao.delete(this);
	}
	/**
	 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
	 * Entity must attached to an entity context.
	 */
	@Generated(hash = 1942392019)
	public void refresh() {
		if (myDao == null) {
			throw new DaoException("Entity is detached from DAO context");
		}
		myDao.refresh(this);
	}
	/**
	 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
	 * Entity must attached to an entity context.
	 */
	@Generated(hash = 713229351)
	public void update() {
		if (myDao == null) {
			throw new DaoException("Entity is detached from DAO context");
		}
		myDao.update(this);
	}
	/** called by internal mechanisms, do not call yourself. */
	@Generated(hash = 1729898580)
	public void __setDaoSession(DaoSession daoSession) {
		this.daoSession = daoSession;
		myDao = daoSession != null ? daoSession.getLocalFileBackupDao() : null;
	}
}
