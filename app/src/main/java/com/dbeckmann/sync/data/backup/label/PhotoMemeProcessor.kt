package com.dbeckmann.sync.data.backup.label

import androidx.exifinterface.media.ExifInterface
import java.io.File

class PhotoMemeProcessor : ImageLabelProcessor() {

    companion object {
        const val LABEL_MEME = "meme"
        const val LABEL_PHOTO = "photo"
    }

    override fun process(): List<String>? {
        if (!hasPhotoFileExtension()) {
            return listOf(LABEL_MEME)
        }

        filePath?.also {
            val exif = ExifInterface(it)

            val model = exif.getAttribute(ExifInterface.TAG_MODEL)
            val maker = exif.getAttribute(ExifInterface.TAG_MAKE)

            println("exif model: $model maker: $maker file: $filePath")

            return if (!model.isNullOrEmpty() && !maker.isNullOrEmpty()) {
                listOf(LABEL_PHOTO)
            } else {
                listOf(LABEL_MEME)
            }
        }

        return null
    }

    private fun hasPhotoFileExtension(): Boolean {
        return when (File(filePath).extension) {
            "jpg" -> true
            "jpeg" -> true
            else -> false
        }
    }
}