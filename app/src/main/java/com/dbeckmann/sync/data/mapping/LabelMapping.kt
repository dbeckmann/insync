package com.dbeckmann.sync.data.mapping

import com.dbeckmann.domain.model.LabelEntity
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.sync.data.greendao.Label
import com.dbeckmann.sync.data.greendao.LocalFile

object LabelMapping {

    fun fromEntity(localFile: LocalFileEntity) : List<Label> {
        val newLabels = mutableListOf<Label>()

        localFile.labels?.also {
            for (label in it.labels) {
                newLabels.add(Label(
                        null,
                        label,
                        localFile.id!!
                ))
            }
        }

        return newLabels
    }

    fun toEntity(entity: LocalFile) : LabelEntity? {
        val newLabels = mutableListOf<String>()

        for (label in entity.labels) {
            newLabels.add(label.label)
        }

        return if (newLabels.isNotEmpty()) {
            LabelEntity(newLabels)
        } else {
            null
        }
    }

}