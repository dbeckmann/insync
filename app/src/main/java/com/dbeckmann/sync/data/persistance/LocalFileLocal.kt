package com.dbeckmann.sync.data.persistance

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.LocalFileFilter
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.data.greendao.*
import com.dbeckmann.sync.data.mapping.LabelMapping
import com.dbeckmann.sync.data.mapping.LocalFileMapping
import org.greenrobot.greendao.query.WhereCondition
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by daniel on 10.05.2018.
 */

@Singleton
class LocalFileLocal @Inject constructor(
        private val daoSession: DaoSession

) : LocalFileRepository {

    companion object {
        private const val TAG = "LocalFileLocal"
    }

    init {
        Logger.d(TAG, "created")
    }

    override fun insertOrUpdate(localFile: LocalFileEntity): Executor<LocalFileEntity> = Executor { callback ->
        if (localFile.id == -1L) { // FIXME !!!
            localFile.id = null
        }
        val rowId = getLocalFileDao().insertOrReplace(LocalFileMapping.fromEntity(localFile))
        localFile.id = rowId

        insertOrUpdateLabels(localFile)

        callback.onSuccess(localFile)
    }

    override fun delete(localFile: LocalFileEntity): Executor<Unit> = Executor { callback ->
        val query = daoSession.localFileDao.queryBuilder()
        query.where(LocalFileDao.Properties.Id.eq(localFile.id))
        query.buildDelete().executeDeleteWithoutDetachingEntities()
        daoSession.clear()

        callback.onSuccess(Unit)
    }

    override fun findAll(): Executor<List<LocalFileEntity>> = Executor { callback ->
        val items = doFindAll()

        if (items.isNotEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findByInternalId(internalId: String): Executor<LocalFileEntity> = Executor { callback ->
        val items = getLocalFileDao().queryBuilder()
                .where(LocalFileDao.Properties.InternalId.eq(internalId.toLong()))
                .list()

        if (items.isNotEmpty()) {
            callback.onSuccess(LocalFileMapping.toEntity(items[0]))
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findNotBackupedFiles(storage: StorageEntity): Executor<List<LocalFileEntity>> = Executor { callback ->
        val rawSqlQuery = "LEFT JOIN LOCAL_FILE_BACKUP AS LFB ON T._id = LFB.local_file_id " +
                "WHERE (LFB._id IS NULL " +
                "OR (LFB.storage_id <> ? AND LFB.local_file_id NOT IN (SELECT local_file_id FROM LOCAL_FILE_BACKUP WHERE storage_id = ?))) " +
                "AND T.never_backup = 0 " +
                "ORDER BY T.date_added DESC"

        val items = getLocalFileDao().queryRaw(rawSqlQuery, storage.id.toString(), storage.id.toString())

        if (!items.isEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findNotBackupedFiles(storage: StorageEntity, packageNames: List<String>): Executor<List<LocalFileEntity>> = Executor { callback ->
        val inCondition = LocalFileDao.Properties.AppPackageName.`in`(packageNames) as WhereCondition.PropertyCondition

        val rawSqlQuery = "LEFT JOIN LOCAL_FILE_BACKUP ON T._id = LOCAL_FILE_BACKUP.local_file_id " +
                "WHERE (LOCAL_FILE_BACKUP.storage_id IS NULL " +
                "OR LOCAL_FILE_BACKUP.storage_id <> ?) " +
                "AND T.app_package_name " + inCondition.op

        val paraValues = arrayOfNulls<String>(packageNames.size + 1)
        paraValues[0] = storage.id.toString()
        for (i in packageNames.indices) {
            paraValues[i + 1] = packageNames[i]
        }

        val items = getLocalFileDao().queryRaw(rawSqlQuery, *paraValues)

        if (!items.isEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findNotBackupedFilesForDate(storage: StorageEntity, date: Long): Executor<List<LocalFileEntity>> = Executor { callback ->
        val rawSqlQuery = "LEFT JOIN LOCAL_FILE_BACKUP AS LFB ON T._id = LFB.local_file_id " +
                "WHERE (LFB._id IS NULL " +
                "OR (LFB.storage_id <> ? AND LFB.local_file_id NOT IN (SELECT local_file_id FROM LOCAL_FILE_BACKUP WHERE storage_id = ?))) " +
                "AND T.never_backup = 0 " +
                "AND T.day = ? " +
                "ORDER BY T.date_added DESC"

        val items = getLocalFileDao().queryRaw(rawSqlQuery, storage.id.toString(), storage.id.toString(), date.toString())

        if (!items.isEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findNotBackupedDates(storage: StorageEntity, limit: Int): Executor<List<Long>> = Executor { callback ->
        val rawSqlQuery = "LEFT JOIN LOCAL_FILE_BACKUP AS LFB ON T._id = LFB.local_file_id " +
                "WHERE (LFB._id IS NULL " +
                "OR (LFB.storage_id <> ? AND LFB.local_file_id NOT IN (SELECT local_file_id FROM LOCAL_FILE_BACKUP WHERE storage_id = ?))) " +
                "AND T.never_backup = 0 " +
                "GROUP BY T.day " +
                "ORDER BY T.day DESC " +
                "LIMIT ?"

        val items = getLocalFileDao().queryRaw(rawSqlQuery, storage.id.toString(), storage.id.toString(), limit.toString())

        if (items.isNotEmpty()) {
            callback.onSuccess(items.map { it.day })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun findBackupedFilesByDate(storage: StorageEntity, date: Long): Executor<List<LocalFileEntity>> = Executor { callback ->
        val query = getLocalFileDao().queryBuilder()
        query
                .where(LocalFileDao.Properties.Day.eq(date))
                .join(LocalFileBackup::class.java, LocalFileBackupDao.Properties.LocalFileId)
                .where(LocalFileBackupDao.Properties.StorageId.eq(storage.id))


        val items = query.list()

        if (items.isNotEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun addBackup(storage: StorageEntity, localFile: LocalFileEntity): Executor<LocalFileEntity> = Executor { callback ->
        var localFileBackup = LocalFileBackup()

        // check if file was maybe already backuped
        val items = getBackupDao().queryBuilder()
                .where(LocalFileBackupDao.Properties.LocalFileId.eq(localFile.id))
                .where(LocalFileBackupDao.Properties.StorageId.eq(storage.id))
                .list()

        if (items.isNotEmpty()) {
            localFileBackup = items[0]
            localFileBackup.dateCreated = System.currentTimeMillis()
        } else {
            localFileBackup.apply {
                dateCreated = System.currentTimeMillis()
                localFileId = localFile.id!!
                storageId = storage.id!!
            }
        }
        getBackupDao().insertOrReplace(localFileBackup)

        localFile.apply {
            this.storage = storage
            this.hasBackup = true
        }

        callback.onSuccess(localFile)
    }

    override fun findAllSetBackupFlag(storage: StorageEntity): Executor<List<LocalFileEntity>> = Executor { callback ->
        val backupFiles = doFindByStorageId(storage) as MutableList
        val allFiles = doFindAll()

        val result = allFiles.map {
                LocalFileMapping.toEntity(it).apply {
                    this.hasBackup = false

                    val listIterator = backupFiles.iterator()
                    for (backup in listIterator) {
                        if (this.id == backup.id) {
                            this.storage = storage
                            this.hasBackup = true
                            listIterator.remove()
                        }
                    }
                }
            }

        callback.onSuccess(result)
    }

    override fun findByStorageId(storage: StorageEntity): Executor<List<LocalFileEntity>> = Executor { callback ->
        val items = doFindByStorageId(storage)

        if (items.isNotEmpty()) {
            callback.onSuccess(items.map {
                LocalFileMapping.toEntity(it).apply {
                                                        this.hasBackup = true
                                                        this.storage = storage
                                                    }
            })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun filter(filter: LocalFileFilter): Executor<List<LocalFileEntity>> = Executor { callback ->
        val query = getLocalFileDao().queryBuilder()

        filter.isBackupable?.let {
            query.where(LocalFileDao.Properties.NeverBackup.eq(!it))
        }

        filter.appPackageNames?.let {
            query.where(LocalFileDao.Properties.AppPackageName.`in`(filter.appPackageNames))
        }

        val items = query.list()

        if (items.isNotEmpty()) {
            callback.onSuccess(items.map { LocalFileMapping.toEntity(it) })
        } else {
            callback.onError(NoSuchElementException())
        }
    }

    override fun deleteBackupForStorage(storage: StorageEntity): Executor<Unit> = Executor { callback ->
        try {
            daoSession.localFileBackupDao.queryBuilder()
                    .where(LocalFileBackupDao.Properties.StorageId.eq(storage.id))
                    .buildDelete().executeDeleteWithoutDetachingEntities()

            callback.onSuccess(Unit)
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    private fun doFindAll() : List<LocalFile> {
        return getLocalFileDao().queryBuilder()
                .orderDesc(LocalFileDao.Properties.DateAdded)
                .list()
    }

    private fun doFindByStorageId(storage: StorageEntity) : List<LocalFile> {
        val query = getLocalFileDao().queryBuilder()
        query
                .join(LocalFileBackup::class.java, LocalFileBackupDao.Properties.LocalFileId)
                .where(LocalFileBackupDao.Properties.StorageId.eq(storage.id))

        return query.list()
    }

    private fun insertOrUpdateLabels(localFile: LocalFileEntity) {
        localFile.id?.also {

            // delete first then add rows
            daoSession.labelDao.queryBuilder()
                    .where(LabelDao.Properties.LocalFileId.eq(it))
                    .buildDelete().executeDeleteWithoutDetachingEntities()

            for(label in LabelMapping.fromEntity(localFile)) {
                daoSession.labelDao.insertOrReplace(label)
            }
        }
    }

    private fun getLocalFileDao() : LocalFileDao {
        return daoSession.localFileDao
    }

    private fun getBackupDao() : LocalFileBackupDao {
        return daoSession.localFileBackupDao
    }
}