package com.dbeckmann.sync.data.backup

import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 11.07.2018.
 */
class StopBackupForAllStorages(
        private val repository: BackupLocalAndExecutor,
        useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<Unit>(useCaseExecutor) {

    override fun buildUseCase(params: Unit): Executor<Unit> {
        return repository.stopBackupForAllStorages()
    }
}