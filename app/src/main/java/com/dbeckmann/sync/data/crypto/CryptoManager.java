package com.dbeckmann.sync.data.crypto;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.x500.X500Principal;

/**
 * Created by daniel on 26.08.16.
 */
public class CryptoManager
{
	private static final String CIPHER_TRANSFORMATION = "RSA/ECB/PKCS1Padding";
	private static final String ANDROID_KEYSTORE = "AndroidKeyStore";
	private static final String DEFAULT_KEYSTORE_ALIAS = "sync";
	private static final String DEFAULT_CERTIFICATE_COMMON_NAME = "cryptomanager";
	private static final String DEFAULT_CERTIFICATE_ORGANIZATION = "organization";

	private static CryptoManager mInstance;

	public static CryptoManager getInstance() {
		if (mInstance == null) {
			mInstance = new CryptoManager();
		}

		return mInstance;
	}

	/**
	 *
	 * @param targetString
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws CertificateException
	 * @throws UnrecoverableEntryException
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws InvalidKeyException
	 */
	public String encrypt(String targetString) throws NoSuchAlgorithmException, NoSuchPaddingException, CertificateException, UnrecoverableEntryException, KeyStoreException, IOException, InvalidKeyException
	{
		String encryptedString = null;

		Cipher cipher = getCipher();
		cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		CipherOutputStream cipherOutputStream = new CipherOutputStream(
					outputStream, cipher);
		cipherOutputStream.write(targetString.getBytes("UTF-8"));
		cipherOutputStream.close();

		byte[] vals = outputStream.toByteArray();
		encryptedString = (Base64.encodeToString(vals, Base64.DEFAULT));

		return encryptedString;
	}

	public String encryptNoThrow(String targetString)
	{
		String encryptedString = null;

		try {
			encryptedString = encrypt(targetString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return encryptedString;
	}

	/**
	 *
	 * @param targetString
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws CertificateException
	 * @throws UnrecoverableEntryException
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws InvalidKeyException
	 */
	public String decrypt(String targetString) throws NoSuchAlgorithmException, NoSuchPaddingException, CertificateException, UnrecoverableEntryException, KeyStoreException, IOException,
				InvalidKeyException
	{
		String decryptedString = null;

		Cipher cipher = getCipher();
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey());

		CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(Base64.decode(targetString, Base64.DEFAULT)), cipher);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int byteRead = -1;
		byte[] buffer = new byte[4096];
		while((byteRead = cipherInputStream.read(buffer)) != -1) {
			out.write(buffer, 0, byteRead);
		}

		decryptedString = out.toString("UTF-8");

		return decryptedString;
	}

	public String decryptNoThrow(String targetString) {
		String decryptedString = null;

		try {
			decryptedString = decrypt(targetString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return decryptedString;
	}

	/**
	 *
	 * @return
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 */
	private Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException
	{
		// Provider: "AndroidOpenSSL" or "AndroidKeyStoreBCWorkaround"
		Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);

		return cipher;
	}

	/**
	 *
	 * @return
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws UnrecoverableEntryException
	 */
	private PublicKey getPublicKey() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, UnrecoverableEntryException
	{
		KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) getKeyStore().getEntry(DEFAULT_KEYSTORE_ALIAS, null);
		PublicKey publicKey = privateKeyEntry.getCertificate().getPublicKey();

		return publicKey;
	}

	/**
	 *
	 * @return
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws UnrecoverableEntryException
	 */
	private PrivateKey getPrivateKey() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, UnrecoverableEntryException
	{
		KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) getKeyStore().getEntry(DEFAULT_KEYSTORE_ALIAS, null);
		PrivateKey privateKey = privateKeyEntry.getPrivateKey();

		return privateKey;
	}

	/**
	 *
	 * @return
	 * @throws KeyStoreException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	private KeyStore getKeyStore() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException
	{
		KeyStore ks = KeyStore.getInstance(ANDROID_KEYSTORE);
		ks.load(null);

		return ks;
	}

	public void deleteKey() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException
	{
		getKeyStore().deleteEntry(DEFAULT_KEYSTORE_ALIAS);
	}

	public void generateKey(Context context) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, CertificateException, KeyStoreException, IOException
	{
		if (!hasKeyAlias(DEFAULT_KEYSTORE_ALIAS))
		{
			KeyPairGenerator generator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);

			Calendar start = Calendar.getInstance();
			Calendar end = Calendar.getInstance();
			end.add(Calendar.YEAR, 100);

			KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
					.setAlias(DEFAULT_KEYSTORE_ALIAS)
					.setSubject(new X500Principal("CN=" + DEFAULT_CERTIFICATE_COMMON_NAME + ", O=" + DEFAULT_CERTIFICATE_ORGANIZATION))
					.setSerialNumber(BigInteger.valueOf(1337))
					.setStartDate(start.getTime())
					.setEndDate(end.getTime())
					//.setEncryptionRequired()
					.build();

			generator.initialize(spec);
			KeyPair keyPair = generator.generateKeyPair();
		}
	}

	public boolean hasKeyAlias(String alias) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
		return getKeyStore().containsAlias(alias);
	}
}
