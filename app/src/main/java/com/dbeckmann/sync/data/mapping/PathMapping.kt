package com.dbeckmann.sync.data.mapping

import android.os.Parcel
import android.os.Parcelable
import com.dbeckmann.domain.model.Path

/**
 * Created by daniel on 01.05.2018.
 */
class PathMapping(
        name: String,
        fullpath: String,
        parentPath: Path? = null,
        isDirectory: Boolean = true,
        isSecured: Boolean = true,
        size: Long = 0,
        modifiedAt: Long = 0,
        createdAt: Long = 0,
        isWritable: Boolean = true,
        isReadable: Boolean = true
) : Path(
        name = name,
        fullpath = fullpath,
        parentPath = parentPath,
        isDirectory = isDirectory,
        isSecured = isSecured,
        size = size,
        modifiedAt = modifiedAt,
        createdAt = createdAt,
        isWritable = isWritable,
        isReadable = isReadable
), Parcelable {
    constructor(parcel: Parcel) : this(
            name = parcel.readString(),
            fullpath = parcel.readString(),
            parentPath = parcel.readParcelable<PathMapping>(PathMapping::class.java.classLoader),
            isDirectory = parcel.readInt() != 0,
            isSecured = parcel.readInt() != 0,
            size = parcel.readLong(),
            modifiedAt = parcel.readLong(),
            createdAt = parcel.readLong(),
            isWritable = parcel.readInt() != 0,
            isReadable = parcel.readInt() != 0)

    companion object {
        fun fromPath(path: Path): PathMapping {
            return PathMapping(
                    name = path.name,
                    fullpath = path.fullpath,
                    parentPath = path.parentPath,
                    isDirectory = path.isDirectory,
                    isSecured = path.isSecured,
                    size = path.size,
                    modifiedAt = path.modifiedAt,
                    createdAt = path.createdAt,
                    isWritable = path.isWritable,
                    isReadable = path.isReadable
            )
        }

        @JvmField
        val CREATOR: Parcelable.Creator<PathMapping> = object : Parcelable.Creator<PathMapping> {
            override fun createFromParcel(parcel: Parcel): PathMapping {
                return PathMapping(parcel)
            }

            override fun newArray(size: Int): Array<PathMapping?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(fullpath)
        parcel.writeParcelable(
                if (parentPath != null) {
                    PathMapping.fromPath(parentPath)
                } else {
                    null
                }
                , flags)
        parcel.writeInt(if (isDirectory) 1 else 0)
        parcel.writeInt(if (isSecured) 1 else 0)
        parcel.writeLong(size)
        parcel.writeLong(modifiedAt)
        parcel.writeLong(createdAt)
        parcel.writeInt(if (isWritable) 1 else 0)
        parcel.writeInt(if (isReadable) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }
}