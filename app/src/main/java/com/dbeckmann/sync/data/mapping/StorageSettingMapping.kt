package com.dbeckmann.sync.data.mapping

import com.dbeckmann.domain.model.StorageSettingEntity
import com.dbeckmann.sync.data.greendao.StorageSetting

object StorageSettingMapping {

    fun fromEntity(entity: StorageSettingEntity, storageId: Long) : StorageSetting {
        return StorageSetting(
                null,
                entity.isOnlyWifi,
                entity.isOnlyHomeWifi,
                entity.isOnlyCharging,
                entity.backupMode,
                storageId
        )
    }

    fun toEntity(entity: StorageSetting) : StorageSettingEntity {
        return StorageSettingEntity(
                isOnlyHomeWifi = entity.needHomeWifi,
                isOnlyWifi = entity.needWifi,
                isOnlyCharging = entity.needCharging,
                backupMode = entity.autoBackupMode
        )
    }

}