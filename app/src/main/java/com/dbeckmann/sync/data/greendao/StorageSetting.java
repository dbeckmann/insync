package com.dbeckmann.sync.data.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

/**
 * Created by daniel on 09.07.2017.
 */

@Entity
public class StorageSetting {

    public static final int AUTO_BACKUP_MODE_NEVER = 0;
    public static final int AUTO_BACKUP_MODE_ALWAYS = 1;
    public static final int AUTO_BACKUP_MODE_NOTIFY_CONFIRM = 2;
    public static final int AUTO_BACKUP_MODE_NOTIFY = 3;

    @Id(autoincrement = true)
    private Long id;

    private boolean needWifi;
    private boolean needHomeWifi;
    private boolean needCharging;
    private int autoBackupMode;

    private long storageId;
    @ToOne(joinProperty = "storageId")
    private Storage storage;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 984658775)
    private transient StorageSettingDao myDao;
    @Generated(hash = 442202054)
    public StorageSetting(Long id, boolean needWifi, boolean needHomeWifi, boolean needCharging,
            int autoBackupMode, long storageId) {
        this.id = id;
        this.needWifi = needWifi;
        this.needHomeWifi = needHomeWifi;
        this.needCharging = needCharging;
        this.autoBackupMode = autoBackupMode;
        this.storageId = storageId;
    }
    @Generated(hash = 1315623449)
    public StorageSetting() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public boolean getNeedWifi() {
        return this.needWifi;
    }
    public void setNeedWifi(boolean needWifi) {
        this.needWifi = needWifi;
    }
    public boolean getNeedCharging() {
        return this.needCharging;
    }
    public void setNeedCharging(boolean needCharging) {
        this.needCharging = needCharging;
    }
    public long getStorageId() {
        return this.storageId;
    }
    public void setStorageId(long storageId) {
        this.storageId = storageId;
    }
    @Generated(hash = 1770166036)
    private transient Long storage__resolvedKey;
    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1723919968)
    public Storage getStorage() {
        long __key = this.storageId;
        if (storage__resolvedKey == null || !storage__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            StorageDao targetDao = daoSession.getStorageDao();
            Storage storageNew = targetDao.load(__key);
            synchronized (this) {
                storage = storageNew;
                storage__resolvedKey = __key;
            }
        }
        return storage;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1596245866)
    public void setStorage(@NotNull Storage storage) {
        if (storage == null) {
            throw new DaoException(
                    "To-one property 'storageId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.storage = storage;
            storageId = storage.getId();
            storage__resolvedKey = storageId;
        }
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    public int getAutoBackupMode() {
        return this.autoBackupMode;
    }
    public void setAutoBackupMode(int autoBackupMode) {
        this.autoBackupMode = autoBackupMode;
    }
    public boolean getNeedHomeWifi() {
        return this.needHomeWifi;
    }
    public void setNeedHomeWifi(boolean needHomeWifi) {
        this.needHomeWifi = needHomeWifi;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 800084098)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getStorageSettingDao() : null;
    }
}
