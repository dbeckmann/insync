package com.dbeckmann.sync.data.mapping

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.data.crypto.CryptoManager
import com.dbeckmann.sync.data.greendao.Storage
import com.dbeckmann.sync.data.greendao.StorageSetting

/**
 * Created by daniel on 03.05.2018.
 */
class StorageMapping {
    companion object {
        fun fromEntity(storage: StorageEntity) : Storage {
            return Storage(
                    storage.id,
                    storage.displayName,
                    storage.host,
                    storage.connectionAddress,
                    storage.hostIpAddress,
                    storage.hostMacAddress,
                    storage.homeWifiSsid,
                    storage.homeWifiBssid,
                    CryptoManager.getInstance().encryptNoThrow(storage.username),
                    CryptoManager.getInstance().encryptNoThrow(storage.auth),
                    CryptoManager.getInstance().encryptNoThrow(storage.auth),
                    storage.driverIdentifier,
                    storage.rootFolder,
                    storage.cacheMaxSpace,
                    storage.cacheUsedSpace,
                    storage.cacheUsedBackupSpace,
                    storage.lastBackupAt,
                    storage.isEnabled
            )
        }

        fun toEntity(storage: Storage, setting: StorageSetting?) : StorageEntity {
            return StorageEntity(
                    id = storage.id,
                    displayName = storage.displayName,
                    host = storage.hostName,
                    connectionAddress = storage.connectionAdress,
                    hostIpAddress = storage.hostIpAddress,
                    hostMacAddress = storage.hostMacAddress,
                    homeWifiSsid = storage.homeWifiSsid,
                    homeWifiBssid = storage.homeWifiBssid,
                    username = CryptoManager.getInstance().decryptNoThrow(storage.username),
                    auth = CryptoManager.getInstance().decryptNoThrow(storage.authToken),
                    rootFolder = storage.rootFolder,
                    cacheMaxSpace = storage.cacheMaxSpace,
                    cacheUsedSpace = storage.cacheUsedSpace,
                    cacheUsedBackupSpace = storage.cacheUsedBackupSpace,
                    lastBackupAt = storage.lastBackupAt,
                    isEnabled = storage.isEnabled,
                    driverIdentifier = storage.driverIdentifier,
                    setting = setting?.let { StorageSettingMapping.toEntity(it) }
            )
        }
    }
}