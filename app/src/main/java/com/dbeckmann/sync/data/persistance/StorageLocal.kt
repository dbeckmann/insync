package com.dbeckmann.sync.data.persistance

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.sync.data.greendao.DaoSession
import com.dbeckmann.sync.data.greendao.StorageDao
import com.dbeckmann.sync.data.greendao.StorageSetting
import com.dbeckmann.sync.data.greendao.StorageSettingDao
import com.dbeckmann.sync.data.mapping.StorageMapping
import com.dbeckmann.sync.data.mapping.StorageSettingMapping
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by daniel on 03.05.2018.
 */
@Singleton
class StorageLocal @Inject constructor(
        private val daoSession: DaoSession
) : StorageRepository {
    override fun insertOrUpdate(storage: StorageEntity): Executor<StorageEntity> = Executor { callback ->
        try {
            if (storage.id == -1L) { // FIXME !!!
                storage.id = null
            }
            val rowId = getDao().insertOrReplace(StorageMapping.fromEntity(storage))
            storage.id = rowId

            insertOrUpdateSettings(storage)

            callback.onSuccess(storage)
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    override fun findById(id: Long): Executor<StorageEntity> = Executor { callback ->
        try {
            val items = getDao().queryBuilder()
                    .where(StorageDao.Properties.Id.eq(id))
                    .list()

            if (!items.isEmpty()) {
                callback.onSuccess(StorageMapping.toEntity(items[0], findSettings(id)))
            } else {
                callback.onError(NoSuchElementException())
            }
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    override fun findAll(): Executor<List<StorageEntity>> = Executor { callback ->
        try {
            val items = getDao().queryBuilder().list()

            if (!items.isEmpty()) {
                callback.onSuccess(items.map { StorageMapping.toEntity(it, findSettings(it.id)) })
            } else {
                callback.onError(NoSuchElementException())
            }
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    override fun delete(id: Long): Executor<Unit> = Executor { callback ->
        try {
            findSettings(id)?.also {
                daoSession.storageSettingDao.delete(it)
            }

            getDao().deleteByKey(id)

            callback.onSuccess(Unit)
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    private fun findSettings(storageId: Long): StorageSetting? {
        val item = daoSession.storageSettingDao.queryBuilder()
                .where(StorageSettingDao.Properties.StorageId.eq(storageId))
                .list()

        return if (item.isNotEmpty()) {
            item[0]
        } else {
            null
        }
    }

    private fun insertOrUpdateSettings(storage: StorageEntity) {
        storage.id?.also { storageId ->
            storage.setting?.also { settingEntity ->
                val setting = findSettings(storageId)
                val settingId = setting?.id
                val newSetting = StorageSettingMapping.fromEntity(settingEntity, storageId)
                newSetting.id = settingId

                daoSession.storageSettingDao.insertOrReplace(newSetting)
            }
        }
    }

    private fun getDao(): StorageDao {
        return daoSession.storageDao
    }
}