package com.dbeckmann.sync.data.backup.label

abstract class ImageLabelProcessor {

    protected var filePath: String? = null

    fun setFilePath(filePath: String?): ImageLabelProcessor {
        this.filePath = filePath
        return this
    }

    abstract fun process(): List<String>?
}