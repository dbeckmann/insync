package com.dbeckmann.sync.data.backup.label

class MultiImageLabelProcessor<T : ImageLabelProcessor>(
        private vararg val processors: T
) : ImageLabelProcessor() {

    override fun process(): List<String>? {
        val labels = mutableListOf<String>()

        for (p in processors) {
            p.setFilePath(filePath).process()?.also {
                labels.addAll(it)
            }
        }

        return if (labels.isNotEmpty()) {
            labels
        } else {
            null
        }
    }
}