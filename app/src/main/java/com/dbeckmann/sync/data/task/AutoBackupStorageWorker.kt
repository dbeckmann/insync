package com.dbeckmann.sync.data.task

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.dbeckmann.domain.usecase.backup.ProcessBackupForStorage
import com.dbeckmann.domain.usecase.fileoperation.RemoteAvailability
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.injection.annotation.RunAsService
import javax.inject.Inject

class AutoBackupStorageWorker(
        context: Context,
        params: WorkerParameters)
    : Worker(context, params) {

    @Inject @field:RunAsService lateinit var processBackupForStorage: ProcessBackupForStorage
    @Inject lateinit var findStorageById: FindStorageById
    @Inject lateinit var configuration: Configuration
    @Inject lateinit var checkAvailability: RemoteAvailability

    companion object {
        const val KEY_STORAGE_ID = "keyStorageEntity"
    }

    init {
        Injector.injectServiceComponent().inject(this)
    }

    override fun doWork(): Result {
        val storageId = inputData.getLong(KEY_STORAGE_ID, -1)

        findStorageById.execute(storageId) { storage ->
            if (storage.hasBackupConstraintsMet(configuration)) {
                checkAvailability.execute(storage) {
                    processBackupForStorage.execute(storage)
                }
            }
        }

        return Result.success()
    }
}