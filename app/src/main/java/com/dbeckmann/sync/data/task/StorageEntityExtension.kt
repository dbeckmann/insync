package com.dbeckmann.sync.data.task

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.data.Configuration

fun StorageEntity.hasBackupConstraintsMet(configuration: Configuration) : Boolean {
    setting?.also {

        if (it.isOnlyWifi && !configuration.isWifiConnected()) {
            return false
        }

        if (it.isOnlyHomeWifi && (configuration.getWifiBssid() != homeWifiBssid)) {
            return false
        }

        if (it.isOnlyCharging && !configuration.isCharging()) {
            return false
        }

        return true
    }

    return false
}