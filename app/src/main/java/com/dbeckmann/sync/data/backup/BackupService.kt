package com.dbeckmann.sync.data.backup

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSettingEntity
import com.dbeckmann.domain.usecase.backup.BackupFiles
import com.dbeckmann.domain.usecase.backup.BackupFilesParameter
import com.dbeckmann.domain.usecase.backup.IndexFiles
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackup
import com.dbeckmann.domain.usecase.storage.FindAllStorages
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.rxexecutor.toSingle
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.notification.BackupFilesNotification
import com.dbeckmann.sync.views.notification.ConfirmBackupNotification
import com.dbeckmann.sync.views.notification.IndexFilesNotification
import com.dbeckmann.sync.views.notification.NotificationBase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * Created by daniel on 12.05.2018.
 */
class BackupService : Service() {
    @Inject lateinit var configuration: Configuration
    @Inject lateinit var indexFiles: IndexFiles
    @Inject lateinit var backupFiles: BackupFiles
    @Inject lateinit var findAllStorages: FindAllStorages
    @Inject lateinit var findFilesWithoutBackup: FindFilesWithoutBackup
    @Inject lateinit var findStorageById: FindStorageById

    private var backupQueue: MutableList<BackupFileQueueEntry> = mutableListOf()
    private lateinit var backupFilesNotification: BackupFilesNotification
    private var hasJob = false

    private var backupFilesDisposable: Disposable? = null

    companion object {
        const val TAG = "BackupService"

        private val fileQueueStateMap: MutableMap<Long, MutableList<FileQueueState>> = mutableMapOf()
        val queueSubject: BehaviorSubject<Map<Long, List<FileQueueState>>> = BehaviorSubject.create()
        val indexingSubject: BehaviorSubject<IndexState> = BehaviorSubject.create()
    }

    fun indexFiles() {
        Logger.d(TAG, "indexFiles")
        val indexNotification = IndexFilesNotification(applicationContext).apply {
            create()
            makeForeground(notification)
        }

        indexingSubject.onNext(IndexState.PROGRESS)

        indexFiles.execute(
                {
                    indexNotification.apply {
                        done()
                        cancel()
                    }

                    haltForeground(true)
                    indexingSubject.onNext(IndexState.COMPLETED)
                },
                {
                    indexNotification.error()
                    haltForeground(false)
                    indexingSubject.onNext(IndexState.FAILED)
                }
        )
    }

    fun backupFiles(storage: StorageEntity, files: List<LocalFileEntity>) {
        Logger.d(TAG, "processBackup")

        backupFilesNotification = BackupFilesNotification(applicationContext).apply {
            create(storage)
            makeForeground(notification)
        }

        addBackupJob(BackupFileQueueEntry(storage, files))
    }

    fun backupForStorage(storage: StorageEntity) {
        Logger.d(TAG, "backupForStorage id ${storage.id}")

        backupFilesNotification.indexing()
        makeForeground(backupFilesNotification.notification)

        indexFiles.execute(
                {
                    findFilesWithoutBackup.execute(storage,
                            {
                                //backupFiles(storage, it)
                            },
                            {
                                haltForeground(false)
                            })
                },
                {
                    haltForeground(false)
                })
    }

    fun processBackupForAllStorages() {
        Logger.d(TAG, "processBackupForAllStorages")

        findAllStorages.execute {
            it.forEach {

            }
        }
    }

    fun processBackupForStorage(storage: StorageEntity) {
        Logger.d(TAG, "processBackupForStorage id ${storage.id}")

        backupFilesNotification.indexing()
        makeForeground(backupFilesNotification.notification)

        indexFiles.execute(
                {
                    findFilesWithoutBackup.execute(storage,
                            {
                                when (storage.setting?.backupMode) {
                                    StorageSettingEntity.AUTO_BACKUP_MODE_ALWAYS -> {
                                        //backupFiles(storage, it)
                                    }

                                    StorageSettingEntity.AUTO_BACKUP_MODE_NOTIFY -> {
                                        ConfirmBackupNotification(applicationContext, storage.id!!.toInt()).create(storage, it.size)
                                        haltForeground(false)
                                    }

                                    StorageSettingEntity.AUTO_BACKUP_MODE_NOTIFY_CONFIRM -> {
                                        ConfirmBackupNotification(applicationContext, storage.id!!.toInt()).create(storage, it.size)
                                        haltForeground(false)
                                    }
                                }
                            },
                            {
                                haltForeground(false)
                            }
                    )
                },
                {
                    haltForeground(false)
                })
    }

    fun stopBackupForAllStorages() {
        Logger.d(TAG, "stopBackupForAllStorages")

        backupQueue.clear()
        hasJob = false
        backupFilesDisposable?.dispose()

        setAllFilesCanceled()
        propagateQueueState()
        fileQueueStateMap.clear()

        haltForeground(true)
    }

    private fun addBackupJob(entry: BackupFileQueueEntry) {
        backupQueue.add(entry)

        addFilesToMap(entry.storage, entry.files, BackupLocal.BackupState.QUEUED)
        propagateQueueState()

        if (!hasJob) {
            processBackupQueue()
        }
    }

    private fun processBackupQueue() {
        hasJob = true

        BackupLocal.fileBackupStateSubject
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Logger.d(TAG, "progress -> ${it.localFile.filePath} processed ${it.processed} of ${it.max}")

                    // update ui
                    if (hasJob && it.state == BackupLocal.BackupState.PROGRESS) {
                        backupFilesNotification.increment(it.max, it.processed)
                    }

                    // update queued files for listeners
                    if (hasJob && it.state == BackupLocal.BackupState.COMPLETED) {
                        removeFileFromMap(it.storage, it.localFile)
                        propagateQueueState()
                    }
                }

        val iterator = backupQueue.iterator()

        if (iterator.hasNext()) {
            val entry = iterator.next()
            iterator.remove()

            backupFilesDisposable = backupFiles.toSingle(BackupFilesParameter(entry.storage, entry.files))
                    .subscribe(
                            {
                                Logger.d(TAG, "onSuccess")
                                processBackupQueue()
                            },
                            {
                                Logger.d(TAG, "onError")
                                processBackupQueue()
                            })
        } else {
            Logger.d(TAG, "no entries left")

            hasJob = false
            haltForeground(false)
            //backupFilesNotification.done()
        }
    }

    private fun makeForeground(notification: Notification) {
        startForeground(NotificationBase.NOTIFICATION_ID, notification)
    }

    private fun haltForeground(removeNotification: Boolean) {
        stopForeground(removeNotification)
        BackupLocalAndExecutor.unbindService(applicationContext)
        stopSelf()
    }

    private fun addFileToMap(storage: StorageEntity, file: LocalFileEntity, state: BackupLocal.BackupState) {
        var list = mutableListOf<FileQueueState>()

        if (fileQueueStateMap.containsKey(storage.id)) {
            fileQueueStateMap[storage.id]?.also {
                list = it
            }
        }

        list.add(FileQueueState(storage, file, state))
        fileQueueStateMap[storage.id!!] = list
    }

    private fun addFilesToMap(storage: StorageEntity, files: List<LocalFileEntity>, state: BackupLocal.BackupState) {
        files.forEach {
            addFileToMap(storage, it, state)
        }
    }

    private fun removeFileFromMap(storage: StorageEntity, file: LocalFileEntity) {
        if (fileQueueStateMap.containsKey(storage.id)) {
            fileQueueStateMap[storage.id]?.apply {
                this.removeAt(this.indexOf(this.find{ it.file == file }))
            }
        }
    }

    private fun setAllFilesCanceled() {
        fileQueueStateMap.forEach { (key, value) ->
            value.forEach {
                it.state = BackupLocal.BackupState.CANCELED
            }
        }
    }

    private fun propagateQueueState() {
        queueSubject.onNext(fileQueueStateMap)
    }

    override fun onCreate() {
        Injector.injectServiceComponent().inject(this)

        backupFilesNotification = BackupFilesNotification(applicationContext)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Logger.d(TAG, "onUnbind")
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Logger.d(TAG, "onDestroy")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Logger.d(TAG, "onStartCommand")
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder {
        return BackupServiceBinder(this)
    }

    class BackupServiceBinder(private val service: BackupService) : Binder() {
        fun getService(): BackupService = service
    }

    data class BackupFileQueueEntry(
            val storage: StorageEntity,
            val files: List<LocalFileEntity>
    )

    data class FileQueueState(
            val storage: StorageEntity,
            val file: LocalFileEntity,
            var state: BackupLocal.BackupState
    )

    enum class IndexState {
        PROGRESS,
        COMPLETED,
        FAILED
    }
}