package com.dbeckmann.sync.data.task

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.dbeckmann.domain.usecase.backup.ProcessBackupForAllStorages
import com.dbeckmann.sync.data.backup.StopBackupForAllStorages
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.injection.annotation.RunAsService
import javax.inject.Inject

class AutoBackupWorker (
        context: Context,
        params: WorkerParameters)
: Worker(context, params) {

    @Inject lateinit var processBackup: ProcessBackupForAllStorages
    @Inject @field:RunAsService lateinit var stopBackup: StopBackupForAllStorages

    init {
        Injector.injectServiceComponent().inject(this)
    }

    override fun doWork(): Result {
        processBackup.execute()
        return Result.success()
    }

    override fun onStopped() {
        stopBackup.execute()
    }
}