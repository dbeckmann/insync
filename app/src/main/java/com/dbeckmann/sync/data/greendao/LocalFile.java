package com.dbeckmann.sync.data.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;
import org.greenrobot.greendao.DaoException;

/**
 * Created by daniel on 11.09.2016.
 */
@Entity
public class LocalFile {

    @Id(autoincrement = true)
    private Long id;

    private int type;
    private long internalId;
    private String filepath;
    private long day;
    private long dateAdded;
    private long dateModified;
    private String mimeType;
    private String hash;
    private long size;
    private String appPackageName;
    private boolean neverBackup;

    @Transient
    private boolean hasBackup = false;

    // relations
    @ToMany(referencedJoinProperty = "localFileId")
    private List<Label> labels;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1703908460)
    private transient LocalFileDao myDao;

    @Generated(hash = 1677461259)
    public LocalFile(Long id, int type, long internalId, String filepath, long day,
            long dateAdded, long dateModified, String mimeType, String hash,
            long size, String appPackageName, boolean neverBackup) {
        this.id = id;
        this.type = type;
        this.internalId = internalId;
        this.filepath = filepath;
        this.day = day;
        this.dateAdded = dateAdded;
        this.dateModified = dateModified;
        this.mimeType = mimeType;
        this.hash = hash;
        this.size = size;
        this.appPackageName = appPackageName;
        this.neverBackup = neverBackup;
    }
    @Generated(hash = 2106851084)
    public LocalFile() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getType() {
        return this.type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public long getInternalId() {
        return this.internalId;
    }
    public void setInternalId(long internalId) {
        this.internalId = internalId;
    }
    public String getFilepath() {
        return this.filepath;
    }
    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }
    public long getDateAdded() {
        return this.dateAdded;
    }
    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }
    public long getDateModified() {
        return this.dateModified;
    }
    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }
    public String getMimeType() {
        return this.mimeType;
    }
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    public String getHash() {
        return this.hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    public long getSize() {
        return this.size;
    }
    public void setSize(long size) {
        this.size = size;
    }
    public String getAppPackageName() {
        return this.appPackageName;
    }
    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }
    public boolean getNeverBackup() {
        return this.neverBackup;
    }
    public void setNeverBackup(boolean neverBackup) {
        this.neverBackup = neverBackup;
    }

    public void setHasBackup(boolean hasBackup) {
        this.hasBackup = hasBackup;
    }

    public boolean hasBackup() {
        return this.hasBackup;
    }
    public long getDay() {
        return this.day;
    }
    public void setDay(long day) {
        this.day = day;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 845229444)
    public List<Label> getLabels() {
        if (labels == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            LabelDao targetDao = daoSession.getLabelDao();
            List<Label> labelsNew = targetDao._queryLocalFile_Labels(id);
            synchronized (this) {
                if (labels == null) {
                    labels = labelsNew;
                }
            }
        }
        return labels;
    }
    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 902294403)
    public synchronized void resetLabels() {
        labels = null;
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1096818328)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getLocalFileDao() : null;
    }
}
