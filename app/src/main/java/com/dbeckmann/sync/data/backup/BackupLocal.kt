package com.dbeckmann.sync.data.backup

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.domain.usecase.fileoperation.CopyFileToRemote
import com.dbeckmann.domain.usecase.fileoperation.CopyFileToRemoteParameter
import com.dbeckmann.domain.usecase.localfile.DeleteLocalFile
import com.dbeckmann.domain.usecase.localfile.FindAllLocalFile
import com.dbeckmann.domain.usecase.localfile.FindLocalFileByInternalId
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackup
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackupByPackageName
import com.dbeckmann.domain.usecase.localfile.InsertOrUpdateLocalFile
import com.dbeckmann.domain.usecase.storage.AddBackup
import com.dbeckmann.domain.usecase.storage.AddBackupParameter
import com.dbeckmann.domain.util.PathUtil
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.data.backup.label.MlKitProcessor
import com.dbeckmann.sync.data.backup.label.MultiImageLabelProcessor
import com.dbeckmann.sync.data.backup.label.PhotoMemeProcessor
import io.reactivex.subjects.PublishSubject
import java.io.File
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by daniel on 10.05.2018.
 */

@Singleton
class BackupLocal @Inject constructor(
        private val context: Context,
        private val findByInternalId: FindLocalFileByInternalId,
        private val insertOrUpdate: InsertOrUpdateLocalFile,
        private val findFilesWithoutBackup: FindFilesWithoutBackup,
        private val findFilesWithoutBackupByPackageName: FindFilesWithoutBackupByPackageName,
        private val copyFileToRemote: CopyFileToRemote,
        private val addBackup: AddBackup,
        private val findAll: FindAllLocalFile,
        private val deleteLocalFile: DeleteLocalFile
) : BackupRepository {

    private var deviceInstalledPackages = HashMap<String, String>()

    companion object {
        private const val TAG = "BackupLocal"

        const val DEFAULT_APP_PACKAGE_NAME_DCIM = "DCIM"
        const val DEFAULT_APP_PACKAGE_NAME_DOWNLOAD = "DOWNLOAD"
        const val DEFAULT_APP_PACKAGE_NAME_SCREENSHOT = "SCREENSHOT"

        val fileBackupStateSubject: PublishSubject<FileState> = PublishSubject.create()
    }

    enum class BackupState {
        QUEUED,
        PROGRESS,
        COMPLETED,
        CANCELED,
        FAILED
    }

    data class FileState(
            val storage: StorageEntity,
            val localFile: LocalFileEntity,
            val state: BackupState,
            val max: Int,
            val processed: Int
    )

    init {
        Logger.d(TAG, "created")
    }

    override fun indexFiles(): Executor<Unit> = Executor { callback ->
        try {
            deviceInstalledPackages = getInstalledPackages()
            val mediaFiles = getAllMediaFiles()
            val indexFiles: MutableList<LocalFileEntity> = mutableListOf()

            findAll.executeBlocking {
                indexFiles.addAll(it)
            }

            if (mediaFiles.size == indexFiles.size) {
                Logger.d(TAG, "nothing to index")
                callback.onSuccess(Unit)

                return@Executor
            }

            // identify deleted files and remove from database
            val iterator = indexFiles.listIterator()
            for (localFile in iterator) {
                for (mediaFile in mediaFiles) {
                    if (mediaFile.internalId == localFile.internalId) {
                        iterator.remove()
                        break
                    }
                }
            }

            indexFiles.forEach {
                Logger.d(TAG, "remove file from index ${it.internalId} ${it.filePath}")
                deleteLocalFile.executeBlocking(it)
            }

            // TODO maybe get rid of blocking access to DB
            // identify new files and add them to index
            mediaFiles.forEach { localFile ->
                Logger.d(TAG, "checking file ${localFile.filePath} internal_id: ${localFile.internalId}")
                findByInternalId.executeBlocking(localFile.internalId, {},
                                                 {
                                                     if (it is NoSuchElementException) {
                                                         Logger.d(TAG, "add new file: ${localFile.filePath}")
                                                         insertOrUpdate.executeBlocking(localFile)
                                                     } else {
                                                         it.printStackTrace()
                                                     }
                                                 }
                )
            }

            callback.onSuccess(Unit)
        } catch (e: Throwable) {
            callback.onError(e)
        }
    }

    override fun findLocalFilesToBackup(storage: StorageEntity): Executor<List<LocalFileEntity>> {
        return findLocalFilesToBackup(storage, emptyList())
    }

    override fun findLocalFilesToBackup(storage: StorageEntity,
                                        packages: List<String>): Executor<List<LocalFileEntity>> = Executor { callback ->
        if (packages.isEmpty()) {
            findFilesWithoutBackup.execute(storage,
                                         {
                                             callback.onSuccess(it)
                                         },
                                         {
                                             callback.onError(it)
                                         })
        } else {
            findFilesWithoutBackupByPackageName.execute(FindFilesWithoutBackupByPackageName.FindFilesWithoutBackupByPackageNameParameter(storage, packages),
                                                      {
                                                          callback.onSuccess(it)
                                                      },
                                                      {
                                                          callback.onError(it)
                                                      })
        }
    }

    override fun processBackupForAllStorages(): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun processBackupForStorage(storage: StorageEntity): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun backupFiles(storage: StorageEntity, files: List<LocalFileEntity>): Executor<Unit> = Executor { callback ->
        var hasError = false
        var processed = 0

        files.forEach { file ->
            processed++
            propagateFileState(BackupState.PROGRESS, storage, file, files.size, processed)

            copyFileToRemote.executeBlocking(CopyFileToRemoteParameter(storage, PathUtil.rootFolderFromStorage(storage), File(file.filePath)),
                {
                    addBackup.executeBlocking(AddBackupParameter(storage, file))
                    propagateFileState(BackupState.COMPLETED, storage, file, files.size, processed)
                },
                {
                    it.printStackTrace()
                    hasError = true
                    propagateFileState(BackupState.FAILED, storage, file, files.size, processed)
                })
        }

        if (!hasError) {
            callback.onSuccess(Unit)
        } else {
            callback.onError(Throwable("backup failed"))
        }
    }

    override fun backupForStorage(storage: StorageEntity): Executor<Unit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun propagateFileState(state: BackupState, storage: StorageEntity, file: LocalFileEntity, max: Int, processed: Int) {
        fileBackupStateSubject.onNext(FileState(
                storage = storage,
                localFile = file,
                state = state,
                max = max,
                processed = processed
        ))
    }

    private fun getInstalledPackages(): HashMap<String, String> {
        val packageManager = context.packageManager

        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val resInfos = packageManager.queryIntentActivities(intent, 0)

        val appHashMap = HashMap<String, String>()
        for (resolveInfo in resInfos) {
            try {
                val appName = packageManager.getApplicationLabel(
                    packageManager.getApplicationInfo(resolveInfo.activityInfo.packageName, PackageManager.GET_META_DATA)).toString()
                appHashMap[resolveInfo.activityInfo.packageName] = appName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

        }

        return appHashMap
    }

    private fun getAllMediaFiles(): ArrayList<LocalFileEntity> {
        val cursor = context.contentResolver.query(
                MediaStore.Files.getContentUri("external"),
                arrayOf(MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA,
                        MediaStore.Images.Media.DATE_ADDED,
                        MediaStore.Images.Media.DATE_MODIFIED,
                        MediaStore.Images.Media.MIME_TYPE,
                        MediaStore.Images.Media.SIZE,
                        MediaStore.Files.FileColumns.MEDIA_TYPE),
                null,
                null,
                null
        )

        val labelProcessor = MultiImageLabelProcessor(
                PhotoMemeProcessor(),
                MlKitProcessor(context)
        )

        val fileList = ArrayList<LocalFileEntity>()
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val id = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media._ID))
                val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
                val dateAdded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED))
                val dateModified = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))
                val mimeType = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE))
                val size = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.SIZE))
                val mediaType = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE))

                if (mediaType != null) {
                    if (Integer.parseInt(mediaType) == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE || Integer.parseInt(
                            mediaType) == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
                        //Log.d(TAG, "id: " + id + " path: " + path + " dateAdded: " + dateAdded + " dateModified: " + dateModified + " mimeType: " + mimeType + " size: " + size + " mediaType: " + mediaType + " app: " + appPackageName);

                        val file = LocalFileEntity(
                            internalId = id,
                            filePath = path,
                            day = getDay(dateAdded.toLong()),
                            addedAt = dateAdded.toLong(),
                            modifiedAt = dateModified.toLong(),
                            mimeType = mimeType,
                            size = size.toLong(),
                            type = mediaType.toInt(),
                            appPackageName = getAppByPath(path)
                        )

                        // label images
                        /*
                        if (file.type == LocalFileEntity.TYPE_IMAGE) {
                            val labels = labelProcessor.setFilePath(file.filePath).process()

                            labels?.also {
                                file.labels = LabelEntity(it)
                            }
                        }*/

                        fileList.add(file)
                    }
                }

                cursor.moveToNext()
            }
        }

        cursor.close()

        return fileList
    }

    private fun getAppByPath(path: String): String? {
        when {
            isDcim(path)       -> return DEFAULT_APP_PACKAGE_NAME_DCIM
            isDownload(path)   -> return DEFAULT_APP_PACKAGE_NAME_DOWNLOAD
            isScreenshot(path) -> return DEFAULT_APP_PACKAGE_NAME_SCREENSHOT
            else               -> {
                for (entry in deviceInstalledPackages.entries) {
                    // todo replace this with regex
                    if (path.toLowerCase().contains(entry.value.toLowerCase())) {
                        return entry.key
                    }
                }

                return null
            }
        }
    }

    private fun getDay(timestamp: Long): Long {
        return timestamp - timestamp % (60 * 60 * 24)
    }

    private fun isDcim(path: String): Boolean {
        return path.startsWith("/storage/emulated/0/DCIM/")
    }

    private fun isDownload(path: String): Boolean {
        return path.startsWith("/storage/emulated/0/Download/")
    }

    private fun isScreenshot(path: String): Boolean {
        return path.startsWith("/storage/emulated/0/Pictures/Screenshots/")
    }
}