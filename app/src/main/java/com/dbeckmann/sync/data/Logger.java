package com.dbeckmann.sync.data;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.dbeckmann.sync.injection.Injector;

/**
 * Created by daniel on 02.12.2017.
 */

public class Logger {

    private static Logger mLogger;

    private Logger() {
        Injector.getApplicationComponent().inject(this);
    }

    private static Logger getInstance() {
        if (mLogger == null) {
            mLogger = new Logger();
        }

        return mLogger;
    }

    private int msgVerbose(String tag, String msg) {
        persistLoggingIfNecessary(tag, msg);
        return Log.v(tag, msg);
    }

    private int msgDebug(String tag, String msg) {
        persistLoggingIfNecessary(tag, msg);
        return Log.d(tag, msg);
    }

    private int msgInfo(String tag, String msg) {
        persistLoggingIfNecessary(tag, msg);
        return Log.i(tag, msg);
    }

    private int msgWarning(String tag, String msg) {
        persistLoggingIfNecessary(tag, msg);
        return Log.w(tag, msg);
    }

    private int msgError(String tag, String msg) {
        persistLoggingIfNecessary(tag, msg);
        return Log.e(tag, msg);
    }

    private void persistLoggingIfNecessary(String tag, String msg) {
        // do persisting if wished
    }

    public static int v(String tag, String msg) {
        return Logger.getInstance().msgVerbose(tag, msg);
    }

    public static int d(String tag, String msg) {
        return Logger.getInstance().msgDebug(tag, msg);
    }

    public static int i(String tag, String msg) {
        return Logger.getInstance().msgInfo(tag, msg);
    }

    public static int w(String tag, String msg) {
        return Logger.getInstance().msgWarning(tag, msg);
    }

    public static int e(String tag, String msg) {
        return Logger.getInstance().msgError(tag, msg);
    }

    public static void report(String tag, String msg, Throwable throwable) {
        e(tag, msg);

        try {
            Crashlytics.log(tag + ": " + msg);
            Crashlytics.logException(throwable);
        } catch (Exception e) {
            i(tag, "crash reporting disabled");
        }
    }
}
