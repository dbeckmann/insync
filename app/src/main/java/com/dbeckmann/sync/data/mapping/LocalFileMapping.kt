package com.dbeckmann.sync.data.mapping

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.sync.data.greendao.LocalFile
/**
 * Created by daniel on 10.05.2018.
 */
class LocalFileMapping {

    companion object {
        fun fromEntity(entity: LocalFileEntity) : LocalFile {
            return LocalFile(
                    entity.id,
                    entity.type,
                    entity.internalId.toLong(),
                    entity.filePath,
                    entity.day,
                    entity.addedAt,
                    entity.modifiedAt,
                    entity.mimeType,
                    entity.hash,
                    entity.size,
                    entity.appPackageName,
                    entity.neverBackup
            )
        }

        fun toEntity(localFile: LocalFile) : LocalFileEntity {
            return LocalFileEntity(
                    id = localFile.id,
                    type = localFile.type,
                    internalId = localFile.internalId.toString(),
                    filePath = localFile.filepath,
                    day = localFile.day,
                    addedAt = localFile.dateAdded,
                    modifiedAt = localFile.dateModified,
                    mimeType = localFile.mimeType,
                    hash = localFile.hash,
                    size = localFile.size,
                    appPackageName = localFile.appPackageName,
                    neverBackup = localFile.neverBackup,
                    labels = LabelMapping.toEntity(localFile)
            )
        }
    }
}