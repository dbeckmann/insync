package com.dbeckmann.sync.data.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;
import org.greenrobot.greendao.DaoException;

/**
 * Created by daniel on 05.09.2016.
 */
@Entity
public class Storage {
    @Id(autoincrement = true)
    private Long id;
    private String displayName;
    private String hostName;
    private String connectionAdress;
    private String hostIpAddress;
    private String hostMacAddress;
    private String homeWifiSsid;
    private String homeWifiBssid;
    private String username;
    private String password;
    private String authToken;
    private String driverIdentifier;
    private String rootFolder;
    private long cacheMaxSpace;
    private long cacheUsedSpace;
    private long cacheUsedBackupSpace;
    private long lastBackupAt;
    private boolean isEnabled;
    
    // relations
    @ToMany(referencedJoinProperty = "storageId")
    private List<LocalFileBackup>  localFileBackups;

    @ToMany(referencedJoinProperty = "storageId")
    private List<StorageSetting>  storageSettings;


    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 363957741)
    private transient StorageDao myDao;
    @Generated(hash = 595473355)
    public Storage(Long id, String displayName, String hostName, String connectionAdress,
            String hostIpAddress, String hostMacAddress, String homeWifiSsid, String homeWifiBssid,
            String username, String password, String authToken, String driverIdentifier,
            String rootFolder, long cacheMaxSpace, long cacheUsedSpace, long cacheUsedBackupSpace,
            long lastBackupAt, boolean isEnabled) {
        this.id = id;
        this.displayName = displayName;
        this.hostName = hostName;
        this.connectionAdress = connectionAdress;
        this.hostIpAddress = hostIpAddress;
        this.hostMacAddress = hostMacAddress;
        this.homeWifiSsid = homeWifiSsid;
        this.homeWifiBssid = homeWifiBssid;
        this.username = username;
        this.password = password;
        this.authToken = authToken;
        this.driverIdentifier = driverIdentifier;
        this.rootFolder = rootFolder;
        this.cacheMaxSpace = cacheMaxSpace;
        this.cacheUsedSpace = cacheUsedSpace;
        this.cacheUsedBackupSpace = cacheUsedBackupSpace;
        this.lastBackupAt = lastBackupAt;
        this.isEnabled = isEnabled;
    }
    @Generated(hash = 2114225574)
    public Storage() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getHostName() {
        return this.hostName;
    }
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
    public String getHostIpAddress() {
        return this.hostIpAddress;
    }
    public void setHostIpAddress(String hostIpAddress) {
        this.hostIpAddress = hostIpAddress;
    }
    public String getHostMacAddress() {
        return this.hostMacAddress;
    }
    public void setHostMacAddress(String hostMacAddress) {
        this.hostMacAddress = hostMacAddress;
    }
    public String getHomeWifiSsid() {
        return this.homeWifiSsid;
    }
    public void setHomeWifiSsid(String homeWifiSsid) {
        this.homeWifiSsid = homeWifiSsid;
    }
    public String getHomeWifiBssid() {
        return this.homeWifiBssid;
    }
    public void setHomeWifiBssid(String homeWifiBssid) {
        this.homeWifiBssid = homeWifiBssid;
    }
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public boolean getIsEnabled() {
        return this.isEnabled;
    }
    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    public String getConnectionAdress() {
        return this.connectionAdress;
    }
    public void setConnectionAdress(String connectionAdress) {
        this.connectionAdress = connectionAdress;
    }
    public String getAuthToken() {
        return this.authToken;
    }
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    public String getDriverIdentifier() {
        return this.driverIdentifier;
    }
    public void setDriverIdentifier(String driverIdentifier) {
        this.driverIdentifier = driverIdentifier;
    }
    public String getRootFolder() {
        return this.rootFolder;
    }
    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1934060042)
    public List<LocalFileBackup> getLocalFileBackups() {
        if (localFileBackups == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            LocalFileBackupDao targetDao = daoSession.getLocalFileBackupDao();
            List<LocalFileBackup> localFileBackupsNew = targetDao
                    ._queryStorage_LocalFileBackups(id);
            synchronized (this) {
                if (localFileBackups == null) {
                    localFileBackups = localFileBackupsNew;
                }
            }
        }
        return localFileBackups;
    }
    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 327747600)
    public synchronized void resetLocalFileBackups() {
        localFileBackups = null;
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    public String getDisplayName() {
        return this.displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public long getCacheMaxSpace() {
        return this.cacheMaxSpace;
    }
    public void setCacheMaxSpace(long cacheMaxSpace) {
        this.cacheMaxSpace = cacheMaxSpace;
    }
    public long getCacheUsedSpace() {
        return this.cacheUsedSpace;
    }
    public void setCacheUsedSpace(long cacheUsedSpace) {
        this.cacheUsedSpace = cacheUsedSpace;
    }
    public long getCacheUsedBackupSpace() {
        return this.cacheUsedBackupSpace;
    }
    public void setCacheUsedBackupSpace(long cacheUsedBackupSpace) {
        this.cacheUsedBackupSpace = cacheUsedBackupSpace;
    }
    public long getLastBackupAt() {
        return this.lastBackupAt;
    }
    public void setLastBackupAt(long lastBackupAt) {
        this.lastBackupAt = lastBackupAt;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1551359051)
    public List<StorageSetting> getStorageSettings() {
        if (storageSettings == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            StorageSettingDao targetDao = daoSession.getStorageSettingDao();
            List<StorageSetting> storageSettingsNew = targetDao._queryStorage_StorageSettings(id);
            synchronized (this) {
                if (storageSettings == null) {
                    storageSettings = storageSettingsNew;
                }
            }
        }
        return storageSettings;
    }
    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 998431369)
    public synchronized void resetStorageSettings() {
        storageSettings = null;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1527546401)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getStorageDao() : null;
    }


}
