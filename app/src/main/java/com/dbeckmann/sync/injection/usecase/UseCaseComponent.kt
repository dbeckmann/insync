package com.dbeckmann.sync.injection.usecase

import com.dbeckmann.domain.usecase.backup.BackupFiles
import com.dbeckmann.domain.usecase.backup.IndexFiles
import com.dbeckmann.domain.usecase.fileoperation.*
import com.dbeckmann.domain.usecase.localfile.*
import com.dbeckmann.domain.usecase.network.AuthUserWithCredentials
import com.dbeckmann.domain.usecase.network.MapNetwork
import com.dbeckmann.domain.usecase.network.MapNetworkAuto
import com.dbeckmann.domain.usecase.storage.AddBackup
import com.dbeckmann.domain.usecase.storage.FindAllStorages
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.domain.usecase.storage.InsertOrUpdateStorage
import dagger.Subcomponent

/**
 * Created by daniel on 11.05.2018.
 */

@Subcomponent(modules = [(UseCaseModule::class)])
interface UseCaseComponent {
    //val backupLocal: BackupLocal

    val mapNetwork: MapNetwork
    val mapNetworkAuto: MapNetworkAuto
    val authUserWithCredentials: AuthUserWithCredentials

    val listPath: ListPath
    val getRootPath: GetRootPath
    val createFolder: CreateFolder
    val readConfigFile: ReadConfigFile
    val writeConfigFile: WriteConfigFile
    val hasNoConfigFile: HasNoConfigFile
    val getStorageSpace: GetStorageSpace
    val getStorageSpaceFromStorageEntity: GetStorageSpaceFromStorageEntity
    val copyFileToRemote: CopyFileToRemote

    val insertOrUpdateStorage: InsertOrUpdateStorage
    val findAllStorages: FindAllStorages
    val findStorageById: FindStorageById

    val findLocalFileByInternalId: FindLocalFileByInternalId
    val insertOrUpdateLocalFile: InsertOrUpdateLocalFile
    val findAllLocalFile: FindAllLocalFile
    val findFileWithoutBackup: FindFilesWithoutBackup
    val findFilesWithoutBackupByPackageName: FindFilesWithoutBackupByPackageName
    val addBackup: AddBackup
    val findByStorageId: FindByStorageId
    val findAllSetBackupFlag: FindAllSetBackupFlag

    val indexFiles: IndexFiles
    val backupFiles: BackupFiles
}