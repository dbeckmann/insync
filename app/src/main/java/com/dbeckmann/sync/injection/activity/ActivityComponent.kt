package com.dbeckmann.sync.injection.activity


import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.sync.injection.ApplicationComponent
import com.dbeckmann.sync.injection.activity.viewmodel.ViewModelModule
import com.dbeckmann.sync.injection.annotation.ActivityScope
import com.dbeckmann.sync.injection.annotation.ActivityUseCaseExecutor
import com.dbeckmann.sync.injection.usecase.UseCaseModule
import com.dbeckmann.sync.views.addstorage.AddStorageWizardActivity
import com.dbeckmann.sync.views.addstorage.finalizeaddstorage.FinalizeAddStorageActivity
import com.dbeckmann.sync.views.addstorage.sambadiscovery.AuthDialogFragment
import com.dbeckmann.sync.views.addstorage.sambadiscovery.SambaDiscoveryActivity
import com.dbeckmann.sync.views.commonbrowse.browse.CommonBrowseActivity
import com.dbeckmann.sync.views.commonbrowse.createfolder.CreateFolderDialogFragment
import com.dbeckmann.sync.views.gallery.GalleryActivity
import com.dbeckmann.sync.views.main.MainActivity
import com.dbeckmann.sync.views.main.StorageFragment
import com.dbeckmann.sync.views.storagesetting.StorageSettingFragment
import dagger.Component

/**
 * Created by daniel on 10.12.2017.
 */

@ActivityScope
@Component(modules = [(ActivityModule::class), (ViewModelModule::class), (UseCaseModule::class)], dependencies = [(ApplicationComponent::class)])
interface ActivityComponent {

    @ActivityUseCaseExecutor
    fun activityExecutor() : UseCaseExecutor

    fun inject(activity: MainActivity)
    fun inject(activity: SambaDiscoveryActivity)
    fun inject(activity: CommonBrowseActivity)
    fun inject(activity: FinalizeAddStorageActivity)
    fun inject(activity: GalleryActivity)
    fun inject(activity: AddStorageWizardActivity)

    fun inject(fragment: AuthDialogFragment)
    fun inject(fragment: CreateFolderDialogFragment)
    fun inject(fragment: StorageFragment)
    fun inject(fragment: StorageSettingFragment)
}
