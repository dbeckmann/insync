package com.dbeckmann.sync.injection

import androidx.fragment.app.FragmentActivity
import com.dbeckmann.sync.AppController
import com.dbeckmann.sync.injection.activity.ActivityComponent
import com.dbeckmann.sync.injection.activity.ActivityModule
import com.dbeckmann.sync.injection.activity.DaggerActivityComponent
import com.dbeckmann.sync.injection.service.DaggerServiceComponent
import com.dbeckmann.sync.injection.service.ServiceComponent

/**
 * Created by daniel on 06.12.2017.
 */

object Injector {
    private val activityComponentMap: MutableMap<String, ActivityComponent> = mutableMapOf()
    private val lifecycleComponentMap: MutableMap<String, LifecycleAwareComponent> = mutableMapOf()

    @JvmStatic
    fun getApplicationComponent() : ApplicationComponent {
        return AppController.getInstance().applicationComponent
    }

    @JvmStatic
    fun injectActivityComponent(activity: FragmentActivity) : ActivityComponent {
        val component = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(ActivityModule(activity))
                .build()

        val key = getActivityKey(activity)

        activityComponentMap.forEach {
            println("mapped comp: ${it.key} = ${it.value}")
        }

        activityComponentMap[key] = component
        lifecycleComponentMap[key] = LifecycleAwareComponent(activity)

        return component
    }

    @JvmStatic
    fun injectServiceComponent() : ServiceComponent {
        return DaggerServiceComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build()
    }

    @JvmStatic
    fun getActivityComponent(activity: FragmentActivity): ActivityComponent {
        val key = getActivityKey(activity)
        return getActivityComponent(key)
    }

    @JvmStatic
    fun getActivityComponent(key: String): ActivityComponent {
        if (activityComponentMap.containsKey(key)) {
            return activityComponentMap[key]!!
        } else {
            throw IllegalStateException("could not find ActivityComponent $key")
        }
    }

    @JvmStatic
    fun removeActivityComponent(activity: FragmentActivity) {
        val key = getActivityKey(activity)

        if (activityComponentMap.containsKey(key)) {
            lifecycleComponentMap.remove(key)
            activityComponentMap.remove(key)

            println("ActivityComponent removed for $key")
        }
    }

    fun getActivityKey(activity: FragmentActivity): String {
        return activity::class.java.canonicalName!!
    }
}
