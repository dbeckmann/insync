package com.dbeckmann.sync.injection.service

import com.dbeckmann.sync.data.backup.BackupService
import com.dbeckmann.sync.data.task.AutoBackupStorageWorker
import com.dbeckmann.sync.data.task.AutoBackupWorker
import com.dbeckmann.sync.injection.ApplicationComponent
import com.dbeckmann.sync.injection.annotation.ServiceScope
import com.dbeckmann.sync.injection.usecase.UseCaseModule
import dagger.Component

/**
 * Created by daniel on 12.05.2018.
 */
@ServiceScope
@Component(modules = [(ServiceModule::class), (UseCaseModule::class)], dependencies = [(ApplicationComponent::class)])
interface ServiceComponent {

    fun inject(Service: BackupService)

    fun inject(task: AutoBackupWorker)
    fun inject(task: AutoBackupStorageWorker)
}