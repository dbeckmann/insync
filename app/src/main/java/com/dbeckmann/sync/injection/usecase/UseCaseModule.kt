package com.dbeckmann.sync.injection.usecase

import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.domain.usecase.backup.BackupFiles
import com.dbeckmann.domain.usecase.backup.BackupForStorage
import com.dbeckmann.domain.usecase.backup.IndexFiles
import com.dbeckmann.domain.usecase.backup.ProcessBackupForAllStorages
import com.dbeckmann.domain.usecase.backup.ProcessBackupForStorage
import com.dbeckmann.domain.usecase.fileoperation.CopyFileToRemote
import com.dbeckmann.domain.usecase.fileoperation.CreateFolder
import com.dbeckmann.domain.usecase.fileoperation.GetRootPath
import com.dbeckmann.domain.usecase.fileoperation.GetStorageSpace
import com.dbeckmann.domain.usecase.fileoperation.GetStorageSpaceFromStorageEntity
import com.dbeckmann.domain.usecase.fileoperation.HasNoConfigFile
import com.dbeckmann.domain.usecase.fileoperation.ListPath
import com.dbeckmann.domain.usecase.fileoperation.ReadConfigFile
import com.dbeckmann.domain.usecase.fileoperation.RemoteAvailability
import com.dbeckmann.domain.usecase.fileoperation.WriteConfigFile
import com.dbeckmann.domain.usecase.localfile.DeleteBackupForStorage
import com.dbeckmann.domain.usecase.localfile.DeleteLocalFile
import com.dbeckmann.domain.usecase.localfile.FilterLocalFile
import com.dbeckmann.domain.usecase.localfile.FindAllLocalFile
import com.dbeckmann.domain.usecase.localfile.FindAllSetBackupFlag
import com.dbeckmann.domain.usecase.localfile.FindFilesWithBackupForDate
import com.dbeckmann.domain.usecase.localfile.FindByStorageId
import com.dbeckmann.domain.usecase.localfile.FindLocalFileByInternalId
import com.dbeckmann.domain.usecase.localfile.FindDatesWithoutBackup
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackupForDate
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackup
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackupByPackageName
import com.dbeckmann.domain.usecase.localfile.InsertOrUpdateLocalFile
import com.dbeckmann.domain.usecase.network.AuthUserWithCredentials
import com.dbeckmann.domain.usecase.network.MapNetwork
import com.dbeckmann.domain.usecase.network.MapNetworkAuto
import com.dbeckmann.domain.usecase.storage.AddBackup
import com.dbeckmann.domain.usecase.storage.DeleteStorage
import com.dbeckmann.domain.usecase.storage.FindAllStorages
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.domain.usecase.storage.InsertOrUpdateStorage
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.sync.data.backup.BackupLocal
import com.dbeckmann.sync.data.backup.BackupLocalAndExecutor
import com.dbeckmann.sync.data.backup.StopBackupForAllStorages
import com.dbeckmann.sync.data.persistance.LocalFileLocal
import com.dbeckmann.sync.data.persistance.StorageLocal
import com.dbeckmann.sync.injection.annotation.RunAsService
import com.dbeckmann.sync.injection.annotation.SqliteOperation
import com.dbeckmann.sync.networking.FileOperationCreator
import com.dbeckmann.sync.networking.samba.SambaDiscovery
import dagger.Module
import dagger.Provides

/**
 * Created by daniel on 22.04.2018.
 */

@Module
class UseCaseModule {

    /**
     * NetworkRepository UseCases
     */

    @Provides
    fun mapNetwork(repository: SambaDiscovery, useCaseExecutor: UseCaseExecutor)
        : MapNetwork = MapNetwork(repository, useCaseExecutor)

    @Provides
    fun mapNetworkAuto(repository: SambaDiscovery, useCaseExecutor: UseCaseExecutor)
        : MapNetworkAuto = MapNetworkAuto(repository, useCaseExecutor)

    @Provides
    fun authUserWithCredentials(repository: SambaDiscovery, useCaseExecutor: UseCaseExecutor)
        : AuthUserWithCredentials = AuthUserWithCredentials(repository, useCaseExecutor)

    /**
     * FileOperationRepository UseCases
     */

    @Provides
    fun fileOperationFactory(): StorageFileOperationFactory = FileOperationCreator()

    @Provides
    fun listPath(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : ListPath = ListPath(fileOperationFactory, useCaseExecutor)

    @Provides
    fun getRootPath(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : GetRootPath = GetRootPath(fileOperationFactory, useCaseExecutor)

    @Provides
    fun createFolder(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : CreateFolder = CreateFolder(fileOperationFactory, useCaseExecutor)

    @Provides
    fun readConfigFile(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : ReadConfigFile = ReadConfigFile(fileOperationFactory, useCaseExecutor)

    @Provides
    fun writeConfigFile(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : WriteConfigFile = WriteConfigFile(fileOperationFactory, useCaseExecutor)

    @Provides
    fun hasNoConfigFile(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : HasNoConfigFile = HasNoConfigFile(fileOperationFactory, useCaseExecutor)

    @Provides
    fun getStorageSpace(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : GetStorageSpace = GetStorageSpace(fileOperationFactory, useCaseExecutor)

    @Provides
    fun getStorageSpaceFromStorage(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : GetStorageSpaceFromStorageEntity = GetStorageSpaceFromStorageEntity(fileOperationFactory, useCaseExecutor)

    @Provides
    fun copyFileToRemote(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
        : CopyFileToRemote = CopyFileToRemote(fileOperationFactory, useCaseExecutor)

    @Provides
    fun isAvailable(fileOperationFactory: StorageFileOperationFactory, useCaseExecutor: UseCaseExecutor)
            : RemoteAvailability = RemoteAvailability(fileOperationFactory, useCaseExecutor)

    /**
     * StorageRepository UseCases
     */

    @Provides
    fun insertOrUpdate(repository: StorageLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : InsertOrUpdateStorage = InsertOrUpdateStorage(repository, useCaseExecutor)

    @Provides
    fun findAll(repository: StorageLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindAllStorages = FindAllStorages(repository, useCaseExecutor)

    @Provides
    fun findById(repository: StorageLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindStorageById = FindStorageById(repository, useCaseExecutor)

    @Provides
    fun deleteById(repository: StorageLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
            : DeleteStorage = DeleteStorage(repository, useCaseExecutor)

    /**
     * LocalFileLocal UseCases
     */

    @Provides
    fun findLocalFileByInternalId(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindLocalFileByInternalId = FindLocalFileByInternalId(repository, useCaseExecutor)

    @Provides
    fun insertOrUpdateLocalFile(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : InsertOrUpdateLocalFile = InsertOrUpdateLocalFile(repository, useCaseExecutor)

    @Provides
    fun deleteLocalFile(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : DeleteLocalFile = DeleteLocalFile(repository, useCaseExecutor)

    @Provides
    fun findAllLocalFile(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindAllLocalFile = FindAllLocalFile(repository, useCaseExecutor)

    @Provides
    fun findNotBackupdFiles(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindFilesWithoutBackup = FindFilesWithoutBackup(repository, useCaseExecutor)

    @Provides
    fun findNotBackupdFilesByPackageName(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindFilesWithoutBackupByPackageName = FindFilesWithoutBackupByPackageName(repository, useCaseExecutor)

    @Provides
    fun addBackup(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : AddBackup = AddBackup(repository, useCaseExecutor)

    @Provides
    fun findByStorageId(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindByStorageId = FindByStorageId(repository, useCaseExecutor)

    @Provides
    fun findAllSetBackupFlag(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindAllSetBackupFlag = FindAllSetBackupFlag(repository, useCaseExecutor)

    @Provides
    fun findNotBackupdFilesForDate(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindFilesWithoutBackupForDate = FindFilesWithoutBackupForDate(repository, useCaseExecutor)

    @Provides
    fun findNotBackupedDates(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindDatesWithoutBackup = FindDatesWithoutBackup(repository, useCaseExecutor)

    @Provides
    fun findBackupedFilesForDate(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FindFilesWithBackupForDate = FindFilesWithBackupForDate(repository, useCaseExecutor)

    @Provides
    fun filterLocalFile(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
        : FilterLocalFile = FilterLocalFile(repository, useCaseExecutor)

    @Provides
    fun deleteBackupForStorage(repository: LocalFileLocal, @SqliteOperation useCaseExecutor: UseCaseExecutor)
            : DeleteBackupForStorage = DeleteBackupForStorage(repository, useCaseExecutor)

    /**
     * BackupRepository UseCases
     */

    @Provides
    fun indexFiles(repository: BackupLocal, useCaseExecutor: UseCaseExecutor)
        : IndexFiles = IndexFiles(repository, useCaseExecutor)

    @Provides
    @RunAsService
    fun indexFilesAsService(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
        : IndexFiles = IndexFiles(repository, useCaseExecutor)

    @Provides
    fun backupFiles(repository: BackupLocal, useCaseExecutor: UseCaseExecutor)
        : BackupFiles = BackupFiles(repository, useCaseExecutor)

    @Provides
    @RunAsService
    fun backupFilesAsService(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
        : BackupFiles = BackupFiles(repository, useCaseExecutor)

    @Provides
    fun processBackupForAllStorages(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
        : ProcessBackupForAllStorages = ProcessBackupForAllStorages(repository, useCaseExecutor)

    @Provides
    @RunAsService
    fun stopBackupForAllStorages(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
            : StopBackupForAllStorages = StopBackupForAllStorages(repository, useCaseExecutor)

    @Provides
    fun processBackupForStorage(repository: BackupLocal, useCaseExecutor: UseCaseExecutor)
            : ProcessBackupForStorage = ProcessBackupForStorage(repository, useCaseExecutor)

    @Provides
    @RunAsService
    fun processBackupForStorageAsService(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
            : ProcessBackupForStorage = ProcessBackupForStorage(repository, useCaseExecutor)

    @Provides
    fun backupForStorage(repository: BackupLocal, useCaseExecutor: UseCaseExecutor)
            : BackupForStorage = BackupForStorage(repository, useCaseExecutor)

    @Provides
    @RunAsService
    fun backupForStorageAsService(repository: BackupLocalAndExecutor, useCaseExecutor: BackupLocalAndExecutor)
            : BackupForStorage = BackupForStorage(repository, useCaseExecutor)

}