package com.dbeckmann.sync.injection

import android.content.Context
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.data.backup.BackupLocal
import com.dbeckmann.sync.data.greendao.DaoSession
import com.dbeckmann.sync.data.persistance.LocalFileLocal
import com.dbeckmann.sync.data.persistance.StorageLocal
import com.dbeckmann.sync.data.task.TaskManager
import com.dbeckmann.sync.injection.annotation.ObserveScheduler
import com.dbeckmann.sync.injection.annotation.SqliteOperation
import com.dbeckmann.sync.injection.usecase.UseCaseModule
import com.dbeckmann.sync.networking.samba.SambaDiscovery
import com.dbeckmann.sync.views.notification.NotificationBroadcastReceiver
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Singleton

/**
 * Created by daniel on 22.04.2018.
 */

@Singleton
@Component(modules = [(ApplicationModule::class), (UseCaseModule::class)])
interface ApplicationComponent {

    val context: Context
    val configuration: Configuration

    @ObserveScheduler fun asyncObserveScheduler() : Scheduler

    val taskManager: TaskManager

    val sambaRepository: SambaDiscovery
    val storageLocal: StorageLocal
    val localFileLocal: LocalFileLocal
    val backupLocal: BackupLocal
    val daoSession: DaoSession

    @SqliteOperation fun sqliteUseCaseExecutor() : UseCaseExecutor // needed because sqlite is limited in concurrency

    fun inject(logger: Logger)
    fun inject(receiver: NotificationBroadcastReceiver)
}