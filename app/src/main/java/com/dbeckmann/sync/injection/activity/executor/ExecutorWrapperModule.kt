package com.dbeckmann.sync.injection.activity.executor

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.sync.injection.annotation.ActivityScope
import com.dbeckmann.sync.injection.annotation.ActivityUseCaseExecutor
import dagger.Module
import dagger.Provides

@Module
class ExecutorWrapperModule {

    @Provides
    @ActivityScope
    fun useCaseExecutorWrapper(viewModel: UseCaseExecutorViewModel): UseCaseExecutor =
        ViewModelUseCaseExecutorWrapper(viewModel)

    @Provides
    @ActivityScope
    fun useCaseExecutorViewModel(fragmentActivity: FragmentActivity, @ActivityUseCaseExecutor executor: UseCaseExecutor): UseCaseExecutorViewModel {
        val viewModel = ViewModelProviders.of(fragmentActivity).get(UseCaseExecutorViewModel::class.java)
        viewModel.executor = executor
        return viewModel
    }
}