package com.dbeckmann.sync.injection.activity.executor

import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import javax.inject.Inject

class ViewModelUseCaseExecutorWrapper @Inject constructor(
    private val viewModel: UseCaseExecutorViewModel
) : UseCaseExecutor {

    init {
        println("create wrapper: $this")
    }

    override fun <Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        println("wrapper $this")
        useCaseExecutor().run(executor, callback)
    }

    override fun <Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        useCaseExecutor().runBlocking(executor, callback)
    }

    override fun stop() = useCaseExecutor().stop()

    override fun isDestroyed(): Boolean = useCaseExecutor().isDestroyed()

    private fun useCaseExecutor(): UseCaseExecutor {
        return viewModel.executor
    }
}