package com.dbeckmann.sync.injection.activity.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dbeckmann.sync.injection.annotation.ViewModelKey
import com.dbeckmann.sync.views.main.MainPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainPresenter::class)
    abstract fun bindMainPresenter(vm: MainPresenter): ViewModel
}