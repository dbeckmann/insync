package com.dbeckmann.sync.injection

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.fragment.app.FragmentActivity

class LifecycleAwareComponent(
        private val activity: FragmentActivity
) : LifecycleObserver {

    init {
        activity.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        Injector.removeActivityComponent(activity)
    }
}