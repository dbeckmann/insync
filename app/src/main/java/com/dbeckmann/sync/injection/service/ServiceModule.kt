package com.dbeckmann.sync.injection.service

import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.rxexecutor.RxExecutor
import com.dbeckmann.sync.injection.annotation.ObserveScheduler
import com.dbeckmann.sync.injection.annotation.ServiceScope
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * Created by daniel on 12.05.2018.
 */

@Module
class ServiceModule {

    @Provides
    @ServiceScope
    fun serviceUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = RxExecutor(Schedulers.io(), observeScheduler, "service")
}