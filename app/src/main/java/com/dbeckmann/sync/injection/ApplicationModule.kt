package com.dbeckmann.sync.injection

import android.app.Application
import android.content.Context
import android.os.Looper
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.rxexecutor.RxExecutor
import com.dbeckmann.sync.data.greendao.DaoMaster
import com.dbeckmann.sync.data.greendao.DaoSession
import com.dbeckmann.sync.data.task.TaskManager
import com.dbeckmann.sync.injection.annotation.ObserveScheduler
import com.dbeckmann.sync.injection.annotation.SqliteOperation
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Created by daniel on 22.04.2018.
 */

@Module
class ApplicationModule(private val mApplication: Application) {
    @Provides
    @Singleton
    fun providesContext(): Context {
        return mApplication.applicationContext
    }

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return mApplication
    }

    @Provides
    @Singleton
    fun provideDaoSession(context: Context) : DaoSession {
        val helper = DaoMaster.DevOpenHelper(context, "sync-db")
        val db = helper.writableDb
        return DaoMaster(db).newSession()
    }

    @Provides
    @Singleton
    @ObserveScheduler
    fun asyncMainThreadScheduler() : Scheduler {
        val asyncMainThreadScheduler = AndroidSchedulers.from(Looper.getMainLooper(), true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { asyncMainThreadScheduler }

        return asyncMainThreadScheduler
    }

    @Provides
    @Singleton
    @SqliteOperation // sqlite operation must be done more strict on limited thread # number
    fun databaseUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = RxExecutor(Schedulers.from(Executors.newSingleThreadExecutor()), observeScheduler, "database")

    @Provides
    @Singleton
    fun defaultUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = RxExecutor(Schedulers.io(), observeScheduler, "application")

    @Provides
    @Singleton
    fun taskManager() : TaskManager = TaskManager()
}