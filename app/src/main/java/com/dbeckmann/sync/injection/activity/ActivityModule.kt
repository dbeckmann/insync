package com.dbeckmann.sync.injection.activity

import androidx.fragment.app.FragmentActivity
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.rxexecutor.LifecycleAwareRxExecutor
import com.dbeckmann.sync.injection.annotation.ActivityScope
import com.dbeckmann.sync.injection.annotation.ActivityUseCaseExecutor
import com.dbeckmann.sync.injection.annotation.ObserveScheduler
import com.dbeckmann.sync.injection.activity.executor.ExecutorWrapperModule
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * Created by daniel on 10.12.2017.
 */

@Module(includes = [ExecutorWrapperModule::class])
class ActivityModule(private var fragmentActivity: FragmentActivity) {

    @Provides
    fun providesFragmentActivity(): FragmentActivity {
        return fragmentActivity
    }

    @Provides
    @ActivityUseCaseExecutor
    @ActivityScope
    fun activityUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = LifecycleAwareRxExecutor(Schedulers.io(), observeScheduler, "activity", fragmentActivity.lifecycle)
}
