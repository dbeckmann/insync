package com.dbeckmann.sync.views.gallery;

/**
 * Created by daniel on 02.08.2017.
 */

public class GalleryState implements GalleryContract.State {
    private long mStorageId;

    public GalleryState(long storageId) {
        mStorageId = storageId;
    }

    @Override
    public long getStorageId() {
        return mStorageId;
    }
}
