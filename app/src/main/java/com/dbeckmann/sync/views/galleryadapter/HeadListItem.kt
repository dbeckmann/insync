package com.dbeckmann.sync.views.galleryadapter

/**
 * Created by daniel on 15.05.2018.
 */
class HeadListItem(
        viewType: Int,
        groupId: Long,
        isSelected: Boolean,
        title: String
) : GroupListItem (
        viewType,
        groupId,
        isSelected
){

}