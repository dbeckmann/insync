package com.dbeckmann.sync.views.main

import com.dbeckmann.sync.views.BasePresenter
import com.dbeckmann.sync.views.BaseView
import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.base.tiles.BaseTilePresenter
import com.dbeckmann.sync.views.base.tiles.TileItem
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.TilePresenter

/**
 * Created by daniel on 10.07.2017.
 */

class StorageContract {

    interface View : BaseView {

        fun showSetupView()
        fun showStorageSettingsView()
        fun getListAdapterUpdater() : ListAdapterUpdater
    }

    interface Presenter : BasePresenter<View>, BaseTilePresenter {
        fun subscribe(view: View, storageId: Long)
    }

}
