package com.dbeckmann.sync.views.main.recyclerview.tile

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.fileoperation.GetStorageSpaceFromStorageEntity
import com.dbeckmann.domain.usecase.localfile.FindByStorageId
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import javax.inject.Inject

/**
 * Created by daniel on 05.05.2018.
 */
class StorageSpaceTile @Inject constructor(
        private var getStorageSpace: GetStorageSpaceFromStorageEntity,
        private var findByStorageId: FindByStorageId
) : Tile<StorageSpaceTileData> {

    private var storage: StorageEntity? = null

    companion object {
        const val ITEM_VIEW_TYPE = R.id.storageSpaceTileId
    }

    fun setStorage(storage: StorageEntity) {
        this.storage = storage
    }

    override fun getItemViewType(): Int {
        return ITEM_VIEW_TYPE
    }

    override fun getData(callback: UseCaseCallback<StorageSpaceTileData>) {
        storage?.let { storage ->

            var maxSpace: Long = 0
            var freeSpace: Long = 0
            var usedSpace: Long = 0

            getStorageSpace.execute(storage,
                    { storageSpace ->
                        maxSpace = storageSpace.maxSpace
                        freeSpace = storageSpace.freeSpace

                        findByStorageId.execute(storage,
                                {
                                    val localFiles = it
                                    localFiles.forEach {
                                        usedSpace += it.size
                                    }

                                    callback.onSuccess(StorageSpaceTileData(maxSpace, freeSpace, usedSpace))
                                },
                                {
                                    callback.onSuccess(StorageSpaceTileData(maxSpace, freeSpace, usedSpace))
                                })
                    },
                    {
                        callback.onError(it)
                    })

            /*
            Single
                    .concat(getStorageSpace.toSingle(it), findByStorageId.toSingle(it))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it is StorageSpace) {
                                    maxSpace = it.maxSpace
                                    freeSpace = it.freeSpace
                                }

                                if (it is List<*>) {
                                    val localFiles = it as List<LocalFileEntity>
                                    localFiles.forEach {
                                        usedSpace += it.size
                                    }
                                }
                            },
                            {
                                callback.onError(it)
                            },
                            {
                                callback.onSuccess(StorageSpaceTileData(maxSpace, freeSpace, usedSpace))
                            })*/
        }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_storage_usage, parent, false)
        return StorageSpaceTileViewHolder(view)
    }
}

data class StorageSpaceTileData(
        val maxSpace: Long,
        val freeSpace: Long,
        val usedSpace: Long
)

