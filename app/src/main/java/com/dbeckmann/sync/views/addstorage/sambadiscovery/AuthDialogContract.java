package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import com.dbeckmann.domain.model.Host;
import com.dbeckmann.sync.views.BasePresenter;
import com.dbeckmann.sync.views.BaseView;

/**
 * Created by daniel on 20.07.2017.
 */

public class AuthDialogContract {

    public interface View extends BaseView{

        void showProgress();
        void hideProgress();
        void showErrorAuthFailed();
        void onAuthenticateSuccess();

    }

    public interface Presenter extends BasePresenter<View> {

        void subscribe(View view, Host host);
        void authenticateUser(String userName, String password);

    }

}
