package com.dbeckmann.sync.views.storagesetting

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.dbeckmann.sync.R

class StorageSettingActivity : AppCompatActivity() {

    companion object {
        const val KEY_STORAGE_ID = "keyStorageId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_storage_setting)

        supportActionBar?.also {
            it.title = "Storage Settings"
            it.setDisplayHomeAsUpEnabled(true)
        }

        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContent, StorageSettingFragment.newInstance(intent.getLongExtra(KEY_STORAGE_ID, -1)))
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}