package com.dbeckmann.sync.views.gallery

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.backup.BackupFiles
import com.dbeckmann.domain.usecase.backup.BackupFilesParameter
import com.dbeckmann.domain.usecase.localfile.FindAllSetBackupFlag
import com.dbeckmann.domain.usecase.localfile.InsertOrUpdateLocalFile
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.sync.data.backup.BackupLocal
import com.dbeckmann.sync.data.backup.BackupService
import com.dbeckmann.sync.injection.annotation.RunAsService
import com.dbeckmann.sync.views.galleryadapter.ActionModePayload
import com.dbeckmann.sync.views.galleryadapter.BackupCompletePayload
import com.dbeckmann.sync.views.galleryadapter.BackupStartedPayload
import com.dbeckmann.sync.views.galleryadapter.BaseGalleryRecyclerViewAdapter
import com.dbeckmann.sync.views.galleryadapter.BlacklistPayload
import com.dbeckmann.sync.views.galleryadapter.GroupListItem
import com.dbeckmann.sync.views.galleryadapter.HeadListItem
import com.dbeckmann.sync.views.galleryadapter.ListAdapter
import com.dbeckmann.sync.views.galleryadapter.ListHolder
import com.dbeckmann.sync.views.galleryadapter.MediaListItem
import com.dbeckmann.sync.views.galleryadapter.SelectUnselectPayload
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by daniel on 01.08.2017.
 */

class GalleryPresenter @Inject constructor(
        private val findStorageById: FindStorageById,
        private val findAll : FindAllSetBackupFlag,
        private val insertOrUpdateLocalFile: InsertOrUpdateLocalFile,
        @RunAsService private val backupFiles: BackupFiles
): GalleryContract.Presenter, ListAdapter {

    private lateinit var view: GalleryContract.View
    private lateinit var storage: StorageEntity

    private var localFiles: MutableList<LocalFileEntity> = mutableListOf()

    private var listItems: MutableList<GroupListItem> = mutableListOf()
    private var groupedListItems: MutableMap<Long, MutableList<MediaListItem>> = mutableMapOf()
    private var groupedListPositions: MutableMap<Long, MutableList<Int>> = mutableMapOf()
    private var selectedItemPositions: MutableList<Int> = mutableListOf()

    private var isActionMode = false
    private var isBackupRequestRunning = false

    private val disposables: CompositeDisposable = CompositeDisposable()

    companion object {
        private const val TAG = "GalleryPresenter"
    }

    override fun subscribe(view: GalleryContract.View) {
        this.view = view
    }

    override fun subscribe(view: GalleryContract.View, state: GalleryContract.State?) {
        subscribe(view)

        if (state != null) {
            findStorageById.executeBlocking(state.storageId) {
                storage = it
            }

            getLocalFiles()
        }
    }

    override fun unsubscribe() {
        disposables.clear()
    }

    private fun initListener() {
        // listen for completed file uploads
        disposables.add(BackupLocal.fileBackupStateSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.storage.id == storage.id) {
                        when (it.state) {
                            BackupLocal.BackupState.COMPLETED -> setItemAsCompleted(getPositionByLocalFile(it.localFile))
                        }
                    }
                })

        disposables.add(BackupService.queueSubject
                .subscribe {
                    if (it.containsKey(storage.id)) {
                        it[storage.id]?.forEach { queueState ->
                            when(queueState.state) {
                                BackupLocal.BackupState.QUEUED -> setItemAsQueued(getPositionByLocalFile(queueState.file))
                                BackupLocal.BackupState.CANCELED -> setItemAsCanceled(getPositionByLocalFile(queueState.file))
                                BackupLocal.BackupState.FAILED -> setItemAsCanceled(getPositionByLocalFile(queueState.file))
                            }
                        }
                    }
                })
    }

    private fun getLocalFiles() {
        findAll.execute(storage,
                {
                    localFiles.addAll(it)

                    Single.fromCallable {
                        createListItems(it)
                    }
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view.getListAdapterUpdater().notifyDataSetChanged()
                                        initListener()
                                    },
                                    {

                                    })

                },
                {
                    // no images found
                })
    }

    override fun getState(): GalleryContract.State? {
        return null
    }

    override fun onItemClick(pos: Int) {
        if (isActionMode) {
            selectUnselectItem(pos)
        } else {
            if (listItems[pos] is MediaListItem) {
                view.showImageView((listItems[pos] as MediaListItem).localFile)
            }
        }
    }

    override fun onItemLongClick(pos: Int) {
        if (!isActionMode) {
            view.enableActionMode()
            selectUnselectItem(pos)
        }
    }

    override fun onActionModeDisabled() {
        isActionMode = false
        resetSelectedItems()
        notifyListActionModeChanged()
    }

    override fun onActionModeEnabled() {
        isActionMode = true
        notifyListActionModeChanged()
    }

    override fun onBackupCollectionSelected() {
        isBackupRequestRunning = true

        backupFiles.execute(BackupFilesParameter(storage, getSelectedLocalFiles()))

        // set files as processing and unselect items
        selectedItemPositions.forEach {
            if (listItems[it] is MediaListItem) {
                (listItems[it] as MediaListItem).isProcessing = true
                view.getListAdapterUpdater().notifyItemChanged(it, BackupStartedPayload())
            }
        }

        resetSelectedItems()
        view.showNumSelected(0)
    }

    override fun onBlacklistCollectionSelected() {
        selectedItemPositions.forEach {pos ->
            val item = listItems[pos]

            if (item is MediaListItem) {
                item.localFile.neverBackup = !item.localFile.neverBackup
                view.getListAdapterUpdater().notifyItemChanged(pos, BlacklistPayload())
                insertOrUpdateLocalFile.execute(item.localFile)
            }
        }
    }

    override fun onBindViewHolder(item: ListHolder, pos: Int) {
        item.fillData(listItems[pos])
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun getItemViewType(pos: Int): Int {
        return listItems[pos].viewType
    }

    override fun isActionModeEnabled(): Boolean {
        return isActionMode
    }

    override fun selectUnselectItem(pos: Int) {
        if (!selectedItemPositions.contains(pos)) {
            selectItem(pos)
        } else {
            unselectItem(pos)
        }

        view.showNumSelected(getSelectedLocalFiles().size)
    }

    override fun getListItem(pos: Int): GroupListItem {
        return listItems[pos]
    }

    private fun getSelectedLocalFiles() : List<LocalFileEntity> {
        val selectedFiles: MutableList<LocalFileEntity> = mutableListOf()

        selectedItemPositions.forEach {
            if (listItems[it] is MediaListItem) {
                selectedFiles.add((listItems[it] as MediaListItem).localFile)
            }
        }

        return selectedFiles
    }

    private fun resetSelectedItems() {
        selectedItemPositions.forEach {
            listItems[it].isSelected = false
        }

        selectedItemPositions.clear()
    }

    private fun unselectItem(pos: Int) {
        val item = listItems[pos]
        item.isSelected = false

        removeItemFromSelectedItemPositions(pos)

        if (item is HeadListItem) {
            unselectGroupItems(item.groupId)
        }

        if (item is MediaListItem) {
            val selectedGroupPositions = getSelectedPositionsForGroupId(item.groupId) as ArrayList<Int>

            if (selectedGroupPositions.size < getPositionsByGroupId(item.groupId).size) {
                val groupIdPosition = getPositionForGroupId(item.groupId)
                listItems[groupIdPosition].isSelected = false
                removeItemFromSelectedItemPositions(groupIdPosition)
            }
        }

        view.getListAdapterUpdater().notifyItemChanged(pos, SelectUnselectPayload())
    }

    private fun getSelectedPositionsForGroupId(groupId: Long): List<Int> {
        val selectedPostionsForGroupId = ArrayList<Int>()

        for (pos in getPositionsByGroupId(groupId)) {
            if (selectedItemPositions.contains(pos)) {
                selectedPostionsForGroupId.add(pos)
            }
        }

        return selectedPostionsForGroupId
    }

    private fun unselectGroupItems(groupId: Long) {
        unselectMultipleItems(getPositionsByGroupId(groupId))
    }

    private fun unselectMultipleItems(positions: List<Int>) {
        for (position in positions) {
            unselectItem(position)
        }
    }

    private fun selectItem(pos: Int) {
        val item = listItems[pos]

        if (item is MediaListItem) {
            if (item.localFile.hasBackup || item.isProcessing) {
                return
            }
        }

        item.isSelected = true
        addItemToSelectedItemPositions(pos)

        if (item is HeadListItem) {
            selectGroupItems(item.groupId)
        }

        if (item is MediaListItem) {
            if (selectedItemPositions.containsAll(getPositionsByGroupId(item.groupId))) {
                val groupIdPosition = getPositionForGroupId(item.groupId)
                listItems[groupIdPosition].isSelected = true
                addItemToSelectedItemPositions(groupIdPosition)
            }
        }

        view.getListAdapterUpdater().notifyItemChanged(pos, SelectUnselectPayload())
    }

    private fun getPositionForGroupId(groupId: Long): Int {
        for (i in listItems.indices) {
            if (listItems[i] is HeadListItem) {
                if ((listItems[i] as HeadListItem).groupId == groupId) {
                    return i
                }
            }
        }

        return -1
    }

    private fun getPositionsByGroupId(groupId: Long): MutableList<Int> {
        return groupedListPositions[groupId]!!
    }

    private fun selectGroupItems(groupId: Long) {
        selectMultipleItems(getPositionsByGroupId(groupId))
    }

    private fun selectMultipleItems(positions: List<Int>) {
        for (position in positions) {
            selectItem(position)
        }
    }

    private fun addItemToSelectedItemPositions(position: Int) {
        if (!selectedItemPositions.contains(position)) {
            selectedItemPositions.add(position)
        } else {
            return
        }
    }

    private fun removeItemFromSelectedItemPositions(position: Int) {
        if (selectedItemPositions.contains(position)) {
            selectedItemPositions.remove(position)
        }
    }

    private fun notifyListActionModeChanged() {
        val diffMargin = 10

        var pos = view.getListAdapterUpdater().findFirstVisibleItemPosition() - diffMargin

        if (pos < 0) {
            pos = 0
        }

        var count = (listItems.size - pos) + 1

        view.getListAdapterUpdater().notifyItemRangeChanged(pos, count, ActionModePayload())
    }

    fun getPositionByLocalFile(localFile: LocalFileEntity): Int {
        return listItems.indexOfFirst {
            it is MediaListItem && it.localFile.id == localFile.id
        }
    }

    private fun fileIsProcessing(localFile: LocalFileEntity) : Boolean {
        val item = listItems[getPositionByLocalFile(localFile)] as MediaListItem
        return item.isProcessing
    }

    private fun setItemAsQueued(pos: Int) {
        if (!(listItems[pos] as MediaListItem).isProcessing) {
            (listItems[pos] as MediaListItem).isProcessing = true
            view.getListAdapterUpdater().notifyItemChanged(pos, BackupStartedPayload())
        }
    }

    private fun setItemAsCompleted(pos: Int) {
        (listItems[pos] as MediaListItem).localFile.hasBackup = true
        view.getListAdapterUpdater().notifyItemChanged(pos, BackupCompletePayload())
    }

    private fun setItemAsCanceled(pos: Int) {
        if ((listItems[pos] as MediaListItem).isProcessing) {
            (listItems[pos] as MediaListItem).isProcessing = false
            view.getListAdapterUpdater().notifyItemChanged(pos, BackupStartedPayload())
        }
    }

    private fun createListItems(localFiles: List<LocalFileEntity>) {
        val group = groupByDay(localFiles)
        val iterator = group.entries.iterator()

        while (iterator.hasNext()) {
            val entry = iterator.next()
            val day = entry.key


            // add head
            listItems.add(HeadListItem(
                    BaseGalleryRecyclerViewAdapter.VIEW_TYPE_HEAD_ITEM,
                    day,
                    false,
                    day.toString()))

            // add media
            for (localFile in entry.value) {
                val mediaItem = MediaListItem(
                        viewType = BaseGalleryRecyclerViewAdapter.VIEW_TYPE_MEDIA_ITEM,
                        groupId = day,
                        isSelected = false,
                        isProcessing = false,
                        localFile = localFile)

                listItems.add(mediaItem)


                if (groupedListItems.containsKey(day)) {
                    groupedListItems[day]?.add(mediaItem)
                } else {
                    val tmp: MutableList<MediaListItem> = mutableListOf()
                    tmp.add(mediaItem)
                    groupedListItems[day] = tmp
                }

                if (groupedListPositions.containsKey(day)) {
                    groupedListPositions[day]?.add(listItems.indexOf(mediaItem))
                } else {
                    val intList: MutableList<Int> = mutableListOf()
                    intList.add(listItems.indexOf(mediaItem))
                    groupedListPositions[day] = intList
                }
            }
        }

        group.clear()
    }

    private fun groupByDay(localFiles: List<LocalFileEntity>): MutableMap<Long, MutableList<LocalFileEntity>> {
        val group: LinkedHashMap<Long, MutableList<LocalFileEntity>> = LinkedHashMap()

        for (localFile in localFiles) {
            if (group.containsKey(localFile.day)) {
                group[localFile.day]?.add(localFile)
            } else {
                val tmp = ArrayList<LocalFileEntity>()
                tmp.add(localFile)
                group[localFile.day] = tmp
            }
        }

        return group
    }
}
