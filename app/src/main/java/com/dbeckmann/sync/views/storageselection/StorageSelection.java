package com.dbeckmann.sync.views.storageselection;

import com.dbeckmann.sync.networking.samba.SambaFileOperation;

import java.util.ArrayList;

/**
 * Created by daniel on 06.07.17.
 */

public class StorageSelection implements StorageSelectionPresenter
{
	private StorageSelectionView mView;
	
	public StorageSelection(StorageSelectionView view)
	{
		mView = view;
	}
	
	@Override
	public void updateRemoteDriverIdentifierList()
	{
		ArrayList<String> driverIdentifierList = new ArrayList<>();
		driverIdentifierList.add(SambaFileOperation.IDENTIFIER);
		
		mView.setRemoteDriverIdentifierList(driverIdentifierList);
	}
	
	@Override
	public void onRemoteDriverIdentifierSelected(String driverIdentifier)
	{
		mView.switchToDriverSetupView(driverIdentifier);
	}
}
