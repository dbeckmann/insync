package com.dbeckmann.sync.views.galleryadapter

import android.graphics.Bitmap
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import java.io.File


/**
 * Created by daniel on 27.08.2017.
 */

class MediaViewHolder(itemView: View, viewHolderOnClickListener: ViewHolderOnClickListener) : AbstractGalleryViewHolder(itemView, viewHolderOnClickListener) {

    private var mMediaContainer: RelativeLayout = view.findViewById(R.id.rl_preview_media_cell)
    private var mMediaView: ImageView = view.findViewById(R.id.iv_preview_media)
    private var mSelectionContainer: RelativeLayout = view.findViewById(R.id.rl_selection_container)
    private var mItemNotSelectedIndicator: ImageView = view.findViewById(R.id.iv_item_not_selected)
    private var mItemSelectedIndicator: ImageView = view.findViewById(R.id.iv_item_selected)
    private var mBackupIndicatorContainer: RelativeLayout = view.findViewById(R.id.rl_backup_indicator_container)
    private var mHasBackup: View = view.findViewById(R.id.iv_item_is_backuped)
    private var mIsCurrentlyBackuping: View = view.findViewById(R.id.iv_item_is_currently_backuping)
    private var mNeverBackup: View = view.findViewById(R.id.iv_item_never_backup)
    private var videoIcon: View = view.findViewById(R.id.videoIcon)
    var imageContainer: RelativeLayout = view.findViewById(R.id.rl_image_container)

    override fun fillData(item: GroupListItem) {
        val mediaItem = item as MediaListItem

        setOnClickListener()
        loadMediaImage(mediaItem)
        showBackupIndicatorVisuals(mediaItem)
        showSelectionVisuals(mediaItem)
        showMediaType(mediaItem)
    }

    private fun loadMediaImage(mediaItem: MediaListItem) {
        Glide.with(mMediaContainer.context)
                .load(File(mediaItem.localFile.filePath))
                .into(mMediaView)
    }

    private fun setOnClickListener() {
        mMediaContainer.setOnClickListener { v -> mViewHolderOnClickListener.onClick(v) }

        mMediaContainer.setOnLongClickListener { v ->
            mViewHolderOnClickListener.onLongClick(v)
            true
        }
    }

    fun showBackupIndicatorVisuals(item: GroupListItem) {
        val mediaItem = item as MediaListItem

        mBackupIndicatorContainer.visibility = View.INVISIBLE

        mHasBackup.visibility = View.INVISIBLE
        mNeverBackup.visibility = View.INVISIBLE
        mIsCurrentlyBackuping.visibility = View.INVISIBLE

        fullColorImage()

        when {
            mediaItem.localFile.hasBackup -> {
                mBackupIndicatorContainer.visibility = View.VISIBLE
                mHasBackup.visibility = View.VISIBLE

                grayOutImage()
            }
            mediaItem.localFile.neverBackup -> {
                mBackupIndicatorContainer.visibility = View.VISIBLE
                mNeverBackup.visibility = View.VISIBLE

                redOutImage()
            }
            mediaItem.isProcessing -> {
                mBackupIndicatorContainer.visibility = View.VISIBLE
                mIsCurrentlyBackuping.visibility = View.VISIBLE
            }
        }
    }

    override fun showSelectionVisuals(listItem: GroupListItem) {
        val mediaItem = listItem as MediaListItem

        // update view for actionmode
        if (isActionModeEnabled && (!mediaItem.localFile.hasBackup && !mediaItem.isProcessing)) {

            /*val animation = AnimationUtils.loadAnimation(mSelectionContainer.context, android.R.anim.slide_in_left)
            mSelectionContainer.startAnimation(animation)*/

            mSelectionContainer.visibility = View.VISIBLE

            if (mediaItem.isSelected) {
                mItemNotSelectedIndicator.visibility = View.INVISIBLE
                mItemSelectedIndicator.visibility = View.VISIBLE
            } else {
                mItemNotSelectedIndicator.visibility = View.VISIBLE
                mItemSelectedIndicator.visibility = View.INVISIBLE
            }
        } else {
            mSelectionContainer.visibility = View.INVISIBLE
        }
    }

    private fun showMediaType(item: MediaListItem) {
        when (item.localFile.type) {
            LocalFileEntity.TYPE_VIDEO -> videoIcon.visibility = View.VISIBLE
            else -> videoIcon.visibility = View.INVISIBLE
        }
    }

    private fun redOutImage() {
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)  //0 means grayscale
        val cf = ColorMatrixColorFilter(matrix)
        mMediaView.colorFilter = cf
        mMediaView.imageAlpha = 128   // 128 = 0.5
        mMediaView.setBackgroundColor(itemView.resources.getColor(R.color.redOutTint))
    }

    private fun grayOutImage() {
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)  //0 means grayscale
        val cf = ColorMatrixColorFilter(matrix)
        mMediaView.colorFilter = cf
        mMediaView.imageAlpha = 128   // 128 = 0.5
    }

    private fun fullColorImage() {
        mMediaView.colorFilter = null
        mMediaView.imageAlpha = 255
        mMediaView.setBackgroundColor(itemView.resources.getColor(R.color.imageBackground))
    }

    private fun getThumbnail(file: LocalFileEntity) : Bitmap {
        val thumbnail = MediaStore.Images.Thumbnails.getThumbnail(
                view.context.contentResolver,
                file.internalId.toLong(),
                MediaStore.Images.Thumbnails.MINI_KIND,
                null
        )

        if (thumbnail != null) {
            return thumbnail
        } else {
            throw NoSuchElementException()
        }
    }
}
