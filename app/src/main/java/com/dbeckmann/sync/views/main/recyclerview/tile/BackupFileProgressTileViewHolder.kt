package com.dbeckmann.sync.views.main.recyclerview.tile

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import java.io.File

/**
 * Created by daniel on 09.05.2018.
 */
class BackupFileProgressTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private var progressBar: ProgressBar? = null
    private var image: ImageView? = null
    private var progressText: TextView? = null

    init {
        progressBar = view.findViewById(R.id.pb_determinate)
        progressText = view.findViewById(R.id.tv_progress_text)
        image = view.findViewById(R.id.iv_current_image)
    }

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback(
                {
                    val result = it as BackupFileProgressTileData
                    render(result)
                },
                {

                }))
    }

    private fun render(data: BackupFileProgressTileData) {
        if (data.thumbnail != null) {
            image?.setImageBitmap(data.thumbnail)
        } else {
            Glide.with(view.context)
                    .load(File(data.filePath))
                    .apply(RequestOptions.centerCropTransform())
                    .into(image)
        }

        progressText?.text = "copy file ${data.processed} of ${data.max}"

        progressBar?.max = data.max
        progressBar?.progress = data.processed
    }
}