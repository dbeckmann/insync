package com.dbeckmann.sync.views.main.recyclerview.tile

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import javax.inject.Inject

/**
 * Created by daniel on 09.05.2018.
 */
class ImageStateTile @Inject constructor(

) : Tile<ImageStateTileData> {

    private var storage: StorageEntity? = null

    fun setStorage(storage: StorageEntity) {
        this.storage = storage
    }

    override fun getItemViewType(): Int {
        return R.id.imageStateTileId
    }

    override fun getData(callback: UseCaseCallback<ImageStateTileData>) {
        storage?.let {
            callback.onSuccess(ImageStateTileData(it))
        }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_image_state, parent, false)
        return ImageStateTileViewHolder(view)
    }
}

data class ImageStateTileData(
        val storage: StorageEntity
)