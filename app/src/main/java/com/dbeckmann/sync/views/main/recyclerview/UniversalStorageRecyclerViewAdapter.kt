package com.dbeckmann.sync.views.main.recyclerview

import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.base.tiles.BaseTileRecyclerViewAdapter
import com.dbeckmann.sync.views.main.StorageContract

/**
 * Created by daniel on 05.05.2018.
 */
class UniversalStorageRecyclerViewAdapter(presenter: StorageContract.Presenter)
    : BaseTileRecyclerViewAdapter(presenter), ListAdapterUpdater {
    override fun findFirstVisibleItemPosition(): Int = 0
    override fun findLastVisibleItemPosition(): Int = 0
}