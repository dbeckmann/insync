package com.dbeckmann.sync.views.gallery

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.sync.views.BaseState
import com.dbeckmann.sync.views.BaseStatefulPresenter
import com.dbeckmann.sync.views.BaseView
import com.dbeckmann.sync.views.ListAdapterUpdater

/**
 * Created by daniel on 29.07.2017.
 */

class GalleryContract {

    interface View : BaseView {
        fun enableActionMode()
        fun disableActionMode()
        fun showImageView(localFile: LocalFileEntity)
        fun updateItem(localFile: LocalFileEntity)
        fun getListAdapterUpdater() : ListAdapterUpdater
        fun showNumSelected(num: Int)
    }

    interface State : BaseState {
        val storageId: Long
    }

    interface Presenter : BaseStatefulPresenter<View, State> {
        fun onItemClick(pos: Int)
        fun onItemLongClick(pos: Int)
        fun onActionModeDisabled()
        fun onActionModeEnabled()
        fun onBackupCollectionSelected()
        fun onBlacklistCollectionSelected()

        fun selectUnselectItem(pos: Int)

    }

}
