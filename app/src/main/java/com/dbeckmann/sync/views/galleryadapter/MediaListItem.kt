package com.dbeckmann.sync.views.galleryadapter

import com.dbeckmann.domain.model.LocalFileEntity

/**
 * Created by daniel on 15.05.2018.
 */
class MediaListItem(
        viewType: Int,
        groupId: Long,
        isSelected: Boolean,
        var isProcessing: Boolean = false,
        val localFile: LocalFileEntity
) : GroupListItem (
        viewType,
        groupId,
        isSelected
){
}