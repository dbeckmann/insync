package com.dbeckmann.sync.views.addstorage.finalizeaddstorage

import android.util.Log
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.domain.usecase.fileoperation.CreateFolder
import com.dbeckmann.domain.usecase.fileoperation.CreateFolderParameter
import com.dbeckmann.domain.usecase.fileoperation.GetStorageSpace
import com.dbeckmann.domain.usecase.fileoperation.GetStorageSpaceParameter
import com.dbeckmann.domain.usecase.fileoperation.HasNoConfigFile
import com.dbeckmann.domain.usecase.fileoperation.WriteConfigFile
import com.dbeckmann.domain.usecase.fileoperation.WriteConfigFileParameter
import com.dbeckmann.domain.usecase.storage.InsertOrUpdateStorage
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.rxexecutor.toSingle
import com.dbeckmann.sync.data.persistance.RemoteConfigurationFile
import com.dbeckmann.sync.networking.Constants
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by daniel on 23.07.2017.
 */

class FinalizeAddStoragePresenter @Inject constructor(
    private val configuration: Configuration,
    private val getStorageSpace: GetStorageSpace,
    private val createFolder: CreateFolder,
    private val writeConfigFile: WriteConfigFile,
    private val hasNoConfigFile: HasNoConfigFile,
    private val insertOrUpdateStorage: InsertOrUpdateStorage
) : FinalizeAddStorageContract.Presenter {

    private lateinit var view: FinalizeAddStorageContract.View
    private lateinit var storage: StorageEntity
    private lateinit var path: Path

    private val mCompositeDisposable = CompositeDisposable()
    private var mIsAlreadyInUse = false

    companion object {
        private const val TAG = "FinalizePresenter"
    }

    override fun subscribe(view: FinalizeAddStorageContract.View) {
        this.view = view
    }

    override fun subscribe(view: FinalizeAddStorageContract.View, state: FinalizeAddStorageContract.State) {
        subscribe(view)

        storage = state.storage
        path = state.path

        initialize()
    }

    override fun unsubscribe() {
        Log.d(TAG, "unsubscribe")
        mCompositeDisposable.dispose()
    }

    override fun getState(): FinalizeAddStorageContract.State {
        return FinalizeAddStorageState(storage, path)
    }

    private fun initialize() {
        view.showGetStorageInfoProgress()

        view.setHostName(storage.host)
        view.setPathName(path.fullpath)
        view.setWifiSsid(configuration.getWifiSsid())

        getStorageSpace.execute(GetStorageSpaceParameter(storage, path), object : UseCaseCallback<StorageSpace> {
            override fun onSuccess(result: StorageSpace) {
                view.setStorageFreeSpace(result.freeSpace)
                view.hideGetStorageInfoProgress()
            }

            override fun onError(throwable: Throwable) {
                view.hideGetStorageInfoProgress()
                view.showStorageNotReachableError()
            }
        })
    }

    override fun onFinalizeConfirmed() {
        Log.d(TAG, "onFinalizeConfirmed")

        // process new storage + settings
        storage.apply{
            homeWifiSsid = configuration.getWifiSsid()
            homeWifiBssid = configuration.getWifiBssid()
            rootFolder = path.fullpath
        }

        insertOrUpdateStorage.executeBlocking(storage, object : UseCaseCallback<StorageEntity> {
            override fun onSuccess(result: StorageEntity) {
                storage = result
            }

            override fun onError(throwable: Throwable) {
                //
            }
        })

       /* storage!!.homeWifiSsid = mConfiguration!!.wifiSsid
        storage!!.homeWifiBssid = mConfiguration!!.wifiBssid
        storage = StorageRepository.insertOrUpdate(storage)

        val setting = StorageSetting()

        setting.needHomeWifi = view!!.getNeedsHomeWifi()
        setting.needWifi = view!!.getNeedsWifi()
        setting.needCharging = view!!.getNeedsCharging()
        setting.autoBackupMode = view!!.getBackupMode()
        setting.storageId = storage!!.id!!

        StorageSettingRepository.insertOrUpdate(setting)*/

        // process conf file
        val newConfFile = RemoteConfigurationFile.Builder()
                .setDeviceModel(configuration.getDeviceModel())
                .setAndroidId(configuration.getAndroidId())
                .setStorageId(storage.id!!)
                .build()

        val createFolderSingle = createFolder.toSingle(CreateFolderParameter(storage, path, Constants.REMOTE_BACKUP_FOLDER_NAME))
        val writeConfigFileSingle = writeConfigFile.toSingle(WriteConfigFileParameter(storage, path, newConfFile.toJsonObject().toString().toByteArray()))
        val getStorageSpaceSingle = getStorageSpace.toSingle(GetStorageSpaceParameter(storage, path)).doOnSuccess {
            storage.cacheMaxSpace = it.maxSpace
            insertOrUpdateStorage.execute(storage)
        }

        Single
                .concat(createFolderSingle, writeConfigFileSingle, getStorageSpaceSingle)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.onFinalizeFinished()
                        },
                        {
                            it.printStackTrace()
                            view.showFinalizeError()
                        })


        /*
        // do remote finalize operations
        val getStorageSpaceData = mRxRemoteFileOperation!!.getStorageSpaceData(storage)
                .doOnNext { response ->
                    Log.d(TAG, "getStorageSpaceData success")
                    val storage = response.storage
                    storage.cacheMaxSpace = response.maxSpace
                    storage.cacheUsedSpace = response.usedSpace
                    storage.cacheUsedBackupSpace = response.usedBackupSpace
                    StorageRepository.insertOrUpdate(storage)
                }

        val createFolder = mRxRemoteFileOperation!!.createFolder(mDriver, mPath, AbstractRemoteFileOperationDriver.REMOTE_BACKUP_FOLDER_NAME)
        val writeConfigFile = mRxRemoteFileOperation!!.writeToConfigFile(mDriver, mPath, newConfFile.toJsonObject()!!.toString().toByteArray())

        mCompositeDisposable.add(
                Observable
                        .concat(createFolderSingle, writeConfigFile, getStorageSpaceData)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith<>(object : DisposableObserver<Any>() {
                            override fun onNext(@NonNull o: Any) {
                                Log.d(TAG, "onNext")
                            }

                            override fun onError(@NonNull e: Throwable) {
                                Log.e(TAG, "onError")
                                view!!.showFinalizeError()
                            }

                            override fun onComplete() {
                                Log.d(TAG, "onComplete")
                                view!!.onFinalizeFinished()
                            }
                        }))*/
    }
}
