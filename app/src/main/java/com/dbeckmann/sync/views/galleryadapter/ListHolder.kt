package com.dbeckmann.sync.views.galleryadapter

/**
 * Created by daniel on 15.05.2018.
 */
interface ListHolder {
    fun fillData(item: GroupListItem)
}