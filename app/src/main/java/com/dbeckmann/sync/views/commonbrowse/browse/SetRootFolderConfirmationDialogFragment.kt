package com.dbeckmann.sync.views.commonbrowse.browse

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.text.Html
import com.dbeckmann.domain.model.Path
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.mapping.PathMapping

/**
 * Created by daniel on 18.07.2017.
 */
class SetRootFolderConfirmationDialogFragment : DialogFragment() {
    private lateinit var alertDialog: AlertDialog
    private var callback: SetRootFolderConfirmationDialogFragmentCallback? = null
    private lateinit var path: Path

    companion object {
        const val TAG = "SetRootDialog"
        private const val KEY_PATH = "keyPath"

        fun newInstance(path: PathMapping): SetRootFolderConfirmationDialogFragment {
            val fragment = SetRootFolderConfirmationDialogFragment()
            val bundle = Bundle()
            bundle.putParcelable(KEY_PATH, path)

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        path = arguments?.getParcelable<PathMapping>(KEY_PATH) as Path
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.set_root_folder_dialog_title))
        val message = String.format(getString(R.string.set_root_folder_dialog_message), path.name)
        builder.setMessage(Html.fromHtml(message))
        builder.setNegativeButton(getString(R.string.dialog_cancel), null)
        builder.setPositiveButton(getString(R.string.set_root_folder_dialog_confirm)) { dialog, which -> callback?.onRootFolderConfirmed(path) }

        alertDialog = builder.create()

        return alertDialog
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)

        if (activity is SetRootFolderConfirmationDialogFragmentCallback) {
            callback = activity
        }
    }

    interface SetRootFolderConfirmationDialogFragmentCallback {
        fun onRootFolderConfirmed(path: Path)
    }
}
