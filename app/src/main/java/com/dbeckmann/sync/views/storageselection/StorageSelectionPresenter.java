package com.dbeckmann.sync.views.storageselection;

/**
 * Created by daniel on 06.07.17.
 */

public interface StorageSelectionPresenter
{
	void updateRemoteDriverIdentifierList();
	void onRemoteDriverIdentifierSelected(String driverIdentifier);
}
