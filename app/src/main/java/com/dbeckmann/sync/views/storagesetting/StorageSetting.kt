package com.dbeckmann.sync.views.storagesetting

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.localfile.DeleteBackupForStorage
import com.dbeckmann.domain.usecase.storage.DeleteStorage
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.sync.data.task.TaskManager
import javax.inject.Inject

class StorageSetting @Inject constructor(
        private val findStorage: FindStorageById,
        private val deleteStorage: DeleteStorage,
        private val taskManager: TaskManager,
        private val deleteBackupForStorage: DeleteBackupForStorage
): StorageSettingContract.Presenter {

    private lateinit var view: StorageSettingContract.View
    private lateinit var storage: StorageEntity

    override fun subscribe(view: StorageSettingContract.View) {
        this.view = view
    }

    fun subscribe(view: StorageSettingContract.View, storageId: Long) {
        subscribe(view)

        findStorage.execute(storageId) {
            storage = it
            updateViewData()
        }
    }

    override fun unsubscribe() {
        //
    }

    override fun onDelete() {
        view.showDeleteConfirm(storage)
    }

    override fun onDeleteConfirmed() {
        storage.id?.also {
            taskManager.stopTask(storage)

            deleteBackupForStorage.execute(storage)
            deleteStorage.execute(it) {
                view.closeView()
            }
        }
    }

    private fun updateViewData() {
        view.setHostName(storage.host)
        view.setRootFolder(storage.rootFolder ?: "")
        view.setDisplayName(storage.displayName)
        view.setBackupMode(storage.setting?.backupMode ?: 2)
        view.setHomeSsid(storage.homeWifiSsid ?: "")
    }
}