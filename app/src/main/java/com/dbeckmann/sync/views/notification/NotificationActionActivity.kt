package com.dbeckmann.sync.views.notification

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.dbeckmann.sync.views.main.MainActivity

/**
 * Created by daniel on 24.08.2017.
 */
class NotificationActionActivity : Activity() {

    companion object {
        private val TAG = "NotificationAction"
        const val ACTION_STOP_JOB_BY_ID = "actionStopJobById"
        const val ACTION_OPEN_MAIN_APP = "actionOpenMainApp"

        const val KEY_JOB_ID = "keyJobId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent != null) {
            when (intent.action) {
                ACTION_STOP_JOB_BY_ID -> {}
                ACTION_OPEN_MAIN_APP -> { startMainActivity() }
            }
        }

        finish()
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}
