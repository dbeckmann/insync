package com.dbeckmann.sync.views.addstorage.sambadiscovery

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.network.MapNetworkAuto
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.networking.samba.SambaFileOperation
import javax.inject.Inject

/**
 * Created by daniel on 20.07.2017.
 */

class SambaDiscoveryPresenter @Inject constructor(
       private val mapNetwork: MapNetworkAuto
) : SambaDiscoveryContract.Presenter {

    private lateinit var view: SambaDiscoveryContract.View

    private var hosts: List<Host> = listOf()
    private var isLoading: Boolean = false
    private var selectedHost: Host? = null


    companion object {
        private const val TAG = "SambaDiscoveryPrs"
    }

    override fun subscribe(view: SambaDiscoveryContract.View) {
        this.view = view
        refreshDiscoveryList()
    }

    override fun subscribe(view: SambaDiscoveryContract.View, state: SambaDiscoveryContract.State) {
        this.view = view
        hosts = state.hosts
        isLoading = state.isLoading
    }

    override fun unsubscribe() {
        Logger.d(TAG, "unsubscribe")
    }

    override fun getState(): SambaDiscoveryContract.State {
        return SambaDiscoveryState(hosts, isLoading)
    }

    override fun refreshDiscoveryList() {
        isLoading = true
        view.showProgress()

        mapNetwork.execute(object : UseCaseCallback<List<Host>> {
            override fun onSuccess(result: List<Host>) {
                hosts = result
                view.setDiscoveryItems(hosts.filter { it.isReachable })
                view.hideProgress()
                isLoading = false

                hosts.forEach {
                    Logger.d(TAG, "host ${it.hostName} mac: ${it.macAddress} secured: ${it.isSecured} reachable: ${it.isReachable}")
                }
            }

            override fun onError(throwable: Throwable) {
                view.hideProgress()
                isLoading = false
            }
        })
    }

    override fun onHostSelected(host: Host) {
        if (isLoading) {
            return
        }

        selectedHost = host

        if (host.isReachable && host.isSecured) {
            if (host.username != null && host.auth != null) {
                view.startBrowse(createStorage(host))
            } else {
                view.showAuthInput(host)
            }
        }

        if (host.isReachable && !host.isSecured) {
            view.startBrowse(createStorage(host))
        }
    }

    override fun onCredentialsReceived(userName: String, auth: String) {
        if (hosts.contains(selectedHost)) {
            val index = hosts.indexOf(selectedHost)

            hosts[index].username = userName
            hosts[index].auth = auth
            hosts[index].isSecured = false

            view.setDiscoveryItems(hosts)
            view.startBrowse(createStorage(selectedHost!!))
        }
    }

    override fun onAddHostManual() {
        view.showAddHostManual()
    }

    private fun createStorage(host: Host): StorageEntity {
        return StorageEntity(
                displayName = host.hostName,
                host = host.hostName,
                hostMacAddress = host.macAddress,
                connectionAddress = host.ipAddress,
                hostIpAddress = host.ipAddress,
                username = host.username,
                auth = host.auth,
                driverIdentifier = SambaFileOperation.IDENTIFIER
        )
    }
}
