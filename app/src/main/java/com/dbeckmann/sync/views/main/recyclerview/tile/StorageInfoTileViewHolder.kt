package com.dbeckmann.sync.views.main.recyclerview.tile

import android.view.View
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder

/**
 * Created by daniel on 09.05.2018.
 */
class StorageInfoTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private var label: TextView = view.findViewById(R.id.tv_label)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback(
                {
                    render(it as StorageInfoTile.StorageInfoTileData)
                },
                {
                    // do nothing
                })
        )
    }

    private fun render(result: StorageInfoTile.StorageInfoTileData) {
        label.text = result.displayName
    }
}