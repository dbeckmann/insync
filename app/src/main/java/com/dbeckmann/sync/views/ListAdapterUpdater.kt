package com.dbeckmann.sync.views

/**
 * Created by daniel on 07.04.2018.
 */
interface ListAdapterUpdater {
    fun notifyDataSetChanged()
    fun notifyItemRemoved(pos: Int)
    fun notifyItemChanged(pos: Int)
    fun notifyItemChanged(pos: Int, payload: Any?)
    fun notifyItemInserted(pos: Int)
    fun notifyItemRangeChanged(pos: Int, count: Int)
    fun notifyItemRangeChanged(pos: Int, count: Int, payload: Any?)

    fun findFirstVisibleItemPosition() : Int
    fun findLastVisibleItemPosition() : Int
}