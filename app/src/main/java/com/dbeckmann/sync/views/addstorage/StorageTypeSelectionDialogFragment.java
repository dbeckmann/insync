package com.dbeckmann.sync.views.addstorage;

import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dbeckmann.sync.R;
import com.dbeckmann.sync.networking.samba.SambaFileOperation;
import com.dbeckmann.sync.views.addstorage.sambadiscovery.SambaDiscoveryActivity;

import java.util.ArrayList;

/**
 * Created by daniel on 18.07.2017.
 */

public class StorageTypeSelectionDialogFragment extends DialogFragment {
    public static final String TAG = "StorageTypeDialog";

    private AlertDialog mAlertDialog;

    public static StorageTypeSelectionDialogFragment newInstance() {
        StorageTypeSelectionDialogFragment fragment = new StorageTypeSelectionDialogFragment();

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.new_storage_selection_title));
        mAlertDialog = builder.create();

        return mAlertDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_storage_type_selection, container, false);

        ListView storageTypeListView = (ListView) v.findViewById(R.id.lv_storage_type);

        BaseAdapter adapter = new StorageTypeListAdapter(getActivity());
        storageTypeListView.setAdapter(adapter);
        storageTypeListView.setOnItemClickListener((parent, view, position, id) -> {
            StorageTypeListItem item = (StorageTypeListItem) adapter.getItem(position);

            if (item.getIdentifier().equals(SambaFileOperation.IDENTIFIER)) {
                getActivity().startActivity(new Intent(getActivity(), SambaDiscoveryActivity.class));
            }

            dismiss();
        });

        mAlertDialog.setView(v);

        return v;
    }

    private class StorageTypeListAdapter extends BaseAdapter {

        ArrayList<StorageTypeListItem> mItems = new ArrayList<>();

        public StorageTypeListAdapter(@NonNull Context context) {
            initDriverList();
        }

        private void initDriverList() {
            mItems.add(new StorageTypeListItem(SambaFileOperation.IDENTIFIER, getString(R.string.storage_samba_title), R.drawable.ic_desktop_windows_black_24dp));

            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_storage_type_list, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.tv_row_type_title);
            ImageView icon = (ImageView) convertView.findViewById(R.id.iv_row_type);

            title.setText(mItems.get(position).getDisplayTitle());
            icon.setImageResource(mItems.get(position).getIconResourceId());

            return convertView;
        }
    }

    private class StorageTypeListItem {
        private String mIdentifier;
        private String mDisplayTitle;
        private int mIconResourceId;

        public StorageTypeListItem(String identifier, String displayTitle, int iconResourceId) {
            mIdentifier = identifier;
            mDisplayTitle = displayTitle;
            mIconResourceId = iconResourceId;
        }

        public String getIdentifier() {
            return mIdentifier;
        }

        public String getDisplayTitle() {
            return mDisplayTitle;
        }

        public int getIconResourceId() {
            return mIconResourceId;
        }
    }
}
