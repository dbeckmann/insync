package com.dbeckmann.sync.views.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import androidx.core.app.NotificationCompat;

import com.dbeckmann.sync.R;

/**
 * Created by daniel on 23.08.2017.
 */

public class CheckFilesNotification {
    public static final String CHANNEL_ID = "checkFilesChannel";
    public static final int NOTIFICATION_ID = 1;

    private Context mContext;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    public CheckFilesNotification(Context context) {
        mContext = context;

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext);
    }

    public void create() {
        mBuilder.setSmallIcon(R.drawable.shape_circle);
        mBuilder.setContentTitle("Backup");
        mBuilder.setContentText("Processing backup");
        mBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public Notification getNotification() {
        return mBuilder.build();
    }
}
