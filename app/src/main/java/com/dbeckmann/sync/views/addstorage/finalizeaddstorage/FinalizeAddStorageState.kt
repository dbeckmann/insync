package com.dbeckmann.sync.views.addstorage.finalizeaddstorage

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity


/**
 * Created by daniel on 23.07.2017.
 */

class FinalizeAddStorageState(override val storage: StorageEntity, override val path: Path) : FinalizeAddStorageContract.State
