package com.dbeckmann.sync.views.addstorage.sambadiscovery

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.usecase.network.AuthUserWithCredentials
import com.dbeckmann.domain.usecase.network.UserCredentials
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.data.Logger
import javax.inject.Inject

/**
 * Created by daniel on 20.07.2017.
 */

class AuthDialogPresenter @Inject constructor(
        private val authUser: AuthUserWithCredentials
) : AuthDialogContract.Presenter {

    private lateinit var host: Host
    private lateinit var view: AuthDialogContract.View

    companion object {
        private const val TAG = "AuthDialogPresenter"
    }

    override fun subscribe(view: AuthDialogContract.View) {
        this.view = view
    }

    override fun unsubscribe() {

    }

    override fun subscribe(view: AuthDialogContract.View, host: Host) {
        subscribe(view)
        this.host = host
    }

    override fun authenticateUser(userName: String, password: String) {
        Logger.d(TAG, "authenticateUser $userName")

        view.showProgress()

        authUser.execute(UserCredentials(host, userName, password), object : UseCaseCallback<Unit> {
            override fun onSuccess(result: Unit) {
                view.onAuthenticateSuccess()
                view.hideProgress()
            }

            override fun onError(throwable: Throwable) {
                view.showErrorAuthFailed()
                view.hideProgress()
            }
        })
    }
}
