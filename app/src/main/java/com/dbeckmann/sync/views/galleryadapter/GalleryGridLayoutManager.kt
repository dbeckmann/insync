package com.dbeckmann.sync.views.galleryadapter

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by daniel on 27.08.2017.
 */

class GalleryGridLayoutManager(context: Context, spanCount: Int) : GridLayoutManager(context, spanCount) {
    private var mAdapter: RecyclerView.Adapter<*>? = null

    init {

        newSpanSizeLookup()
    }

    private fun newSpanSizeLookup() {
        spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {

                when (mAdapter?.getItemViewType(position)) {
                    BaseGalleryRecyclerViewAdapter.VIEW_TYPE_MEDIA_ITEM -> return 1
                    //StorageRecyclerViewAdapter.VIEW_TYPE_DAY_ACTION -> return 1
                }

                return spanCount
            }
        }
    }

    fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        mAdapter = adapter
    }
}
