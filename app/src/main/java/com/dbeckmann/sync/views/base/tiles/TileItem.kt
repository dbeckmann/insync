package com.dbeckmann.sync.views.base.tiles

/**
 * Created by daniel on 05.05.2018.
 */
interface TileItem {
    fun setTile(tile: Tile<Any>)
}