package com.dbeckmann.sync.views.addstorage.finalizeaddstorage

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.views.BaseState
import com.dbeckmann.sync.views.BaseStatefulPresenter
import com.dbeckmann.sync.views.BaseView

/**
 * Created by daniel on 22.07.2017.
 */

class FinalizeAddStorageContract {

    interface View : BaseView {
        fun needsHomeWifi() : Boolean
        fun needsWifi() : Boolean
        fun needsCharging() : Boolean
        fun backupMode() : Int

        fun setHostName(hostName: String)
        fun setStorageFreeSpace(freeSpace: Long)
        fun setWifiSsid(ssid: String)
        fun setPathName(pathName: String)
        fun showStorageNotReachableError()
        fun showGetStorageInfoProgress()
        fun hideGetStorageInfoProgress()
        fun showAlreadyInUseView()
        fun showFinalizeError()
        fun onFinalizeFinished()
    }

    interface State : BaseState {

        val storage: StorageEntity
        val path: Path

    }

    interface Presenter : BaseStatefulPresenter<View, State> {
        fun onFinalizeConfirmed()
    }

}
