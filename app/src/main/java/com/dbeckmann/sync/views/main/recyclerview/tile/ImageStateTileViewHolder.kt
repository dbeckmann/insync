package com.dbeckmann.sync.views.main.recyclerview.tile

import android.content.Intent
import android.view.View
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.gallery.GalleryActivity
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder

/**
 * Created by daniel on 09.05.2018.
 */
class ImageStateTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private var container: View? = null

    init {
        container = view.findViewById(R.id.cv_go_gallery_container)
    }

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback(
                {
                    val result = it as ImageStateTileData
                    render(result)
                },
                {

                }))
    }

    private fun render(data: ImageStateTileData) {
        container?.setOnClickListener {
            val intent = Intent(view.context, GalleryActivity::class.java)
            intent.putExtra(GalleryActivity.KEY_STORAGE_ID, data.storage.id)

            view.context.startActivity(intent)
        }
    }
}