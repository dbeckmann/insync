package com.dbeckmann.sync.views.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R

/**
 * Created by daniel on 23.08.2017.
 */

class BackupFilesNotification(
        context: Context
) : NotificationBase(context) {

    fun create(storage: StorageEntity) {
        builder.setSmallIcon(R.drawable.ic_file_upload_white_24dp)
        builder.setContentTitle("Backup on ${storage.displayName}")
        builder.setContentText("copying files")
        builder.setProgress(0,0, true)
        builder.addAction(R.drawable.ic_cancel_black_24dp, "Cancel", getCancelAction())

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun indexing() {
        builder.setSmallIcon(R.drawable.ic_cached_white_24dp)
        builder.setContentTitle("Indexing files on device")
        builder.setProgress(0,0, true)
        builder.mActions.clear()

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun increment(max: Int, progress: Int) {
        builder.setProgress(max,progress, false)
        builder.setContentText("copy file $progress of $max")

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun done() {
        builder.setSmallIcon(R.drawable.ic_file_upload_white_24dp)
        builder.setContentTitle("Backup")
        builder.setContentText("completed")
        builder.setProgress(0,0, false)
        builder.mActions.clear()

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun error() {
        builder.setSmallIcon(R.drawable.ic_file_upload_white_24dp)
        builder.setContentTitle("Indexing Media Files")
        builder.setContentText("failed with an error")
        builder.setProgress(0,0, false)
        builder.mActions.clear()

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun dismiss() {
        notificationManager.cancel(NOTIFICATION_ID)
    }

    private fun getCancelAction() : PendingIntent {
        val intent = Intent(context, NotificationBroadcastReceiver::class.java).apply {
            action = NotificationBroadcastReceiver.ACTION_CANCEL_BACKUP
        }

        return PendingIntent.getBroadcast(context, 0, intent, 0)
    }
}
