package com.dbeckmann.sync.views.addstorage

import android.view.View
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.dbeckmann.sync.views.base.tiles.Tile

class StorageSourceTileViewholder(
        itemView: View,
        private val clickListener: ViewHolderOnClickListener
) : AbstractTileViewHolder(itemView) {

    private var container: View = itemView.findViewById(R.id.storageTile)
    private var title: TextView = itemView.findViewById(R.id.storageSourceTitle)
    private var descr: TextView = itemView.findViewById(R.id.storageSourceDescription)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback (
                {
                    render(it as StorageSourceTile.StorageSourceTileData)
                },
                {
                    //
                }
        ))
    }

    private fun render(data: StorageSourceTile.StorageSourceTileData) {
        title.text = data.title
        descr.text = data.description

        container.setOnClickListener {
            clickListener.onClick(data.identifier)
        }
    }
}