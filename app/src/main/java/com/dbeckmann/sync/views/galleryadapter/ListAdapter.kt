package com.dbeckmann.sync.views.galleryadapter

/**
 * Created by daniel on 15.05.2018.
 */
interface ListAdapter {

    fun onBindViewHolder(item: ListHolder, pos: Int)
    fun getItemCount() : Int
    fun getItemViewType(pos: Int) : Int
    fun isActionModeEnabled() : Boolean
    fun getListItem(pos: Int) : GroupListItem
}