package com.dbeckmann.sync.views.main.recyclerview.tile

import android.graphics.Bitmap
import android.provider.MediaStore
import android.text.Html
import android.text.format.DateUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.dbeckmann.sync.views.base.tiles.Tile
import java.io.File

/**
 * Created by daniel on 13.05.2018.
 */
class GalleryTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private var title: TextView? = null
    private var infoText: TextView? = null
    private var container4: View? = null
    private var container1: View? = null

    private var image4x1: ImageView? = null
    private var image4x2: ImageView? = null
    private var image4x3: ImageView? = null
    private var image4x4: ImageView? = null

    private var image1x1: ImageView? = null

    private lateinit var titleTemplate: String
    private lateinit var infoTextTemplate: String

    init {
        title = view.findViewById(R.id.tv_image_day_title)
        infoText = view.findViewById(R.id.tv_gallery_info_text)

        container4 = view.findViewById(R.id.image_container_images_4)
        container1 = view.findViewById(R.id.image_container_images_1)

        image4x1 = view.findViewById(R.id.iv_image_4_1)
        image4x2 = view.findViewById(R.id.iv_image_4_2)
        image4x3 = view.findViewById(R.id.iv_image_4_3)
        image4x4 = view.findViewById(R.id.iv_image_4_4)

        image1x1 = view.findViewById(R.id.iv_image_1_1)

        infoTextTemplate = view.context.getString(R.string.gallery_tile_info_text)
        titleTemplate = view.context.getString(R.string.gallery_tile_title)
    }

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback (
                {
                    val result = it as GalleryTileData
                    render(result)
                },
                {

                }))
    }

    private fun render(data: GalleryTileData) {
        title?.text = String.format(titleTemplate, DateUtils.getRelativeTimeSpanString(data.date * 1000, System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS))

        val info = String.format(infoTextTemplate, data.numUnstoredImages, data.numStoredImages)
        infoText?.text = Html.fromHtml(info)

        renderImages(data)
    }

    private fun renderImages(data: GalleryTileData) {
        container4?.visibility = View.GONE
        container1?.visibility = View.GONE

        if (data.numUnstoredImages >= 4) {
            container4?.visibility = View.VISIBLE

            renderSingleImage(image4x1, data.files[0])
            renderSingleImage(image4x2, data.files[1])
            renderSingleImage(image4x3, data.files[2])
            renderSingleImage(image4x4, data.files[3])
        }

        if (data.numUnstoredImages < 4) {
            container1?.visibility = View.VISIBLE

            renderSingleImage(image1x1, data.files[0])
        }
    }

    private fun renderSingleImage(view: ImageView?, file: LocalFileEntity) {
        val thumbnail = getThumbnail(file)

        if (thumbnail != null) {
            view?.setImageBitmap(thumbnail)
        } else {
            Glide.with(view?.context)
                    .load(File(file.filePath))
                    .apply(RequestOptions.centerCropTransform())
                    .into(view)
        }
    }

    private fun getThumbnail(file: LocalFileEntity) : Bitmap? {
        return MediaStore.Images.Thumbnails.getThumbnail(
                view.context.contentResolver,
                file.internalId.toLong(),
                MediaStore.Images.Thumbnails.MINI_KIND,
                null
        )
    }
}