package com.dbeckmann.sync.views;

/**
 * Created by daniel on 10.07.2017.
 */

public interface BasePresenter <V extends BaseView> {
    void subscribe(V view);
    void unsubscribe();
}
