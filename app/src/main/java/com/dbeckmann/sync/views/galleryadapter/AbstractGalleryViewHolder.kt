package com.dbeckmann.sync.views.galleryadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.View

import com.dbeckmann.sync.views.ViewHolderOnClickListener

/**
 * Created by daniel on 27.08.2017.
 */

abstract class AbstractGalleryViewHolder(
        val view: View,
        protected var mViewHolderOnClickListener: ViewHolderOnClickListener)
    : RecyclerView.ViewHolder(view), ListHolder {

    var isActionModeEnabled = false

    abstract fun showSelectionVisuals(listItem: GroupListItem)
}
