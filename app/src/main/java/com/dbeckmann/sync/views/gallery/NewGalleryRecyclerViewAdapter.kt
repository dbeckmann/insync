package com.dbeckmann.sync.views.gallery

import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.galleryadapter.*

/**
 * Created by daniel on 27.08.2017.
 */

class NewGalleryRecyclerViewAdapter(
        presenter: GalleryPresenter,
        viewHolderOnClickListener: ViewHolderOnClickListener)
    : BaseGalleryRecyclerViewAdapter(presenter, viewHolderOnClickListener)
