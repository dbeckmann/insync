package com.dbeckmann.sync.views;

/**
 * Created by daniel on 20.07.2017.
 */

public interface BaseStatefulPresenter<V extends BaseView, S extends BaseState> extends BasePresenter<V> {
    void subscribe(V view, S state);
    S getState();
}
