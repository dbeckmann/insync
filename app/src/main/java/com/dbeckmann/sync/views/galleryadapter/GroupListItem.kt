package com.dbeckmann.sync.views.galleryadapter

/**
 * Created by daniel on 15.05.2018.
 */
open class GroupListItem (
        val viewType: Int,
        val groupId: Long,
        var isSelected: Boolean
) {
}