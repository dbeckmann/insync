package com.dbeckmann.sync.views.galleryadapter

import android.text.format.DateUtils
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.ViewHolderOnClickListener

/**
 * Created by daniel on 27.08.2017.
 */
class HeadViewHolder(itemView: View, viewHolderOnClickListener: ViewHolderOnClickListener) : AbstractGalleryViewHolder(itemView, viewHolderOnClickListener) {
    private var mHeaderContainer: RelativeLayout = view.findViewById(R.id.rl_header_container)
    private var mDate: TextView = view.findViewById(R.id.tv_group_date)

    override fun fillData(item: GroupListItem) {
        val headItem = item as HeadListItem

        mHeaderContainer.setOnClickListener { v -> mViewHolderOnClickListener.onClick(v) }
        mHeaderContainer.setOnLongClickListener { v ->
            mViewHolderOnClickListener.onLongClick(v)
            true
        }

        mDate.text = DateUtils.getRelativeTimeSpanString(headItem.groupId * 1000, System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS)
    }

    override fun showSelectionVisuals(listItem: GroupListItem) {
        val headItem = listItem as HeadListItem

        if (headItem.isSelected) {
            mHeaderContainer.alpha = 0.5f
        } else {
            mHeaderContainer.alpha = 1f
        }
    }
}
