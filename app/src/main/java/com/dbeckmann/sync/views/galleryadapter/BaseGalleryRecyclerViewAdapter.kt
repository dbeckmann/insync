package com.dbeckmann.sync.views.galleryadapter

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.ViewHolderOnClickListener

/**
 * Created by daniel on 27.08.2017.
 */
abstract class BaseGalleryRecyclerViewAdapter(
        private val listAdapter: ListAdapter,
        private val mViewHolderOnClickListener: ViewHolderOnClickListener
) : RecyclerView.Adapter<AbstractGalleryViewHolder>(), ListAdapterUpdater {

    private lateinit var recyclerView: RecyclerView

    private var mediaImageWidth: Int = 250
    private var mediaImageHeight: Int = 250

    companion object {
        private const val TAG = "BaseGlryRecVAdpt"

        const val VIEW_TYPE_HEAD_ITEM = 1000
        const val VIEW_TYPE_MEDIA_ITEM = 1001
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractGalleryViewHolder {
        val view: View

        return when (viewType) {
            VIEW_TYPE_MEDIA_ITEM -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.cell_media, parent, false)
                MediaViewHolder(view, mViewHolderOnClickListener)
            }

            VIEW_TYPE_HEAD_ITEM -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.cell_header, parent, false)
                HeadViewHolder(view, mViewHolderOnClickListener)
            }

            else -> throw IllegalStateException("no viewtype found")
        }
    }

    override fun onBindViewHolder(holder: AbstractGalleryViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.size == 0) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.forEach {
                when (it) {
                    is SelectUnselectPayload -> {
                        holder.showSelectionVisuals(listAdapter.getListItem(position))
                    }
                    is ActionModePayload -> {
                        holder.isActionModeEnabled = listAdapter.isActionModeEnabled()
                        holder.showSelectionVisuals(listAdapter.getListItem(position))
                    }
                    is BackupCompletePayload, is BlacklistPayload -> {
                        if (holder is MediaViewHolder) {
                            holder.showBackupIndicatorVisuals(listAdapter.getListItem(position))
                        }
                    }
                    is BackupStartedPayload -> {
                        if (holder is MediaViewHolder) {
                            holder.showBackupIndicatorVisuals(listAdapter.getListItem(position))
                            holder.showSelectionVisuals(listAdapter.getListItem(position))
                        }
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: AbstractGalleryViewHolder, position: Int) {
        if (holder is MediaViewHolder) {
            holder.imageContainer.layoutParams.width = mediaImageWidth
            holder.imageContainer.layoutParams.height = mediaImageHeight
        }

        holder.isActionModeEnabled = listAdapter.isActionModeEnabled()
        listAdapter.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return listAdapter.getItemCount()
    }

    override fun getItemViewType(position: Int): Int {
        return listAdapter.getItemViewType(position)

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
        calculateMediaImageDimensions()
    }

    override fun findFirstVisibleItemPosition(): Int {
        return (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()
    }

    override fun findLastVisibleItemPosition(): Int {
        return (recyclerView.layoutManager as GridLayoutManager).findLastVisibleItemPosition()
    }

    private fun calculateMediaImageDimensions() {
        val displayMetrics = recyclerView.context.resources.displayMetrics

        val dpHeight = displayMetrics.heightPixels
        val dpWidth = displayMetrics.widthPixels

        val spanCount = (recyclerView.layoutManager as GridLayoutManager).spanCount
        val dim = dpWidth / spanCount

        mediaImageWidth = dim
        mediaImageHeight = dim
    }
}
