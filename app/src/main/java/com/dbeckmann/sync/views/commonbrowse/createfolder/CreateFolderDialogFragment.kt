package com.dbeckmann.sync.views.commonbrowse.createfolder

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.mapping.PathMapping
import com.dbeckmann.sync.data.mapping.StorageParcelable
import com.dbeckmann.sync.injection.Injector
import javax.inject.Inject

/**
 * Created by daniel on 21.07.2017.
 */

class CreateFolderDialogFragment : DialogFragment(), CreateFolderContract.View {

    private var mFolderNameEditText: EditText? = null
    private var mErrorInvalidFolderName: TextView? = null
    private var mErrorFolderNotCreated: TextView? = null

    private lateinit var mAlertDialog: AlertDialog
    private var mCallback: CreateFolderDialogFragmentCallback? = null

    @Inject lateinit var mPresenter: CreateFolderPresenter

    companion object {
        const val TAG = "CreateFolderFragment"
        private const val KEY_STORAGE = "keyStorage"
        private const val KEY_PATH = "keyPath"

        fun newInstance(storage: StorageParcelable, path: PathMapping): CreateFolderDialogFragment {
            val fragment = CreateFolderDialogFragment()

            val bundle = Bundle()
            bundle.putParcelable(KEY_STORAGE, storage)
            bundle.putParcelable(KEY_PATH, path)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Injector.injectActivityComponent(activity!!).inject(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mAlertDialog = AlertDialog.Builder(activity!!)
                .setTitle("Create Folder")
                .setPositiveButton("create", null)
                .setNegativeButton("cancel", null)
                .create()

        mAlertDialog.setOnShowListener { dialog ->
            val positiveButton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
            positiveButton.setOnClickListener { v -> mPresenter.onCreateFolder(mFolderNameEditText?.text.toString()) }
        }

        return mAlertDialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_create_folder, container, false)
        mAlertDialog.setView(v)

        mFolderNameEditText = v.findViewById(R.id.et_new_folder_name)
        mErrorFolderNotCreated = v.findViewById(R.id.tv_error_folder_not_created)
        mErrorInvalidFolderName = v.findViewById(R.id.tv_error_invalid_folder_name)

        val storage = arguments?.getParcelable<StorageParcelable>(KEY_STORAGE) as StorageEntity
        val path = arguments?.getParcelable<PathMapping>(KEY_PATH) as Path

        mPresenter.subscribe(this, storage, path)

        return v
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)

        Log.d(TAG, "onattach")

        if (activity is CreateFolderDialogFragmentCallback) {
            mCallback = activity
        }
    }

    override fun onCreateFolderSuccess(path: Path) {
        mCallback?.onFolderCreated(path)
        dismiss()
    }

    override fun showInvalidFoldernameError() {
        mErrorFolderNotCreated?.visibility = View.GONE
        mErrorInvalidFolderName?.visibility = View.VISIBLE
    }

    override fun showFolderCreateError() {
        mErrorFolderNotCreated?.visibility = View.VISIBLE
        mErrorInvalidFolderName?.visibility = View.GONE
    }

    interface CreateFolderDialogFragmentCallback {
        fun onFolderCreated(path: Path)
    }
}
