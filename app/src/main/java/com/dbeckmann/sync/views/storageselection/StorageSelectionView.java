package com.dbeckmann.sync.views.storageselection;

import java.util.ArrayList;

/**
 * Created by daniel on 06.07.17.
 */

public interface StorageSelectionView
{
	void setRemoteDriverIdentifierList(ArrayList<String> driverIdentifierList);
	void switchToDriverSetupView(String driverIdentifier);
}
