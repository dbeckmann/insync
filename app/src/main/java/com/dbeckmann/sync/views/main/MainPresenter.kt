package com.dbeckmann.sync.views.main

import androidx.lifecycle.ViewModel
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.backup.IndexFiles
import com.dbeckmann.domain.usecase.storage.FindAllStorages
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.data.backup.BackupService
import com.dbeckmann.sync.injection.annotation.RunAsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by daniel on 07.07.2017.
 */

class MainPresenter @Inject constructor(
        private val findAllStorages: FindAllStorages,
        @RunAsService private val indexFiles: IndexFiles
) : MainContract.Presenter, ViewModel() {

    private lateinit var view: MainContract.View
    private var storages: List<StorageEntity> = listOf()
    private var currentStorage: StorageEntity? = null

    private val disposables = CompositeDisposable()

    companion object {
        const val TAG = "Main"
    }

    override fun subscribe(view: MainContract.View) {
        this.view = view

        view.showSplashScreen()

        initIndexStateListener()
        doIndexFiles()
    }

    override fun unsubscribe() {
        disposables.clear()
    }

    private fun initIndexStateListener() {
        disposables.add(
                BackupService.indexingSubject
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            when (it) {
                                BackupService.IndexState.COMPLETED,
                                BackupService.IndexState.FAILED -> {
                                    view.hideSplashScreen()
                                    refreshStorageList()
                                }
                                else -> Unit
                            }
                        }
        )
    }

    override fun refreshStorageList() {
        findAllStorages.executeBlocking {
            storages = it

            if (currentStorage == null) {
                setCurrentStorage(storages[0])
            }

            view.setStorageList(storages)
        }
    }

    override fun subViewInitComplete() {

    }

    override fun onAddStorage() {
        Logger.d(TAG, "onAddStorage")
        view.showAddStorage()
    }

    override fun onStorageSettings() {
        currentStorage?.also {
            view.showStorageSettings(it)
        }
    }

    override fun setCurrentStorage(storage: StorageEntity) {
        currentStorage = storage
        view.showStorage(currentStorage!!)
    }

    override fun onStorageSelected(position: Int) {
        currentStorage = storages[position]
        view.showStorage(currentStorage!!)
    }

    private fun doIndexFiles() {
        indexFiles.execute()
    }
}
