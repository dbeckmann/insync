package com.dbeckmann.sync.views.commonbrowse.browse

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.fileoperation.GetRootPath
import com.dbeckmann.domain.usecase.fileoperation.ListPath
import com.dbeckmann.domain.usecase.fileoperation.ListPathParameter
import com.dbeckmann.domain.usecase.fileoperation.ReadConfigFile
import com.dbeckmann.domain.usecase.fileoperation.ReadConfigFileParameter
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.data.Logger
import java.io.FileNotFoundException
import javax.inject.Inject

/**
 * Created by daniel on 06.07.2017.
 */

class CommonBrowsePresenter @Inject constructor(
    private val listPath: ListPath,
    private val getRootPath: GetRootPath,
    private val readConfigFile: ReadConfigFile
) : CommonBrowseContract.Presenter {

    private lateinit var view: CommonBrowseContract.View
    private lateinit var mCurrentPath: Path
    private lateinit var storage: StorageEntity

    companion object {
        private const val TAG = "CommonBrowsePrs"
    }

    override fun subscribe(view: CommonBrowseContract.View) {
        this.view = view
    }

    override fun subscribe(view: CommonBrowseContract.View, state: CommonBrowseContract.State) {
        subscribe(view)

        storage = state.storage

        if (state.currentPath == null) {
            getRootPath.executeBlocking(storage, object : UseCaseCallback<Path> {
                override fun onSuccess(result: Path) {
                    mCurrentPath = result
                }

                override fun onError(throwable: Throwable) {
                    // not implemented here
                }
            })
        } else {
            setCurrentPath(state.currentPath!!)
        }

        refreshPathList()
    }

    override fun getState(): CommonBrowseContract.State {
        return CommonBrowseState(mCurrentPath, storage)
    }

    override fun unsubscribe() {
        Logger.d(TAG, "unsubscribe")
    }

    override fun refreshPathList() {
        listPath()
    }

    override fun setCurrentPath(path: Path) {
        if (!path.isWritable) {
            view.disableFolderActions()
        } else {
            view.enableFolderActions()
        }

        mCurrentPath = path
        listPath()
    }

    override fun onSetRootFolderSelected(path: Path?) {
        val rootFolderPath: Path = path ?: mCurrentPath

        // check if writeable

        // check if already used
        readConfigFile.execute(ReadConfigFileParameter(storage, rootFolderPath), object : UseCaseCallback<ByteArray> {
            override fun onSuccess(result: ByteArray) {
                view.showFolderInUseView(rootFolderPath)
            }

            override fun onError(throwable: Throwable) {
                // everything is fine ! we do not expect a config file here
                if (throwable is FileNotFoundException) {
                    view.showSetRootFolderConfirmation(rootFolderPath)
                }
            }
        })
    }

    override fun onSetRootFolderConfirmed(path: Path) {
        view.finalizeSetup(storage, path)
    }

    override fun onCreateFolderSelected() {
        view.showNewFolderInput(storage, mCurrentPath)
    }

    override fun onBackPressed() {
        mCurrentPath.parentPath?.let {
            setCurrentPath(it)
        } ?: view.closeView()
    }

    private fun listPath() {
        view.showProgress()

        listPath.execute(ListPathParameter(storage, mCurrentPath), object : UseCaseCallback<List<Path>> {
            override fun onSuccess(result: List<Path>) {
                view.setPathItems(result)
                view.hideProgress()
            }

            override fun onError(throwable: Throwable) {
                view.hideProgress()
                view.showError()
            }
        })
    }
}
