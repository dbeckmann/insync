package com.dbeckmann.sync.views.gallery

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.galleryadapter.GalleryGridLayoutManager
import kotlinx.android.synthetic.main.activity_gallery_new.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

/**
 * Created by daniel on 29.07.2017.
 */

class GalleryActivity : AppCompatActivity(), GalleryContract.View, ViewHolderOnClickListener {

    @Inject lateinit var presenter: GalleryPresenter

    private lateinit var mLayoutManager: GalleryGridLayoutManager
    private lateinit var mRecyclerViewAdapter: NewGalleryRecyclerViewAdapter
    private var mActionMode: ActionMode? = null

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mToolbar: Toolbar

    companion object {
        private const val TAG = "GalleryAct"
        const val KEY_STORAGE_ID = "keyStorageId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_new)

        Injector.injectActivityComponent(this).inject(this)

        mRecyclerView = rv_gallery
        mToolbar = toolbar

        mToolbar.title = "Gallery"
        setSupportActionBar(mToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }

        mRecyclerViewAdapter = NewGalleryRecyclerViewAdapter(presenter, this)
        mLayoutManager = GalleryGridLayoutManager(this, 4)
        mLayoutManager.setAdapter(mRecyclerViewAdapter)

        mRecyclerView.layoutManager = mLayoutManager
        mRecyclerView.adapter = mRecyclerViewAdapter
        (mRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        presenter.subscribe(this, GalleryState(intent.getLongExtra(KEY_STORAGE_ID, -1)))
    }

    override fun onResume() {
        Log.d(TAG, "onResume")
        super.onResume()
    }

    override fun onStop() {
        Log.d(TAG, "onStop")
        super.onStop()
        presenter.unsubscribe()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_gallery, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onClick(response: Any) {
        val pos = mRecyclerView.getChildAdapterPosition(response as View)
        presenter.onItemClick(pos)
    }

    override fun onLongClick(response: Any) {
        val pos = mRecyclerView.getChildAdapterPosition(response as View)
        presenter.onItemLongClick(pos)
    }

    override fun enableActionMode() {
        mActionMode = mToolbar.startActionMode(object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                val inflater = mode.menuInflater
                inflater.inflate(R.menu.menu_context_gallery, menu)

                presenter.onActionModeEnabled()

                return true
            }

            override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                when {
                    item.itemId == R.id.action_upload -> presenter.onBackupCollectionSelected()
                    item.itemId == R.id.action_blacklist -> presenter.onBlacklistCollectionSelected()
                }

                return true
            }

            override fun onDestroyActionMode(mode: ActionMode) {
                presenter.onActionModeDisabled()
            }
        })
    }

    override fun disableActionMode() {
        mActionMode?.also {
            it.finish()
        }
    }

    override fun showImageView(localFile: LocalFileEntity) {

    }

    override fun updateItem(localFile: LocalFileEntity) {
        //mRecyclerViewAdapter.updateLocalFile(localFile)
    }

    override fun getListAdapterUpdater(): ListAdapterUpdater {
        return mRecyclerViewAdapter
    }

    override fun showNumSelected(num: Int) {
        mActionMode?.title = num.toString()
    }
}
