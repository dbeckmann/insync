package com.dbeckmann.sync.views.main

import android.content.Context
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.localfile.FindFilesWithBackupForDate
import com.dbeckmann.domain.usecase.localfile.FindDatesWithoutBackup
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackupForDate
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.sync.data.Logger
import com.dbeckmann.sync.data.backup.BackupLocal
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.TileItem
import com.dbeckmann.sync.views.base.tiles.TilePresenter
import com.dbeckmann.sync.views.main.recyclerview.tile.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by daniel on 05.05.2018.
 */
class StorageTilePresenter @Inject constructor(
        private val findStorageById: FindStorageById,
        private val findFilesWithoutBackupForDate: FindFilesWithoutBackupForDate,
        private val findDatesWithoutBackup: FindDatesWithoutBackup,
        private val context: Context,
        private val findFilesWithBackupForDate: FindFilesWithBackupForDate

) : StorageContract.Presenter, TilePresenter() {

    private lateinit var view: StorageContract.View
    private var storage: StorageEntity? = null

    @Inject lateinit var storageInfoTile: StorageInfoTile
    @Inject lateinit var storageSpaceTile: StorageSpaceTile
    @Inject lateinit var imageStateTile: ImageStateTile
    @Inject lateinit var progressTile: BackupFileProgressTile
    @Inject lateinit var galleryTile: GalleryTile

    private var showsProgress = false

    companion object {
        const val TAG = "StorageTilePresenter"
    }

    override fun subscribe(view: StorageContract.View) {
        this.view = view
        initBackupFileListener()
    }

    override fun subscribe(view: StorageContract.View, storageId: Long) {
        subscribe(view)

        findStorageById.execute(storageId) {
            storage = it
            prepareTiles()
        }
    }

    override fun unsubscribe() {

    }

    private fun prepareTiles() {
        storage?.let {
            storageInfoTile.setStorage(it)
            storageSpaceTile.setStorage(it)
            imageStateTile.setStorage(it)

            tiles.clear()
            tiles.apply {
                add(storageInfoTile)
                add(storageSpaceTile)
                add(imageStateTile)
            }

            addGalleryTiles(it)

            view.getListAdapterUpdater().notifyDataSetChanged()
        }
    }

    private fun addGalleryTiles(storage: StorageEntity) {
        // find dates
        findDatesWithoutBackup.executeBlocking(FindDatesWithoutBackup.FindDatesWithoutBackupParameter(storage, 2)) {
            it.forEach { timestamp ->
                val galleryTile = GalleryTile(context, findFilesWithoutBackupForDate, findFilesWithBackupForDate)
                galleryTile.setStorage(storage)
                galleryTile.setDate(timestamp)

                tiles.add(galleryTile)
            }
        }
    }

    private fun initBackupFileListener() {
        BackupLocal.fileBackupStateSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Logger.d(TAG, "${it.localFile}")

                    if (it.state == BackupLocal.BackupState.PROGRESS) {
                        if (!showsProgress && it.storage.id == storage!!.id) {
                            progressTile.setInitial(it)
                            tiles.add(0, progressTile)
                            view.getListAdapterUpdater().notifyItemInserted(0)
                            showsProgress = true
                        }

                        if (showsProgress && it.max == it.processed) {
                            tiles.remove(progressTile)
                            view.getListAdapterUpdater().notifyItemRemoved(0)
                            showsProgress = false
                        }
                    }
                }
    }
}