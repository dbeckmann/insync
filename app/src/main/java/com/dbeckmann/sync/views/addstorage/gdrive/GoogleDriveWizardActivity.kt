package com.dbeckmann.sync.views.addstorage.gdrive

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dbeckmann.sync.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.services.drive.DriveScopes





class GoogleDriveWizardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.google_drive_wizard_activity)

        val signIn = buildGoogleSignInClient()
        GoogleSignIn.getLastSignedInAccount(this)

        //startActivityForResult(signIn.signInIntent, 1)

       /*startActivityForResult(AccountPicker.newChooseAccountIntent(null,
                null, arrayOf("com.google"), true, null, null, null, null),
                1)*/


        request()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 2) {
            request()
        }
    }

    private fun request() {
        /*
        val accounts = AccountManager.get(applicationContext)
                .getAccountsByType("com.google")

        var account: Account? = null
        accounts.forEach {
            if (it.name == "beckmannd@gmail.com") {
                account = it
            }
        }

        val signInAccount = GoogleSignInAccount.fromAccountAndScopes(account!!, Scope(DriveScopes.DRIVE_FILE))

        signInAccount?.also {
            val driveResourceClient = com.google.android.gms.drive.Drive.getDriveResourceClient(applicationContext, it)
            driveResourceClient.rootFolder.addOnCompleteListener {
                val foo = it.result
                println(it)
            }
        }

        val credential = GoogleAccountCredential.usingOAuth2(applicationContext, Collections.singletonList(DriveScopes.DRIVE_FILE))
                .setSelectedAccount(account)


        val foo = Drive.Builder(
                AndroidHttp.newCompatibleTransport(),
                JacksonFactory(),
                credential
        ).build()

        Single.fromCallable {
            println("credential: ${credential.token}")

            //foo.files().create(File().setName("SyncAppTest").setMimeType("application/vnd.google-apps.folder")).execute()

            //val bar = foo.files().get().setFields("id, parents").execute()
            val bar = foo.files().list().execute()
            bar.forEach {

            }
        }
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {

                        },
                        {
                            it.printStackTrace()
                            if (it is UserRecoverableAuthException) {
                                startActivityForResult(it.intent, 2)
                            }
                        })*/
    }

    private fun buildGoogleSignInClient(): GoogleSignInClient {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Scope(DriveScopes.DRIVE_FILE))
                .build()
        return GoogleSignIn.getClient(this, signInOptions)
    }
}