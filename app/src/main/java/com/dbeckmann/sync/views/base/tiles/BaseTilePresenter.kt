package com.dbeckmann.sync.views.base.tiles

interface BaseTilePresenter {
    fun getItemCount() : Int
    fun onBindViewHolder(tileItem: TileItem, pos: Int)
    fun getItemViewType(pos: Int) : Int
    fun getTileByItemViewType(itemViewType: Int) : Tile<Any>
}