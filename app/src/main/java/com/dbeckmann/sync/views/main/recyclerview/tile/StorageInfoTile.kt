package com.dbeckmann.sync.views.main.recyclerview.tile

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.Configuration
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import javax.inject.Inject

/**
 * Created by daniel on 09.05.2018.
 */
class StorageInfoTile @Inject constructor(
       private val configuration: Configuration
) : Tile<StorageInfoTile.StorageInfoTileData> {

    private var storage: StorageEntity? = null

    fun setStorage(storageEntity: StorageEntity) {
        storage = storageEntity
    }

    override fun getItemViewType(): Int {
        return R.id.storageInfoTileId
    }

    override fun getData(callback: UseCaseCallback<StorageInfoTileData>) {
        storage?.let {
            val data = StorageInfoTileData(
                        hostName = it.host,
                        displayName = it.displayName,
                        rootFolder = it.rootFolder,
                        homeWifiSsid = it.homeWifiSsid,
                        isAvailable = isStorageAvailable()
                    )

            callback.onSuccess(data)
        }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_storage_info, parent, false)
        return StorageInfoTileViewHolder(view)
    }

    private fun isStorageAvailable() : Boolean {
        return storage?.let {
            configuration.getWifiBssid() == it.homeWifiBssid
        } ?: false
    }

    data class StorageInfoTileData(
            val hostName: String,
            val displayName: String,
            val rootFolder: String?,
            val homeWifiSsid: String?,
            val isAvailable: Boolean
    )
}