package com.dbeckmann.sync.views.addstorage.sambadiscovery

import android.content.Intent
import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.mapping.StorageParcelable
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.commonbrowse.browse.CommonBrowseActivity
import kotlinx.android.synthetic.main.activity_samba_discovery.*
import java.util.*
import javax.inject.Inject

/**
 * Created by daniel on 20.07.2017.
 */

class SambaDiscoveryActivity : AppCompatActivity(), SambaDiscoveryContract.View, AuthDialogFragment.AuthDialogFragmentCallback {

    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var recyclerView: RecyclerView? = null

    private lateinit var recyclerViewAdapter: SambaDiscoveryRecyclerViewAdapter
    private lateinit var layoutManager: RecyclerView.LayoutManager

    @Inject lateinit var presenter: SambaDiscoveryPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_samba_discovery)
        Injector.injectActivityComponent(this).inject(this)

        swipeRefreshLayout = sl_discovery_swipe_refresh
        swipeRefreshLayout?.setOnRefreshListener { presenter.refreshDiscoveryList() }

        layoutManager = LinearLayoutManager(this)
        recyclerViewAdapter = SambaDiscoveryRecyclerViewAdapter(presenter, object : ViewHolderOnClickListener {
            override fun onClick(response: Any) {
                presenter.onHostSelected(recyclerViewAdapter.getItem(response as Int))
            }

            override fun onLongClick(response: Any) {
                // do nothing
            }
        })

        recyclerView = rv_discovery_server_list
        recyclerView?.adapter = recyclerViewAdapter
        recyclerView?.layoutManager = layoutManager

        if (savedInstanceState == null) {
            presenter.subscribe(this)
        } else {
            presenter.subscribe(this, readStateBundle(savedInstanceState))
        }


    }
    
    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")

        presenter.unsubscribe()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        writeStateBundle(outState, presenter.state)
    }

    private fun writeStateBundle(outState: Bundle?, state: SambaDiscoveryContract.State) {
        val hostParcelables = ArrayList<HostParcelable>()

        for (host in state.hosts) {
            hostParcelables.add(HostParcelable(host))
        }

        outState?.putParcelableArrayList(KEY_HOSTS, hostParcelables)
        outState?.putBoolean(KEY_IS_LOADING, state.isLoading)
    }

    private fun readStateBundle(savedInstanceState: Bundle): SambaDiscoveryContract.State {
        val hostParcelables = savedInstanceState.getParcelableArrayList<HostParcelable>(KEY_HOSTS)
        val hosts = mutableListOf<Host>()

        if (hostParcelables != null) {
            for (hostParcelable in hostParcelables) {
                hosts.add(hostParcelable.host)
            }
        }

        val isLoading = savedInstanceState.getBoolean(KEY_IS_LOADING)

        return SambaDiscoveryState(hosts, isLoading)
    }

    override fun setDiscoveryItems(hosts: List<Host>) {
        Log.d(TAG, "setDiscoveryItems")
        recyclerViewAdapter.setData(hosts)
    }

    override fun showProgress() {
        swipeRefreshLayout?.isRefreshing = true
        recyclerView?.alpha = 0.5f
    }

    override fun hideProgress() {
        swipeRefreshLayout?.isRefreshing = false
        recyclerView?.alpha = 1f
    }

    override fun showProgressPercent(percent: Int) {

    }

    override fun showAuthInput(host: Host) {
        AuthDialogFragment.newInstance(HostParcelable(host)).show(supportFragmentManager, AuthDialogFragment.TAG)
    }

    override fun startBrowse(storage: StorageEntity) {
        Log.d(TAG, "startBrowse")

        val intent = Intent(this, CommonBrowseActivity::class.java)
        intent.putExtra(CommonBrowseActivity.KEY_STORAGE, StorageParcelable.fromStorage(storage))

        startActivity(intent)
    }

    override fun showAddHostManual() {

    }

    override fun onCredentialsReceived(username: String, password: String) {
        Log.d(TAG, "onCredentialsReceived")
        presenter.onCredentialsReceived(username, password)
    }

    companion object {

        private val TAG = "SambaDiscoveryActivity"
        private val KEY_HOSTS = "keyMappedHosts"
        private val KEY_IS_LOADING = "keyIsLoading"
    }
}
