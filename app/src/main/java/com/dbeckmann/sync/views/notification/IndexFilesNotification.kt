package com.dbeckmann.sync.views.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat

import com.dbeckmann.sync.R

/**
 * Created by daniel on 23.08.2017.
 */

class IndexFilesNotification(
        private val context: Context
) {
    private val builder: NotificationCompat.Builder = NotificationCompat.Builder(context, CHANNEL_ID)
    private val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    companion object {
        const val CHANNEL_ID = "checkFilesChannel"
        const val NOTIFICATION_ID = 1
    }

    val notification: Notification
        get() = builder.build()

    fun create() {
        builder.setSmallIcon(R.drawable.ic_sync_white_24dp)
        builder.setContentTitle("Indexing Media Files")
        builder.setContentText("looking for new files to backup")
        builder.setProgress(0,0, true)
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        createNotificationChannel()

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun done() {
        builder.setSmallIcon(R.drawable.ic_sync_white_24dp)
        builder.setContentTitle("Indexing Media Files")
        builder.setContentText("completed")
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun error() {
        builder.setSmallIcon(R.drawable.ic_sync_white_24dp)
        builder.setContentTitle("Indexing Media Files")
        builder.setContentText("failed with an error")
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    fun cancel() {
        notificationManager.cancel(NOTIFICATION_ID)
    }

    @TargetApi(26)
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    context.getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            )

            channel.description = context.getString(R.string.notification_channel_description)

            notificationManager.createNotificationChannel(channel)
        }
    }
}
