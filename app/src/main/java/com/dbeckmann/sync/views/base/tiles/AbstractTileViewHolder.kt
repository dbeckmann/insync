package com.dbeckmann.sync.views.base.tiles

import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * Created by daniel on 05.05.2018.
 */
abstract class AbstractTileViewHolder(
        itemView: View
) : RecyclerView.ViewHolder(itemView), TileItem {

    protected var view: View = itemView

}