package com.dbeckmann.sync.views.main.recyclerview.tile

import android.content.Context
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.localfile.FindFilesWithBackupForDate
import com.dbeckmann.domain.usecase.localfile.FindFilesWithoutBackupForDate
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.rxexecutor.toSingle
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.dbeckmann.sync.views.base.tiles.Tile
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by daniel on 13.05.2018.
 */
class GalleryTile @Inject constructor(
        private val context: Context,
        private val findFilesWithoutBackupForDate: FindFilesWithoutBackupForDate,
        private val findFilesWithBackupForDate: FindFilesWithBackupForDate
) : Tile<GalleryTileData> {

    private var storage: StorageEntity? = null
    private var date: Long? = null

    fun setStorage(storageEntity: StorageEntity) {
        storage = storageEntity
    }

    fun setDate(date: Long) {
        this.date = date
    }

    override fun getItemViewType(): Int {
        return R.id.galleryTileId
    }

    override fun getData(callback: UseCaseCallback<GalleryTileData>) {
        storage?.let {

            var numStoredImages = 0
            var numUnstoredImages = 0
            var unstoredImages: MutableList<LocalFileEntity> = mutableListOf()

            var count = 0

            Single
                    .concat(
                            findFilesWithBackupForDate.toSingle(FindFilesWithBackupForDate.FindFilesWithBackupForDateParameter(it, date!!)).onErrorReturnItem(listOf()),
                            findFilesWithoutBackupForDate.toSingle(FindFilesWithoutBackupForDate.FindFilesWithoutBackupForDateParameter(it, date!!)).onErrorReturnItem(listOf()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                when (count) {
                                    0 -> numStoredImages = it.size
                                    1 -> {
                                        numUnstoredImages = it.size
                                        unstoredImages.addAll(it)
                                    }
                                }

                                count++
                            },
                            {
                                callback.onError(it)
                            },
                            {
                                callback.onSuccess(GalleryTileData(date!!, numUnstoredImages, numStoredImages, unstoredImages))
                            })
        }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_gallery_2, parent, false)
        return GalleryTileViewHolder(view)
    }

    private fun getThumbnail(file: LocalFileEntity): Bitmap? {
        return MediaStore.Images.Thumbnails.getThumbnail(
                context.contentResolver,
                file.internalId.toLong(),
                MediaStore.Images.Thumbnails.MINI_KIND,
                null
        )
    }
}

data class GalleryTileData(
        val date: Long,
        val numUnstoredImages: Int,
        val numStoredImages: Int,
        val files: List<LocalFileEntity>
)