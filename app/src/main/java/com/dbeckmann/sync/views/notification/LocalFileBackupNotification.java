package com.dbeckmann.sync.views.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;

import com.dbeckmann.sync.R;
import com.dbeckmann.sync.data.greendao.Storage;

/**
 * Created by daniel on 22.08.2017.
 */

public class LocalFileBackupNotification {
    public static final String CHANNEL_ID = "localFileBackupChannel";
    public static final int DEFAULT_NOTIFICATION_ID = 1;

    private Context mContext;
    private Storage mStorage;
    private int mNumFiles;
    private int mNotificationId;

    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    private int mCurrentValue = 0;

    public LocalFileBackupNotification(Context context, Storage storage, int numFiles, int notificationId) {
        mContext = context;
        mStorage = storage;
        mNumFiles = numFiles;
        mNotificationId = notificationId;

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext);
    }

    public void create() {
        mBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mBuilder.setSmallIcon(R.drawable.shape_circle);
        mBuilder.setContentTitle("Backup");
        mBuilder.setContentText("Uploading files to " + mStorage.getDisplayName());
        mBuilder.setProgress(0, 0, true);

        Intent intent = new Intent(mContext, NotificationActionActivity.class);
        //intent.setAction(NotificationActionActivity.Companion.getACTION_STOP_JOB_BY_ID());
        //intent.putExtra(NotificationActionActivity.Companion.getKEY_JOB_ID(), mNotificationId);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.addAction(R.drawable.ic_file_download_black_24dp, "cancel", pendingIntent);

        System.out.println("not_id: " + mNotificationId);

        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    public void increment() {
        mCurrentValue++;
        mBuilder.setProgress(mNumFiles, mCurrentValue, false);

        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    public void error() {
        mBuilder.setContentTitle("Backup failed");
        mBuilder.setContentText("Could not complete Upload to " + mStorage.getDisplayName());
        mBuilder.setProgress(0, 0, false);
        mBuilder.mActions.clear();

        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    public void completed() {
        mBuilder.setContentTitle("Backup");
        mBuilder.setContentText("Completed Upload to " + mStorage.getDisplayName());
        mBuilder.setProgress(0, 0, false);
        mBuilder.mActions.clear();

        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    public Notification getNotification() {
        return mBuilder.build();
    }

    public void cancel() {
        mNotificationManager.cancel(mNotificationId);
    }
}
