package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import com.dbeckmann.domain.model.Host;

import java.util.List;

/**
 * Created by daniel on 20.07.2017.
 */

public class SambaDiscoveryState implements SambaDiscoveryContract.State {
    private List<Host> mHosts;
    private boolean mIsLoading;

    public SambaDiscoveryState(List<Host> hosts, boolean isLoading) {
        mHosts = hosts;
        mIsLoading = isLoading;
    }

    @Override
    public List<Host> getHosts() {
        return mHosts;
    }

    @Override
    public boolean isLoading() {
        return mIsLoading;
    }
}
