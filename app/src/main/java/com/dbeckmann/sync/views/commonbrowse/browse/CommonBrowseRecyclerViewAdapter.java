package com.dbeckmann.sync.views.commonbrowse.browse;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dbeckmann.domain.model.Path;
import com.dbeckmann.sync.R;
import com.dbeckmann.sync.views.ViewHolderOnClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 26.08.2016.
 */
public class CommonBrowseRecyclerViewAdapter extends RecyclerView.Adapter<AbstractFolderListViewHolder> {

    private static final int VIEW_TYPE_FOLDER = 1;
    private static final int VIEW_TYPE_FILE = 2;

    private ArrayList<Path> mData = new ArrayList<>();
    private ViewHolderOnClickListener mOnClickListener;
    private CommonBrowseContract.Presenter mPresenter;

    public CommonBrowseRecyclerViewAdapter(CommonBrowseContract.Presenter presenter, ViewHolderOnClickListener onClickListener) {
        mPresenter = presenter;
        mOnClickListener = onClickListener;
    }

    @Override
    public AbstractFolderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_FOLDER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.folder_row, parent, false);
                return new FolderFolderListViewHolder(view, mOnClickListener);

            case VIEW_TYPE_FILE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_row, parent, false);
                return new FileFolderListViewHolder(view, mOnClickListener);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(AbstractFolderListViewHolder holder, int position) {
        holder.fillData(position, mData.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        Path path = mData.get(position);

        if (path.isDirectory()) {
            return VIEW_TYPE_FOLDER;
        }

        if (!path.isDirectory()) {
            return VIEW_TYPE_FILE;
        }

        return -1;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<Path> pathList) {
        mData = new ArrayList<>(pathList);

        Collections.sort(mData, new Comparator<Path>() {
            @Override
            public int compare(Path t1, Path t2) {
                return t1.getName().compareToIgnoreCase(t2.getName());
            }
        });

        notifyDataSetChanged();
    }

    public Path getItem(int position) {
        return mData.get(position);
    }
}
