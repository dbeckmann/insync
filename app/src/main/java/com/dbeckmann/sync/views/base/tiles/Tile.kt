package com.dbeckmann.sync.views.base.tiles

import android.view.ViewGroup
import com.dbeckmann.interactor.usecase.UseCaseCallback

/**
 * Created by daniel on 05.05.2018.
 */
interface Tile<out Result> {
    fun getItemViewType() : Int
    fun getData(callback: UseCaseCallback<Result>)
    fun createViewHolder(parent: ViewGroup) : AbstractTileViewHolder
}