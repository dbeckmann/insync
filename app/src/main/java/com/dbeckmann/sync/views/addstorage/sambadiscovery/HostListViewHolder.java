package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dbeckmann.domain.model.Host;
import com.dbeckmann.sync.R;
import com.dbeckmann.sync.views.ViewHolderOnClickListener;

/**
 * Created by daniel on 26.08.2016.
 */
public class HostListViewHolder extends RecyclerView.ViewHolder{

    private View container;
    private TextView hostName;
    private TextView ipAddress;
    private ImageView secureState;

    public HostListViewHolder(View itemView, final ViewHolderOnClickListener onClickListener) {
        super(itemView);

        container = itemView.findViewById(R.id.rl_host_row_container);
        hostName = (TextView) itemView.findViewById(R.id.tv_host_name);
        ipAddress = (TextView) itemView.findViewById(R.id.tv_ip_address);
        secureState = (ImageView) itemView.findViewById(R.id.iv_lock_state);

        container.setOnClickListener(view -> onClickListener.onClick(container.getTag()));
    }

    public void fillData(int position, Host host) {
        container.setTag(position);
        hostName.setText(host.getHostName());
        ipAddress.setText(host.getIpAddress());

        if (host.isSecured()) {
            secureState.setImageResource(R.drawable.ic_lock_black_24dp);
        } else {
            secureState.setImageResource(R.drawable.ic_lock_open_black_24dp);
        }
    }
}
