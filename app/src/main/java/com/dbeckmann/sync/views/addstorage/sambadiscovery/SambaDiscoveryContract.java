package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import com.dbeckmann.domain.model.Host;
import com.dbeckmann.domain.model.StorageEntity;
import com.dbeckmann.sync.views.BaseState;
import com.dbeckmann.sync.views.BaseStatefulPresenter;
import com.dbeckmann.sync.views.BaseView;

import java.util.List;

/**
 * Created by daniel on 20.07.2017.
 */

public class SambaDiscoveryContract {

    public interface View extends BaseView {

        void setDiscoveryItems(List<Host> hosts);
        void showProgress();
        void hideProgress();
        void showAuthInput(Host hosts);
        void startBrowse(StorageEntity storage);
        void showAddHostManual();
        void showProgressPercent(int percent);

    }

    public interface State extends BaseState {

        List<Host> getHosts();
        boolean isLoading();
    }

    public interface Presenter extends BaseStatefulPresenter<View, State> {

        void refreshDiscoveryList();
        void onHostSelected(Host host);
        void onCredentialsReceived(String userName, String password);
        void onAddHostManual();
    }

}
