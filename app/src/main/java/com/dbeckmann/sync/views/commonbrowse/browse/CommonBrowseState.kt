package com.dbeckmann.sync.views.commonbrowse.browse


import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity

/**
 * Created by daniel on 21.07.2017.
 */

class CommonBrowseState(override val currentPath: Path?, override val storage: StorageEntity) : CommonBrowseContract.State {
}
