package com.dbeckmann.sync.views.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R



class ConfirmBackupNotification(
       context: Context,
       private val notificationId: Int
) : NotificationBase(context) {

    companion object {
        const val NOTIFICATION_ID = 2
    }

    fun create(storage: StorageEntity, numFiles: Int) {
        builder.setSmallIcon(R.drawable.shape_circle)
        builder.setContentTitle("New files for storage ${storage.displayName}")
        builder.setContentText("You have $numFiles files without backup on your device")
        builder.setContentIntent(getTapAction())
        builder.setAutoCancel(true)

        storage.id?.also {
            builder.addAction(R.drawable.ic_file_upload_white_24dp, "Backup now", getBackupAction(it))
        }

        notificationManager.notify(notificationId, builder.build())
    }

    private fun getTapAction() : PendingIntent {
        val intent = Intent(context, NotificationBroadcastReceiver::class.java).apply {
            action = NotificationBroadcastReceiver.ACTION_OPEN_MAIN_APP
        }

        return PendingIntent.getBroadcast(context, 0, intent, 0)
    }

    private fun getBackupAction(storageId: Long) : PendingIntent {
        val intent = Intent(context, NotificationBroadcastReceiver::class.java).apply {
            action = NotificationBroadcastReceiver.ACTION_BACKUP_STORAGE
        }

        return PendingIntent.getBroadcast(context, 0, intent, 0)
    }

    fun dismiss() {
        notificationManager.cancel(notificationId)
    }
}