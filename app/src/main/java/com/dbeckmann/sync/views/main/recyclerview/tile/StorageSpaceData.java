package com.dbeckmann.sync.views.main.recyclerview.tile;

import com.dbeckmann.sync.data.greendao.Storage;

/**
 * Created by daniel on 16.07.2017.
 */

public class StorageSpaceData {
    private Storage mStorage;

    private long mMaxSpace;
    private long mUsedSpace;
    private long mUsedBackupSpace;
    private long mFreeSpace;

    private int mFreeSpacePercent;
    private int mUsedSpacePercent;
    private int mUsedBackupSpacePercent;

    public StorageSpaceData(Storage storage, long maxSpace, long freeSpace, long usedBackupSpace) {
        mStorage = storage;
        mMaxSpace = maxSpace;
        mFreeSpace = freeSpace;
        mUsedBackupSpace = usedBackupSpace;
        mUsedSpace = mMaxSpace - mFreeSpace;

        mFreeSpacePercent = Math.round(((float) mFreeSpace / (float) mMaxSpace) * 100);
        mUsedSpacePercent = Math.round(((float) mUsedSpace / (float) mMaxSpace) * 100);
        mUsedBackupSpacePercent = Math.round(((float) mUsedBackupSpace / (float) mMaxSpace) * 100);
    }

    public long getMaxSpace() {
        return mMaxSpace;
    }

    public long getUsedSpace() {
        return mUsedSpace;
    }

    public long getUsedBackupSpace() {
        return mUsedBackupSpace;
    }

    public long getFreeSpace() {
        return mFreeSpace;
    }

    public int getFreeSpacePercent() {
        return mFreeSpacePercent;
    }

    public int getUsedSpacePercent() {
        return mUsedSpacePercent;
    }

    public int getUsedBackupSpacePercent() {
        return mUsedBackupSpacePercent;
    }

    public Storage getStorage() {
        return mStorage;
    }

    @Override
    public String toString() {
        return  "max: " + mMaxSpace +
                " free: " + mFreeSpace +
                " used: " + mUsedSpace +
                " used bak: " + mUsedBackupSpace +
                " free percent: " + mFreeSpacePercent +
                " used percent: " + mUsedSpacePercent +
                " used bak percent: " + mUsedBackupSpacePercent;
    }
}
