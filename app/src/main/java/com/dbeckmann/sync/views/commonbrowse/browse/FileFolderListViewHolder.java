package com.dbeckmann.sync.views.commonbrowse.browse;

import android.view.View;
import android.widget.TextView;

import com.dbeckmann.domain.model.Path;
import com.dbeckmann.sync.R;
import com.dbeckmann.sync.views.ViewHolderOnClickListener;

/**
 * Created by daniel on 26.08.2016.
 */
public class FileFolderListViewHolder extends AbstractFolderListViewHolder {

    private View container;
    private TextView fileName;

    public FileFolderListViewHolder(View itemView, final ViewHolderOnClickListener onClickListener) {
        super(itemView, onClickListener);

        container = itemView.findViewById(R.id.rl_file_row_container);
        fileName = (TextView) itemView.findViewById(R.id.tv_file_name);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(container.getTag());
            }
        });
    }

    @Override
    public void fillData(int position, Path path) {
        container.setTag(position);
        fileName.setText(path.getName());
    }
}
