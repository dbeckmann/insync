package com.dbeckmann.sync.views.addstorage.finalizeaddstorage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import android.text.format.Formatter
import android.util.Log
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.greendao.StorageSetting
import com.dbeckmann.sync.data.mapping.PathMapping
import com.dbeckmann.sync.data.mapping.StorageParcelable
import com.dbeckmann.sync.injection.Injector
import kotlinx.android.synthetic.main.activity_finalize_add_storage.*
import javax.inject.Inject

/**
 * Created by daniel on 22.07.2017.
 */

class FinalizeAddStorageActivity : AppCompatActivity(), FinalizeAddStorageContract.View {

    private lateinit var storage: StorageEntity
    private lateinit var path: Path

    @Inject lateinit var mPresenter: FinalizeAddStoragePresenter

    private var mFinishSetupButton: Button? = null
    private var mHostName: TextView? = null
    private var mWifiSsid: TextView? = null
    private var mFreeStorage: TextView? = null
    private var mFullpath: TextView? = null
    private var mSwitchOnlyHomeWifi: SwitchCompat? = null
    private var mSwitchOnlyWifi: SwitchCompat? = null
    private var mSwitchCharging: SwitchCompat? = null
    private var mBackupModeRadioGroup: RadioGroup? = null

    companion object {
        private const val TAG = "FinalizeAct"
        const val KEY_STORAGE = "keyStorage"
        const val KEY_PATH = "keyPath"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finalize_add_storage)
        Injector.injectActivityComponent(this).inject(this)

        processIntent()

        mFinishSetupButton = b_finish_setup
        mHostName = tv_host_name
        mWifiSsid = tv_home_wifi_ssid
        mFreeStorage = tv_free_storage
        mFullpath = tv_fullpath
        mSwitchOnlyHomeWifi = sc_only_home_wifi
        mSwitchOnlyWifi = sc_only_wifi
        mSwitchCharging = sc_charging
        mBackupModeRadioGroup = rg_auto_backup_mode

        mFinishSetupButton?.setOnClickListener { v -> mPresenter.onFinalizeConfirmed() }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        if (savedInstanceState == null) {
            mPresenter.subscribe(this, FinalizeAddStorageState(storage, path))
        } else {
            mPresenter.subscribe(this, readStateBundle(savedInstanceState))
        }
    }

    override fun onStop() {
        super.onStop()
        mPresenter.unsubscribe()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        writeStateBundle(outState, mPresenter.state)
    }

    private fun writeStateBundle(outState: Bundle?, state: FinalizeAddStorageContract.State) {
        Log.d(TAG, "y:" + state.path.name)
        outState?.putParcelable(KEY_PATH, PathMapping.fromPath(state.path))
        outState?.putParcelable(KEY_STORAGE, StorageParcelable.fromStorage(storage))
    }

    private fun readStateBundle(savedInstanceState: Bundle): FinalizeAddStorageContract.State {
        val path = savedInstanceState.getParcelable<PathMapping>(KEY_PATH)
        val connector = savedInstanceState.getParcelable<StorageParcelable>(KEY_STORAGE)

        return FinalizeAddStorageState(connector, path)
    }

    private fun processIntent() {
        if (intent != null) {
            storage = intent.getParcelableExtra<StorageParcelable>(KEY_STORAGE)
            path = intent.getParcelableExtra<PathMapping>(KEY_PATH)
        }
    }

    override fun setHostName(hostName: String) {
        mHostName!!.text = hostName
    }

    override fun setStorageFreeSpace(freeSpace: Long) {
        mFreeStorage!!.text = Formatter.formatFileSize(this, freeSpace)
    }

    override fun setWifiSsid(ssid: String) {
        mWifiSsid!!.text = ssid
    }

    override fun setPathName(pathName: String) {
        mFullpath!!.text = pathName
    }

    override fun showStorageNotReachableError() {

    }

    override fun showGetStorageInfoProgress() {

    }

    override fun hideGetStorageInfoProgress() {

    }

    override fun showAlreadyInUseView() {

    }

    override fun showFinalizeError() {

    }

    override fun onFinalizeFinished() {

    }

    override fun needsHomeWifi(): Boolean {
        return mSwitchOnlyHomeWifi!!.isChecked
    }

    override fun needsWifi(): Boolean {
        return mSwitchOnlyWifi!!.isChecked
    }

    override fun needsCharging(): Boolean {
        return mSwitchCharging!!.isChecked
    }

    override fun backupMode(): Int {
        return when (mBackupModeRadioGroup!!.checkedRadioButtonId) {
            R.id.rb_backup_mode_always -> StorageSetting.AUTO_BACKUP_MODE_ALWAYS
            R.id.rb_backup_mode_notify -> StorageSetting.AUTO_BACKUP_MODE_NOTIFY
            R.id.rb_backup_mode_never -> StorageSetting.AUTO_BACKUP_MODE_NEVER
            else -> StorageSetting.AUTO_BACKUP_MODE_ALWAYS
        }
    }
}
