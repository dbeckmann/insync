package com.dbeckmann.sync.views.storagesetting

import androidx.preference.PreferenceDataStore
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSettingEntity
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.domain.usecase.storage.InsertOrUpdateStorage
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class StorageSettingPreferenceDataStore @Inject constructor(
        private val insertOrUpdateStorage: InsertOrUpdateStorage,
        private val findStorageById: FindStorageById
        ): PreferenceDataStore() {

    private lateinit var storage: StorageEntity

    companion object {
        const val KEY_ONLY_HOME_WIFI = "keyOnlyHomeWifi"
        const val KEY_ONLY_WIFI = "keyOnlyWifi"
        const val KEY_ONLY_CHARGING = "keyOnlyCharging"
        const val KEY_BACKUP_MODE = "keyBackupMode"
        const val KEY_ENABLED = "keyEnabled"
        const val KEY_DISPLAY_NAME = "keyDisplayName"

        val storageSettingChangedSubject: PublishSubject<StorageEntity> = PublishSubject.create()
    }

    fun setStorageId(storageId: Long) {
        findStorageById.executeBlocking(storageId) {
            setStorage(it)
        }
    }

    fun setStorage(storage: StorageEntity) {
        this.storage = storage

        //TODO remove setting creation here, it must exist at this point and be created at the wizard
        if (this.storage.setting == null) {
            val setting = StorageSettingEntity(
                    isOnlyHomeWifi = true,
                    isOnlyWifi = true,
                    isOnlyCharging = false,
                    backupMode = StorageSettingEntity.AUTO_BACKUP_MODE_NOTIFY)

            this.storage.setting = setting
        }
    }

    override fun getBoolean(key: String?, defValue: Boolean): Boolean {
        return when(key) {
            KEY_ONLY_HOME_WIFI  -> storage.setting?.isOnlyHomeWifi ?: defValue
            KEY_ONLY_WIFI       -> storage.setting?.isOnlyWifi ?: defValue
            KEY_ONLY_CHARGING   -> storage.setting?.isOnlyCharging ?: defValue
            KEY_ENABLED         -> storage.isEnabled
            else -> defValue
        }
    }

    override fun putBoolean(key: String?, value: Boolean) {
        when(key) {
            KEY_ONLY_HOME_WIFI  -> storage.setting?.isOnlyHomeWifi = value
            KEY_ONLY_WIFI       -> storage.setting?.isOnlyWifi = value
            KEY_ONLY_CHARGING   -> storage.setting?.isOnlyCharging = value
            KEY_ENABLED         -> storage.isEnabled = value
        }

        storeSettings()
    }

    override fun getInt(key: String?, defValue: Int): Int {
        return when(key) {
            KEY_BACKUP_MODE -> storage.setting?.backupMode ?: defValue
            else -> defValue
        }
    }

    override fun putInt(key: String?, value: Int) {
        when (key) {
            KEY_BACKUP_MODE -> storage.setting?.backupMode = value
        }

        storeSettings()
    }

    override fun getString(key: String?, defValue: String?): String? {
        return when(key) {
            KEY_DISPLAY_NAME -> storage.displayName
            KEY_BACKUP_MODE -> storage.setting?.backupMode.toString()
            else -> defValue
        }
    }

    override fun putString(key: String?, value: String?) {
        when(key) {
            KEY_DISPLAY_NAME -> storage.displayName = value ?: storage.host
            KEY_BACKUP_MODE -> storage.setting?.backupMode = value?.toInt() ?: 2
        }

        storeSettings()
    }

    private fun storeSettings() {
        insertOrUpdateStorage.execute(storage)
        propagetStorageSettingsChanged()
    }

    private fun propagetStorageSettingsChanged() {
        storageSettingChangedSubject.onNext(storage)
    }
}