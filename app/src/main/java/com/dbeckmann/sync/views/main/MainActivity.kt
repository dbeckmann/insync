package com.dbeckmann.sync.views.main

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.injection.activity.viewmodel.DaggerViewModelProviderFactory
import com.dbeckmann.sync.views.addstorage.AddStorageWizardActivity
import com.dbeckmann.sync.views.storagesetting.StorageSettingActivity
import kotlinx.android.synthetic.main.activity_main_new.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.navigation_drawer.*
import javax.inject.Inject

/**
 * Created by daniel on 07.07.2017.
 */

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject lateinit var viewModelProvider: DaggerViewModelProviderFactory
    private lateinit var presenter: MainPresenter

    private var mAppBarLayout: View? = null
    private var mMainLayout: RelativeLayout? = null
    private var toolbarMain: Toolbar? = null
    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.navigation_drawer)

        Injector.injectActivityComponent(this).inject(this)
        presenter = ViewModelProviders.of(this, viewModelProvider).get(MainPresenter::class.java)

        navigationView = nav_view
        drawerLayout = drawer_layout
        mAppBarLayout = nav_drawer_appbar_main
        mMainLayout = rl_main
        toolbarMain = toolbar

        toolbarMain?.let {
            it.title = ""
            setSupportActionBar(it)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }

        navigationView?.setNavigationItemSelectedListener {
            presenter.onStorageSelected(it.itemId)
            drawerLayout?.closeDrawers()
            true
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout?.openDrawer(GravityCompat.START)
            R.id.action_add_storage -> presenter.onAddStorage()
            R.id.action_storage_settings -> presenter.onStorageSettings()
        }

        return false
    }

    override fun setStorageList(storageList: List<StorageEntity>) {
        navigationView?.menu?.clear()

        val subMenu = navigationView?.menu?.addSubMenu("Storages")

        storageList.forEachIndexed { index, entry ->
            subMenu?.apply {
                add(0, index, 0, entry.displayName)
                        .setIcon(R.drawable.ic_samba)
            }
        }
    }

    override fun showSplashScreen() {
        mAppBarLayout?.visibility = View.INVISIBLE
        mMainLayout?.visibility = View.INVISIBLE
    }

    override fun hideSplashScreen() {
        mAppBarLayout?.visibility = View.VISIBLE
        mMainLayout?.visibility = View.VISIBLE
    }

    override fun showError() {

    }

    override fun initComplete() {
        presenter.subViewInitComplete()
    }

    override fun showAddStorage() {
        //StorageTypeSelectionDialogFragment.newInstance().show(supportFragmentManager, StorageTypeSelectionDialogFragment.TAG)
        val intent = Intent(this, AddStorageWizardActivity::class.java)
        startActivity(intent)
    }

    override fun showStorage(storage: StorageEntity) {
        storage.id?.let {
            val fragment = supportFragmentManager.findFragmentByTag(StorageFragment.TAG)
            if (fragment != null) {
                supportFragmentManager.beginTransaction().remove(fragment)
            }

            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_content, StorageFragment.newInstance(it), StorageFragment.TAG)
                    .commit()
        }
    }

    override fun showStorageSettings(storage: StorageEntity) {
        val intent = Intent(this, StorageSettingActivity::class.java)
        intent.putExtra(StorageSettingActivity.KEY_STORAGE_ID, storage.id)

        startActivity(intent)
    }
}
