package com.dbeckmann.sync.views.base.tiles

abstract class TilePresenter : BaseTilePresenter {

    protected val tiles: MutableList<Tile<Any>> = mutableListOf()

    override fun getItemCount(): Int = tiles.size
    override fun onBindViewHolder(tileItem: TileItem, pos: Int) = tileItem.setTile(tiles[pos])
    override fun getItemViewType(pos: Int): Int = tiles[pos].getItemViewType()
    override fun getTileByItemViewType(itemViewType: Int): Tile<Any> = tiles.first { it.getItemViewType() == itemViewType }
}