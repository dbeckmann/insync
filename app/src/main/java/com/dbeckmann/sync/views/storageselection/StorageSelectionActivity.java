package com.dbeckmann.sync.views.storageselection;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dbeckmann.sync.R;

import java.util.ArrayList;

/**
 * Created by daniel on 06.07.17.
 */

public class StorageSelectionActivity extends AppCompatActivity implements StorageSelectionView
{
	private StorageSelectionPresenter mPresenter;
	
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_storage_selection);
		
		mPresenter = new StorageSelection(this);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		mPresenter.updateRemoteDriverIdentifierList();
	}
	
	@Override
	public void setRemoteDriverIdentifierList(ArrayList<String> driverIdentifierList)
	{
		
	}
	
	@Override
	public void switchToDriverSetupView(String driverIdentifier)
	{
		
	}
}
