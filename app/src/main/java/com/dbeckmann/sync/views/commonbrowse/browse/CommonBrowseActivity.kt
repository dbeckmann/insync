package com.dbeckmann.sync.views.commonbrowse.browse

import android.content.Intent
import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.mapping.PathMapping
import com.dbeckmann.sync.data.mapping.StorageParcelable
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.addstorage.finalizeaddstorage.FinalizeAddStorageActivity
import com.dbeckmann.sync.views.commonbrowse.createfolder.CreateFolderDialogFragment
import kotlinx.android.synthetic.main.activity_common_browse.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

/**
 * Created by daniel on 06.07.17.
 */

class CommonBrowseActivity : AppCompatActivity(),
        CommonBrowseContract.View,
        ViewHolderOnClickListener,
        CreateFolderDialogFragment.CreateFolderDialogFragmentCallback,
        SetRootFolderConfirmationDialogFragment.SetRootFolderConfirmationDialogFragmentCallback {

    @Inject lateinit var presenter: CommonBrowsePresenter
    private lateinit var mStorage: StorageEntity

    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var mRecyclerView: RecyclerView? = null
    private var mToolbar: Toolbar? = null

    private var mFolderActionsEnabled = true

    private lateinit var mRecyclerViewAdapter: CommonBrowseRecyclerViewAdapter
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    companion object {
        const val TAG = "CommonBrowseAct"
        const val KEY_CONNECTOR_PARCELABLE = "keyConnectorParcelable"
        const val KEY_STORAGE = "keyStorage"
        const val KEY_CURRENT_PATH = "keyCurrentPath"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common_browse)
        Injector.injectActivityComponent(this).inject(this)

        processIntent()

        mSwipeRefreshLayout = sl_browser_swipe_refresh
        mRecyclerView = rv_browser_folder_list
        mToolbar = toolbar

        mSwipeRefreshLayout?.setOnRefreshListener { presenter.refreshPathList() }
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerViewAdapter = CommonBrowseRecyclerViewAdapter(presenter, this)
        mRecyclerView?.adapter = mRecyclerViewAdapter
        mRecyclerView?.layoutManager = mLayoutManager

        mToolbar?.title = ""
        setSupportActionBar(mToolbar)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        if (savedInstanceState == null) {
            presenter.subscribe(this, CommonBrowseState(null, mStorage))
        } else {
            presenter.subscribe(this, readStateBundle(savedInstanceState))
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        writeStateBundle(outState, presenter.state)
    }

    private fun writeStateBundle(outState: Bundle?, state: CommonBrowseContract.State) {
        outState?.putParcelable(KEY_CURRENT_PATH, PathMapping.fromPath(state.currentPath!!))
        outState?.putParcelable(KEY_STORAGE, StorageParcelable.fromStorage(state.storage))
    }

    private fun readStateBundle(savedInstanceState: Bundle): CommonBrowseContract.State {
        val currentPath = savedInstanceState.getParcelable<PathMapping>(KEY_CURRENT_PATH) as Path
        val storage = savedInstanceState.getParcelable<StorageParcelable>(KEY_STORAGE) as StorageEntity

        return CommonBrowseState(currentPath, storage)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_common_browse, menu)

        if (mFolderActionsEnabled) {
            menu.findItem(R.id.action_new_folder).isEnabled = true
            menu.findItem(R.id.action_select_folder).isEnabled = true

            menu.findItem(R.id.action_new_folder).icon.alpha = 255
            menu.findItem(R.id.action_select_folder).icon.alpha = 255

        } else {
            menu.findItem(R.id.action_new_folder).isEnabled = false
            menu.findItem(R.id.action_select_folder).isEnabled = false

            menu.findItem(R.id.action_new_folder).icon.alpha = 100
            menu.findItem(R.id.action_select_folder).icon.alpha = 100
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_new_folder) {
            presenter.onCreateFolderSelected()
        }

        if (item.itemId == R.id.action_select_folder) {
            presenter.onSetRootFolderSelected(null)
        }

        return false
    }

    private fun processIntent() {
        if (intent != null) {
            mStorage = intent.getParcelableExtra<StorageParcelable>(KEY_STORAGE)
        }
    }

    override fun onClick(response: Any) {
        presenter.setCurrentPath(mRecyclerViewAdapter.getItem(response as Int))
    }

    override fun onLongClick(response: Any) {
        Log.d(TAG, "onLongClick")
        presenter.onSetRootFolderSelected(mRecyclerViewAdapter.getItem(response as Int))
    }

    override fun onFolderCreated(path: Path) {
        presenter.refreshPathList()
    }

    override fun setPathItems(pathItems: List<Path>) {
        mRecyclerViewAdapter.setData(pathItems)
    }

    override fun showProgress() {
        mSwipeRefreshLayout?.isRefreshing = true
    }

    override fun hideProgress() {
        mSwipeRefreshLayout?.isRefreshing = false
    }

    override fun showAuthInput() {

    }

    override fun showNewFolderInput(storage: StorageEntity, path: Path) {
        CreateFolderDialogFragment.newInstance(StorageParcelable.fromStorage(storage), PathMapping.fromPath(path)).show(supportFragmentManager, CreateFolderDialogFragment.TAG)
    }

    override fun showFolderInUseView(path: Path) {
        Log.d(TAG, "showFolderInUseView")
    }

    override fun finalizeSetup(storage: StorageEntity, path: Path) {
        Log.d(TAG, "finalizeSetup")

        val intent = Intent(this, FinalizeAddStorageActivity::class.java)
        intent.putExtra(FinalizeAddStorageActivity.KEY_PATH, PathMapping.fromPath(path))
        intent.putExtra(FinalizeAddStorageActivity.KEY_STORAGE, StorageParcelable.fromStorage(storage))

        startActivity(intent)
    }

    override fun showSetRootFolderConfirmation(path: Path) {
        SetRootFolderConfirmationDialogFragment.newInstance(PathMapping.fromPath(path)).show(supportFragmentManager, SetRootFolderConfirmationDialogFragment.TAG)
    }

    override fun onRootFolderConfirmed(path: Path) {
        presenter.onSetRootFolderConfirmed(path)
    }

    override fun showError() {

    }

    override fun closeView() {
        finish()
    }

    override fun disableFolderActions() {
        mFolderActionsEnabled = false
        invalidateOptionsMenu()
    }

    override fun enableFolderActions() {
        mFolderActionsEnabled = true
        invalidateOptionsMenu()
    }
}
