package com.dbeckmann.sync.views.main

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.views.BasePresenter
import com.dbeckmann.sync.views.BaseView

/**
 * Created by daniel on 10.07.2017.
 */

class MainContract {

    interface View : BaseView {

        fun setStorageList(storageList: List<StorageEntity>)
        fun showSplashScreen()
        fun hideSplashScreen()
        fun showError()
        fun initComplete()
        fun showAddStorage()
        fun showStorage(storage: StorageEntity)
        fun showStorageSettings(storage: StorageEntity)

    }

    interface Presenter : BasePresenter<View> {

        fun refreshStorageList()
        fun subViewInitComplete()
        fun onAddStorage()
        fun onStorageSettings()
        fun setCurrentStorage(storage: StorageEntity)
        fun onStorageSelected(position: Int)

    }
}
