package com.dbeckmann.sync.views.commonbrowse.browse;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.dbeckmann.domain.model.Path;
import com.dbeckmann.sync.views.ViewHolderOnClickListener;

/**
 * Created by daniel on 26.08.2016.
 */
public abstract class AbstractFolderListViewHolder extends RecyclerView.ViewHolder {
    protected ViewHolderOnClickListener mOnClickListener;

    public AbstractFolderListViewHolder(View itemView, ViewHolderOnClickListener onClickListener) {
        super(itemView);
        mOnClickListener = onClickListener;
    }

    public abstract void fillData(int position, Path path);
}
