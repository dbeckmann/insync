package com.dbeckmann.sync.views.main.recyclerview.tile

import android.graphics.Color
import android.text.format.Formatter
import android.view.View
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.base.tiles.Tile
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import java.util.*

/**
 * Created by daniel on 07.05.2018.
 */
class StorageSpaceTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private var mPieChart: PieChart? = null
    private var mUsedBackupSpace: TextView? = null
    private var mUsedSpace: TextView? = null
    private var mFreeSpace: TextView? = null
    private var mTotalSpace: TextView? = null
    private var mUsedBackupContainer: View? = null
    private var mUsedContainer: View? = null

    init {
        mPieChart = itemView.findViewById(R.id.piechart)
        mUsedBackupSpace = itemView.findViewById(R.id.tv_num_used_backup)
        mUsedSpace = itemView.findViewById(R.id.tv_num_used)
        mFreeSpace = itemView.findViewById(R.id.tv_num_free)
        mTotalSpace = itemView.findViewById(R.id.tv_num_space_total)
        mUsedBackupContainer = itemView.findViewById(R.id.rl_used_backup_container)
        mUsedContainer = itemView.findViewById(R.id.rl_used_container)

        view = itemView

        initPieChart()
    }

    override fun setTile(tile: Tile<Any>) {
        tile.getData(object : UseCaseCallback<Any> {
            override fun onSuccess(result: Any) {
                result as StorageSpaceTileData

                renderPieChart(StorageSpaceData(null, result.maxSpace, result.freeSpace, result.usedSpace))
            }

            override fun onError(throwable: Throwable) {
                throwable.printStackTrace()
            }
        })
    }

    private fun initPieChart() {
        mPieChart?.apply {
            holeRadius = 60f
            setHoleColor(view.resources.getColor(android.R.color.transparent))
            setTransparentCircleColor(Color.WHITE)
            setTransparentCircleAlpha(110)
            transparentCircleRadius = 65f
            isClickable = false
            isRotationEnabled = false
            setTouchEnabled(false)
            legend.isEnabled = false
            description.isEnabled = false
            setDrawEntryLabels(true)
            setEntryLabelTextSize(10f)
        }
    }

    private fun renderPieChart(storageSpaceData: StorageSpaceData) {
        val pieEntries = ArrayList<PieEntry>()
        val usedBackupEntry = PieEntry(storageSpaceData.usedBackupSpacePercent.toFloat())

        if (storageSpaceData.usedBackupSpacePercent > 0) {
            usedBackupEntry.label = storageSpaceData.usedBackupSpacePercent.toString() + "%"
        }

        val usedSpacePercent = storageSpaceData.usedSpacePercent - storageSpaceData.usedBackupSpacePercent
        val usedSpaceEntry = PieEntry(usedSpacePercent.toFloat())

        if (usedSpacePercent > 0) {
            usedSpaceEntry.label = usedSpacePercent.toString() + "%"
        }

        val freeSpaceEntry = PieEntry(storageSpaceData.freeSpacePercent.toFloat())
        freeSpaceEntry.label = storageSpaceData.freeSpacePercent.toString() + "%"

        pieEntries.add(usedBackupEntry)
        pieEntries.add(usedSpaceEntry)
        pieEntries.add(freeSpaceEntry)

        val pieDataSet = PieDataSet(pieEntries, null)
        pieDataSet.setColors(view.resources.getColor(R.color.pieChartBackup), view.resources.getColor(R.color.pieChartUsed), view.resources.getColor(R.color.pieChartFree))
        pieDataSet.sliceSpace = 2f
        pieDataSet.setDrawValues(false)

        val pieData = PieData(pieDataSet)

        mPieChart?.data = pieData
        mPieChart?.animateXY(1000, 1000)
        mPieChart?.invalidate()

        // update legend
        val usedSpaceByOther = storageSpaceData.usedSpace - storageSpaceData.usedBackupSpace

        mTotalSpace?.setText(String.format(mTotalSpace?.getText().toString(), Formatter.formatFileSize(view.getContext(), storageSpaceData.maxSpace)))
        mUsedBackupSpace?.setText(String.format(mUsedBackupSpace?.getText().toString(), Formatter.formatFileSize(view.getContext(), storageSpaceData.usedBackupSpace)))
        mUsedSpace?.setText(String.format(mUsedSpace?.getText().toString(), Formatter.formatFileSize(view.getContext(), usedSpaceByOther)))
        mFreeSpace?.setText(String.format(mFreeSpace?.getText().toString(), Formatter.formatFileSize(view.getContext(), storageSpaceData.freeSpace)))

        if (storageSpaceData.usedBackupSpace == 0L) {
            mUsedBackupContainer?.setVisibility(View.GONE)
        }

        if (usedSpaceByOther == 0L) {
            mUsedContainer?.setVisibility(View.GONE)
        }
    }
}