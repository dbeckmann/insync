package com.dbeckmann.sync.views.addstorage

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dbeckmann.sync.R
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.addstorage.gdrive.GoogleDriveWizardActivity
import com.dbeckmann.sync.views.addstorage.sambadiscovery.SambaDiscoveryActivity
import com.dbeckmann.sync.views.base.tiles.BaseTileRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_addstorage_wizard.*
import javax.inject.Inject

class AddStorageWizardActivity : AppCompatActivity(), AddStorageWizardContract.View {
    @Inject lateinit var presenter: AddStorageWizard

    private lateinit var recyclerViewAdapter: BaseTileRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addstorage_wizard)
        Injector.injectActivityComponent(this).inject(this)

        layoutManager = LinearLayoutManager(this)
        recyclerViewAdapter = BaseTileRecyclerViewAdapter(presenter)

        recyclerView = wizardRecyclerView
        recyclerView.adapter = recyclerViewAdapter
        recyclerView.layoutManager = layoutManager
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    override fun showNasWizard() {
        startActivity(Intent(this, SambaDiscoveryActivity::class.java))
    }

    override fun showGoogleDriveWizard() {
        startActivity(Intent(this, GoogleDriveWizardActivity::class.java))
    }
}