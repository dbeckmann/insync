package com.dbeckmann.sync.views.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.dbeckmann.domain.usecase.backup.BackupForStorage
import com.dbeckmann.domain.usecase.storage.FindStorageById
import com.dbeckmann.sync.data.backup.StopBackupForAllStorages
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.injection.annotation.RunAsService
import com.dbeckmann.sync.views.main.MainActivity
import javax.inject.Inject

class NotificationBroadcastReceiver : BroadcastReceiver() {

    @Inject lateinit var findStorageById: FindStorageById
    @Inject @field:RunAsService lateinit var backupForStorage: BackupForStorage
    @Inject @field:RunAsService lateinit var stopBackupForAllStorages: StopBackupForAllStorages

    companion object {
        private val TAG = "NotificationBroadcastReceiver"
        const val ACTION_STOP_JOB_BY_ID = "actionStopJobById"
        const val ACTION_OPEN_MAIN_APP = "actionOpenMainApp"
        const val ACTION_BACKUP_STORAGE = "actionBackupStorage"
        const val ACTION_CANCEL_BACKUP = "actionCancelBackup"

        const val KEY_JOB_ID = "keyJobId"
        const val KEY_STORAGE_ID = "keyStorageId"
    }

    init {
        Injector.getApplicationComponent().inject(this)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.also {
            if (intent != null) {
                when (intent.action) {
                    ACTION_STOP_JOB_BY_ID -> {}
                    ACTION_OPEN_MAIN_APP -> { startMainActivity(it) }
                    ACTION_BACKUP_STORAGE -> { backupStorage(intent.getLongExtra(KEY_STORAGE_ID, -1), it)}
                    ACTION_CANCEL_BACKUP -> { cancelBackup(it) }
                }
            }
        }
    }

    private fun startMainActivity(context: Context) {
        context.startActivity(Intent(context, MainActivity::class.java))
    }

    private fun backupStorage(storageId: Long, context: Context) {
        ConfirmBackupNotification(context, storageId.toInt()).dismiss()

        if (storageId > -1) {
            findStorageById.execute(storageId) {
                backupForStorage.execute(it)
            }
        }
    }

    private fun cancelBackup(context: Context) {
        BackupFilesNotification(context).dismiss()
        stopBackupForAllStorages.execute()
    }
}