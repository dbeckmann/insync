package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import android.os.Parcel;
import android.os.Parcelable;

import com.dbeckmann.domain.model.Host;

/**
 * Created by daniel on 20.07.2017.
 */

public class HostParcelable extends Host implements Parcelable {

    public HostParcelable(Host host) {
        super(host.getIpAddress(), host.getHostName(), host.getMacAddress(), host.isSecured(), host.isReachable(), host.getUsername(), host.getAuth());
    }

    public Host getHost() {
        return new Host(getIpAddress(), getHostName(), getMacAddress(), isSecured(), isReachable(), getUsername(), getAuth());
    }

    public static final Creator<HostParcelable> CREATOR = new Creator<HostParcelable>() {
        @Override
        public HostParcelable createFromParcel(Parcel in) {
            return new HostParcelable(in);
        }

        @Override
        public HostParcelable[] newArray(int size) {
            return new HostParcelable[size];
        }
    };

    private HostParcelable(Parcel in) {
        super(
                in.readString(),
                in.readString(),
                in.readString(),
                in.readInt() != 0,
                in.readInt() != 0,
                in.readString(),
                in.readString()
        );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIpAddress());
        dest.writeString(getHostName());
        dest.writeString(getMacAddress());
        dest.writeInt(isSecured() ? 1 : 0);
        dest.writeInt(isReachable() ? 1 : 0);
        dest.writeString(getUsername());
        dest.writeString(getAuth());
    }
}
