package com.dbeckmann.sync.views.addstorage.sambadiscovery;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dbeckmann.sync.R;
import com.dbeckmann.sync.injection.Injector;
import com.dbeckmann.sync.networking.samba.mapping.MappedHost;

import javax.inject.Inject;

/**
 * Created by daniel on 04.09.2016.
 */
public class AuthDialogFragment extends DialogFragment implements AuthDialogContract.View{
    public static final String TAG = "AuthDialogFragment";

    private static final String KEY_MAPPED_HOST = "keyMappedHost";

    private AlertDialog mAlertDialog;
    private EditText mUsername;
    private EditText mPassword;
    private AuthDialogFragmentCallback mCallback;

    @Inject AuthDialogPresenter mPresenter;

    public static AuthDialogFragment newInstance(HostParcelable mappedHostParcelable) {
        AuthDialogFragment fragment = new AuthDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_MAPPED_HOST, mappedHostParcelable);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.injectActivityComponent(getActivity()).inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Authentication Required")
                .setPositiveButton("LOGIN", null)
                .setNegativeButton("CANCEL", null)
                .create();

        mAlertDialog.setOnShowListener(dialog -> {
            Button positiveButton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(v -> {

                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();

                mPresenter.authenticateUser(username, password);

            });
        });

        return mAlertDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_auth_dialog, container, false);
        mUsername = (EditText) v.findViewById(R.id.et_username);
        mPassword = (EditText) v.findViewById(R.id.et_password);

        mAlertDialog.setView(v);

        HostParcelable mappedHostParcelable = getArguments().getParcelable(KEY_MAPPED_HOST);
        MappedHost mappedHost = new MappedHost(
                mappedHostParcelable.getIpAddress(),
                mappedHostParcelable.getHostName(),
                mappedHostParcelable.isSecured(),
                mappedHostParcelable.isReachable()
        );

        mPresenter.subscribe(this, mappedHost);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Log.d(TAG, "onattach");

        if (activity instanceof AuthDialogFragmentCallback) {
            mCallback = (AuthDialogFragmentCallback) activity;
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showErrorAuthFailed() {

    }

    @Override
    public void onAuthenticateSuccess() {
        mCallback.onCredentialsReceived(mUsername.getText().toString(), mPassword.getText().toString());
        dismiss();
    }

    public interface AuthDialogFragmentCallback {
        void onCredentialsReceived(String username, String password);
    }
}
