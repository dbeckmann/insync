package com.dbeckmann.sync.views.main

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dbeckmann.sync.R
import com.dbeckmann.sync.injection.Injector
import com.dbeckmann.sync.views.ListAdapterUpdater
import com.dbeckmann.sync.views.galleryadapter.GalleryGridLayoutManager
import com.dbeckmann.sync.views.main.recyclerview.UniversalStorageRecyclerViewAdapter
import javax.inject.Inject

/**
 * Created by daniel on 09.07.2017.
 */
class StorageFragment : Fragment(), StorageContract.View {
    @Inject lateinit var presenter: StorageTilePresenter
    private lateinit var recyclerViewAdapter: UniversalStorageRecyclerViewAdapter
    private lateinit var layoutManager: GalleryGridLayoutManager
    private lateinit var recyclerView: RecyclerView

    companion object {
        const val TAG = "StorageFragment"
        private const val KEY_STORAGE_ID = "storageId"

        fun newInstance(storageId: Long): StorageFragment {
            val fragment = StorageFragment()
            val bundle = Bundle()
            bundle.putLong(KEY_STORAGE_ID, storageId)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Injector.getActivityComponent(activity!!).inject(this)

        recyclerViewAdapter = UniversalStorageRecyclerViewAdapter(presenter)
        layoutManager = GalleryGridLayoutManager(context!!, 2)
        layoutManager.setAdapter(recyclerViewAdapter)

        recyclerView.adapter = recyclerViewAdapter
        recyclerView.layoutManager = layoutManager
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_storage_recyclerview, container, false) as ViewGroup

        recyclerView = rootView.findViewById(R.id.rv_storage)

        return rootView
    }

    override fun onResume() {
        Log.d(TAG, "onResume")
        super.onResume()

        arguments?.let {
            presenter.subscribe(this, it.getLong(KEY_STORAGE_ID))
        }
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        Log.d(TAG, "onAttach")
    }

    override fun showSetupView() {

    }

    override fun showStorageSettingsView() {

    }

    override fun getListAdapterUpdater(): ListAdapterUpdater {
        return recyclerViewAdapter
    }

}
