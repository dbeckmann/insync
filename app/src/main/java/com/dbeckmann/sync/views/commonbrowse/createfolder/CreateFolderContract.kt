package com.dbeckmann.sync.views.commonbrowse.createfolder

import com.dbeckmann.domain.model.Path
import com.dbeckmann.sync.views.BasePresenter
import com.dbeckmann.sync.views.BaseView

/**
 * Created by daniel on 21.07.2017.
 */

class CreateFolderContract {

    interface View : BaseView {

        fun onCreateFolderSuccess(path: Path)
        fun showInvalidFoldernameError()
        fun showFolderCreateError()

    }

    interface Presenter : BasePresenter<View> {

        fun onCreateFolder(foldername: String)

    }

}
