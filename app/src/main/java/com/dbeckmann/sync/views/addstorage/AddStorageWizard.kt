package com.dbeckmann.sync.views.addstorage

import com.dbeckmann.sync.networking.gdrive.GoogleDriveFileOperation
import com.dbeckmann.sync.networking.samba.SambaFileOperation
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.base.tiles.TilePresenter
import javax.inject.Inject

class AddStorageWizard @Inject constructor(

): AddStorageWizardContract.Presenter, TilePresenter() {

    private lateinit var view: AddStorageWizardContract.View

    override fun subscribe(view: AddStorageWizardContract.View) {
        this.view = view

        prepareTiles()
    }

    override fun unsubscribe() {

    }

    private fun prepareTiles() {
        val clickListener = object: ViewHolderOnClickListener {
            override fun onClick(response: Any?) {
                when (response as String) {
                    SambaFileOperation.IDENTIFIER -> view.showNasWizard()
                    GoogleDriveFileOperation.IDENTIFIER -> view.showGoogleDriveWizard()
                }
            }

            override fun onLongClick(response: Any?) {
                //
            }
        }

        tiles.add(StorageSourceTile(SambaFileOperation.IDENTIFIER, clickListener))
        tiles.add(StorageSourceTile(GoogleDriveFileOperation.IDENTIFIER, clickListener))
    }

}