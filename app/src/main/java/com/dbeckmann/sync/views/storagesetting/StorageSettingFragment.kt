package com.dbeckmann.sync.views.storagesetting

import android.os.Bundle
import androidx.preference.PreferenceFragment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSettingEntity
import com.dbeckmann.sync.R
import com.dbeckmann.sync.injection.Injector
import javax.inject.Inject

class StorageSettingFragment : PreferenceFragment(), StorageSettingContract.View {
    @Inject lateinit var presenter: StorageSetting
    @Inject lateinit var dataStore: StorageSettingPreferenceDataStore

    private lateinit var host: Preference
    private lateinit var rootFolder: Preference
    private lateinit var displayName: EditTextPreference
    private lateinit var backupMode: ListPreference
    private lateinit var homeWlan: CheckBoxPreference
    private lateinit var onlyWlan: CheckBoxPreference

    companion object {
        const val KEY_STORAGE_ID = "keyStorageId"

        fun newInstance(storageId: Long) : StorageSettingFragment {
            val fragment = StorageSettingFragment()

            val bundle = Bundle()
            bundle.putLong(KEY_STORAGE_ID, storageId)

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        Injector.injectActivityComponent(activity as AppCompatActivity).inject(this)

        dataStore.setStorageId(arguments.getLong(KEY_STORAGE_ID))
        preferenceManager.preferenceDataStore = dataStore

        addPreferencesFromResource(R.xml.storage_setting)

        host = preferenceManager.findPreference("keyHostName")
        rootFolder = preferenceManager.findPreference("keyRootFolder")
        displayName = preferenceManager.findPreference("keyDisplayName") as EditTextPreference
        backupMode = preferenceManager.findPreference("keyBackupMode") as ListPreference
        homeWlan = preferenceManager.findPreference("keyOnlyHomeWifi") as CheckBoxPreference
        onlyWlan = preferenceManager.findPreference("keyOnlyWifi") as CheckBoxPreference

        setPreferenceChangeListeners()
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe(this, arguments.getLong(KEY_STORAGE_ID))
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.storage_setting_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.action_delete_storage -> {
                presenter.onDelete()
                true
            }
            else -> false
        }
    }

    override fun showDeleteConfirm(storage: StorageEntity) {
        AlertDialog.Builder(activity)
                .setTitle("Delete storage ${storage.displayName}")
                .setMessage("Confirm that you want to delete this storage. All local settings for this storage will be deleted. Already backuped files will stay preserved.")
                .setPositiveButton("delete") {_,_ ->
                    presenter.onDeleteConfirmed()
                }
                .setNegativeButton("cancel") {_,_ ->

                }
                .show()

    }
    override fun setOnlyHomeWifiEnabled(enabled: Boolean) {

    }

    override fun setOnlyWifiEnabled(enabled: Boolean) {

    }

    override fun setHostName(host: String) {
        this.host.summary = getString(R.string.storage_setting_host_name_summary, host)
    }

    override fun setRootFolder(folder: String) {
        rootFolder.summary = getString(R.string.storage_setting_root_folder_summary, folder)
    }

    override fun setDisplayName(displayName: String) {
        this.displayName.summary = getString(R.string.storage_stting_display_name_summary, displayName)
        this.displayName.text = displayName
    }

    override fun setBackupMode(mode: Int) {
        backupMode.value = mode.toString()

        val title = when(mode) {
            StorageSettingEntity.AUTO_BACKUP_MODE_NEVER -> getString(R.string.storage_setting_backup_mode_title, getString(R.string.backup_mode_never))
            StorageSettingEntity.AUTO_BACKUP_MODE_ALWAYS -> getString(R.string.storage_setting_backup_mode_title, getString(R.string.backup_mode_always))
            StorageSettingEntity.AUTO_BACKUP_MODE_NOTIFY -> getString(R.string.storage_setting_backup_mode_title, getString(R.string.backup_mode_notify))
            StorageSettingEntity.AUTO_BACKUP_MODE_NOTIFY_CONFIRM -> getString(R.string.storage_setting_backup_mode_title, getString(R.string.backup_mode_notify_confirm))
            else -> ""
        }

        backupMode.title = title
        backupMode.dialogTitle = getString(R.string.storage_setting_backup_mode_dialog_title)
    }

    override fun setHomeSsid(ssid: String) {
        homeWlan.summary = getString(R.string.storage_setting_only_home_summary, ssid)
    }

    override fun closeView() {
        activity.finish()
    }

    private fun setPreferenceChangeListeners() {
        displayName.setOnPreferenceChangeListener { pref, value ->
            setDisplayName(value as String)
            true
        }

        backupMode.setOnPreferenceChangeListener { pref, value ->
            setBackupMode((value as String).toInt())
            true
        }
    }
}