package com.dbeckmann.sync.views.commonbrowse.browse;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dbeckmann.domain.model.Path;
import com.dbeckmann.sync.R;
import com.dbeckmann.sync.views.ViewHolderOnClickListener;

/**
 * Created by daniel on 26.08.2016.
 */
public class FolderFolderListViewHolder extends AbstractFolderListViewHolder {

    private View container;
    private TextView folderName;
    private ImageView secureState;

    public FolderFolderListViewHolder(View itemView, final ViewHolderOnClickListener onClickListener) {
        super(itemView, onClickListener);

        container = itemView.findViewById(R.id.rl_folder_row_container);
        folderName = (TextView) itemView.findViewById(R.id.tv_folder_name);
        secureState = (ImageView) itemView.findViewById(R.id.iv_lock_state);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(container.getTag());
            }
        });

        container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onClickListener.onLongClick(container.getTag());
                return true;
            }
        });
    }

    @Override
    public void fillData(int position, Path path) {
        container.setTag(position);
        folderName.setText(path.getName());

        if (path.isSecured()) {
            secureState.setImageResource(R.drawable.ic_lock_black_24dp);
        } else {
            secureState.setImageResource(R.drawable.ic_lock_open_black_24dp);
        }
    }
}
