package com.dbeckmann.sync.views.addstorage.sambadiscovery

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dbeckmann.domain.model.Host
import com.dbeckmann.sync.R
import com.dbeckmann.sync.views.ViewHolderOnClickListener

import java.util.ArrayList
import java.util.Comparator

/**
 * Created by daniel on 26.08.2016.
 */
class SambaDiscoveryRecyclerViewAdapter(
        private val presenter: SambaDiscoveryContract.Presenter,
        private val onClickListener: ViewHolderOnClickListener)
    : RecyclerView.Adapter<HostListViewHolder>() {

    private var mData = ArrayList<Host>()

    companion object {
        private const val VIEW_TYPE_NAS = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HostListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.host_row, parent, false)
        return HostListViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(holder: HostListViewHolder, position: Int) {
        holder.fillData(position, mData[position])
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_NAS
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setData(hosts: List<Host>) {
        mData = ArrayList(hosts)

        mData.sortWith(Comparator { t1, t2 -> t1.hostName.compareTo(t2.hostName, ignoreCase = true) })

        notifyDataSetChanged()
    }

    fun getItem(position: Int): Host {
        return mData[position]
    }
}
