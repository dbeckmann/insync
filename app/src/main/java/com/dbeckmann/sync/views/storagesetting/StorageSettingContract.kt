package com.dbeckmann.sync.views.storagesetting

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.views.BasePresenter
import com.dbeckmann.sync.views.BaseView

class StorageSettingContract {

    interface View : BaseView {
        fun setOnlyHomeWifiEnabled(enabled: Boolean)
        fun setOnlyWifiEnabled(enabled: Boolean)
        fun setHostName(host: String)
        fun setRootFolder(folder: String)
        fun setDisplayName(displayName: String)
        fun setBackupMode(mode: Int)
        fun setHomeSsid(ssid: String)
        fun showDeleteConfirm(storage: StorageEntity)
        fun closeView()
    }

    interface Presenter : BasePresenter<View> {
        fun onDelete()
        fun onDeleteConfirmed()
    }

}