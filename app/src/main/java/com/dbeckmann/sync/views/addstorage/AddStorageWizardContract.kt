package com.dbeckmann.sync.views.addstorage

import com.dbeckmann.sync.views.BasePresenter
import com.dbeckmann.sync.views.BaseView
import com.dbeckmann.sync.views.base.tiles.BaseTilePresenter

class AddStorageWizardContract {

    interface View : BaseView {
        fun showNasWizard()
        fun showGoogleDriveWizard()
    }

    interface Presenter : BasePresenter<View>, BaseTilePresenter {

    }
}