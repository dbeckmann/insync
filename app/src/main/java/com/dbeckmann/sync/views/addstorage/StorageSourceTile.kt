package com.dbeckmann.sync.views.addstorage

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.networking.gdrive.GoogleDriveFileOperation
import com.dbeckmann.sync.networking.samba.SambaFileOperation
import com.dbeckmann.sync.views.ViewHolderOnClickListener
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.dbeckmann.sync.views.base.tiles.Tile

class StorageSourceTile(
        private val identifier: String,
        private val clickListener: ViewHolderOnClickListener
) : Tile<StorageSourceTile.StorageSourceTileData> {

    override fun getItemViewType(): Int {
        return R.id.storageSourceTileId
    }

    override fun getData(callback: UseCaseCallback<StorageSourceTileData>) {
        when (identifier) {
            SambaFileOperation.IDENTIFIER -> {
            callback.onSuccess(StorageSourceTileData(
                    identifier,
                    "Network attached Storage",
                    "Will look for NAS"
            ))}

            GoogleDriveFileOperation.IDENTIFIER -> {
                callback.onSuccess(StorageSourceTileData(
                        identifier,
                        "Google Drive",
                        "Will use Google Drive cloud"
                ))}

            else -> callback.onError(NoSuchElementException())
        }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.storage_source_tile, parent, false)
        return StorageSourceTileViewholder(view, clickListener)
    }

    data class StorageSourceTileData(
            val identifier: String,
            val title: String,
            val description: String
    )
}