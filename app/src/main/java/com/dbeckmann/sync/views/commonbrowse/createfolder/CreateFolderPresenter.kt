package com.dbeckmann.sync.views.commonbrowse.createfolder

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.usecase.fileoperation.CreateFolder
import com.dbeckmann.domain.usecase.fileoperation.CreateFolderParameter
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.data.Logger
import javax.inject.Inject

/**
 * Created by daniel on 21.07.2017.
 */

class CreateFolderPresenter @Inject constructor(
    private val createFolder: CreateFolder
) : CreateFolderContract.Presenter {

    private lateinit var view: CreateFolderContract.View
    private lateinit var storage: StorageEntity
    private lateinit var path: Path

    companion object {
        private const val TAG = "CreateFolderPrs"
    }

    override fun subscribe(view: CreateFolderContract.View) {
        this.view = view
    }

    fun subscribe(view: CreateFolderContract.View, storage: StorageEntity, path: Path) {
        subscribe(view)

        this.storage = storage
        this.path = path
    }

    override fun unsubscribe() {
    }

    override fun onCreateFolder(foldername: String) {
        Logger.d(TAG, "onCreateFolder $foldername")

        if (!isValidFoldername(foldername)) {
            view.showInvalidFoldernameError()
            return
        }

        createFolder.execute(CreateFolderParameter(storage, path, foldername), object : UseCaseCallback<Path> {
            override fun onSuccess(result: Path) {
                view.onCreateFolderSuccess(result)
            }

            override fun onError(throwable: Throwable) {
                view.showFolderCreateError()
            }
        })
    }

    private fun isValidFoldername(foldername: String): Boolean {
        if (foldername.isNullOrEmpty() || foldername.trim().isNullOrEmpty()) {
            return false
        } else {
            return true
        }
    }
}
