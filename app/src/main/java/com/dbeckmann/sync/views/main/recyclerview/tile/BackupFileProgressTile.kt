package com.dbeckmann.sync.views.main.recyclerview.tile

import android.content.Context
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.sync.R
import com.dbeckmann.sync.data.backup.BackupLocal
import com.dbeckmann.sync.views.base.tiles.AbstractTileViewHolder
import com.dbeckmann.sync.views.base.tiles.Tile
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by daniel on 12.05.2018.
 */
class BackupFileProgressTile @Inject constructor(
        private val context: Context
) : Tile<BackupFileProgressTileData> {

    private var initialData: BackupLocal.FileState? = null

    override fun getItemViewType(): Int {
        return R.id.backupFileProgressTileId
    }

    override fun getData(callback: UseCaseCallback<BackupFileProgressTileData>) {
        initialData?.let {
            callback.onSuccess(BackupFileProgressTileData(it.max, it.processed, it.localFile.filePath, getThumbnail(it.localFile)))
        }

        BackupLocal.fileBackupStateSubject
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.state == BackupLocal.BackupState.PROGRESS) {
                        callback.onSuccess(BackupFileProgressTileData(it.max, it.processed, it.localFile.filePath, getThumbnail(it.localFile)))
                    }
                }
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_storage_backup_progress, parent, false)
        return BackupFileProgressTileViewHolder(view)
    }

    fun setInitial(data: BackupLocal.FileState) {
        initialData = data
    }

    private fun getThumbnail(file: LocalFileEntity) : Bitmap? {
        return MediaStore.Images.Thumbnails.getThumbnail(
                context.contentResolver,
                file.internalId.toLong(),
                MediaStore.Images.Thumbnails.MICRO_KIND,
                null
        )
    }
}

data class BackupFileProgressTileData(
        val max: Int,
        val processed: Int,
        val filePath: String,
        val thumbnail: Bitmap?
)
