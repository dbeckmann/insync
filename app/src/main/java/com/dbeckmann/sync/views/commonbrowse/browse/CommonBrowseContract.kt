package com.dbeckmann.sync.views.commonbrowse.browse

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.sync.views.BaseState
import com.dbeckmann.sync.views.BaseStatefulPresenter
import com.dbeckmann.sync.views.BaseView

/**
 * Created by daniel on 20.07.2017.
 */

class CommonBrowseContract {

    interface View : BaseView {

        fun setPathItems(pathItems: List<Path>)
        fun showProgress()
        fun hideProgress()
        fun showAuthInput()
        fun showNewFolderInput(storage: StorageEntity, path: Path)
        fun showFolderInUseView(path: Path)
        fun showSetRootFolderConfirmation(path: Path)
        fun finalizeSetup(storage: StorageEntity, path: Path)
        fun showError()
        fun closeView()
        fun disableFolderActions()
        fun enableFolderActions()

    }

    interface State : BaseState {
        val storage: StorageEntity
        val currentPath: Path?
    }

    interface Presenter : BaseStatefulPresenter<CommonBrowseContract.View, State> {

        fun refreshPathList()
        fun setCurrentPath(path: Path)
        fun onSetRootFolderSelected(path: Path?)
        fun onSetRootFolderConfirmed(path: Path)
        fun onCreateFolderSelected()
        fun onBackPressed()

    }

}
