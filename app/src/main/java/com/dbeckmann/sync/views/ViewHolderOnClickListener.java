package com.dbeckmann.sync.views;

/**
 * Created by daniel on 26.08.2016.
 */
public interface ViewHolderOnClickListener {
    void onClick(Object response);
    void onLongClick(Object response);
}
