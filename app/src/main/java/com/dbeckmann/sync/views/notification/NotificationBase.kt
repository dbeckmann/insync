package com.dbeckmann.sync.views.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.dbeckmann.sync.R

open class NotificationBase(
        protected val context: Context
) {

    protected val builder: NotificationCompat.Builder = NotificationCompat.Builder(context, CHANNEL_ID)
    protected val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    val notification: Notification
        get() = builder.build()

    companion object {
        const val CHANNEL_ID = "backupFilesChannel"
        const val NOTIFICATION_ID = 10000
    }

    init {
        createNotificationChannel()
        builder.setVisibility(Notification.VISIBILITY_PUBLIC)
    }

    @TargetApi(26)
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    context.getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            )

            channel.description = context.getString(R.string.notification_channel_description)

            notificationManager.createNotificationChannel(channel)
        }
    }
}