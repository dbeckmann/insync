package com.dbeckmann.sync;

import android.app.Application;
import android.util.Log;

import com.dbeckmann.sync.data.crypto.CryptoManager;
import com.dbeckmann.sync.data.greendao.DaoMaster;
import com.dbeckmann.sync.data.greendao.DaoSession;
import com.dbeckmann.sync.injection.ApplicationComponent;
import com.dbeckmann.sync.injection.ApplicationModule;
import com.dbeckmann.sync.injection.DaggerApplicationComponent;
import com.facebook.stetho.Stetho;

import org.greenrobot.greendao.database.Database;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by daniel on 05.09.2016.
 */
public class AppController extends Application {
    private static final String TAG = "AppController";

    private ApplicationComponent mApplicationComponent;
    private ApplicationModule mApplicationModule;
    private static AppController mInstance;

    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        buildApplicationComponent(); // build this always first

        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.e(TAG, "undelivered error: " + throwable.toString());
            throwable.printStackTrace();
        });


        Stetho.initializeWithDefaults(this);

        initKeystore();
        initDatabase();

        getApplicationComponent().getTaskManager();
    }

    public static AppController getInstance() {
        return mInstance;
    }

    private void initKeystore() {
        CryptoManager cryptoManager = new CryptoManager();
        try {
            cryptoManager.generateKey(this);
        } catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | KeyStoreException | IOException e) {
            e.printStackTrace();
        }
    }

    private void initDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "sync-db");
        Database db = helper.getWritableDb();
        mDaoSession = new DaoMaster(db).newSession();
    }

    private void buildApplicationComponent() {
        mApplicationModule = new ApplicationModule(getInstance());

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(mApplicationModule)
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    public ApplicationModule getApplicationModule() {
        return mApplicationModule;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
