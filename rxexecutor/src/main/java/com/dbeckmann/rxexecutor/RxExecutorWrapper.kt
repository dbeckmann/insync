package com.dbeckmann.rxexecutor

import com.dbeckmann.interactor.usecase.UseCaseExecutor
import io.reactivex.Scheduler

interface RxExecutorWrapper : UseCaseExecutor {
         fun getSubscribeScheduler() : Scheduler
}