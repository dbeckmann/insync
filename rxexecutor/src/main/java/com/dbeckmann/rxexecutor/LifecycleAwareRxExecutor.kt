package com.dbeckmann.rxexecutor

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.Scheduler

/**
 * Created by daniel on 24.04.2018.
 */
class LifecycleAwareRxExecutor(
        subscribeScheduler: Scheduler,
        observeScheduler: Scheduler,
        identifier: String,
        lifecycle: Lifecycle
) : RxExecutor(subscribeScheduler, observeScheduler, identifier), LifecycleObserver {

    private var isDestroyed = false

    companion object {
        private const val TAG = "LifecycleAwareRxExecutor"
    }

    init {
        println("$TAG: created ${this.hashCode()}")
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        println("$TAG: onStop")
        stop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        println("$TAG: onDestroy")
        isDestroyed = true
    }

    override fun isDestroyed(): Boolean {
        return isDestroyed
    }
}