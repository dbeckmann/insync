package com.dbeckmann.rxexecutor

import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCase
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleEmitter

/**
 * Created by daniel on 03.04.2018.
 */

fun <Result, Params> UseCase<Result, Params>.toSingle(params: Params) : Single<Result> {
    return Single.create<Result> {e: SingleEmitter<Result> ->
        buildUseCase(params).execute(ExecutorCallback.invoke(
                {
                    e.onSuccess(it)
                },
                {
                    e.onError(it)
                })
        )
    }
            .subscribeOn(getSubscribeScheduler())
}

fun <Result> NoParamUseCase<Result>.toSingle() : Single<Result> {
    return toSingle(Unit)
}

fun <Result, Params> UseCase<Result, Params>.getSubscribeScheduler() : Scheduler {
    return when (useCaseExecutor) {
        is RxExecutor -> (useCaseExecutor as RxExecutor).subscribeScheduler
        is RxExecutorWrapper -> (useCaseExecutor as RxExecutorWrapper).getSubscribeScheduler()
        else -> throw IllegalStateException("no SubscribeScheduler defined")
    }
}
