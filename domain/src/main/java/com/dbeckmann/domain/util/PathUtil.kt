package com.dbeckmann.domain.util

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity

/**
 * Created by daniel on 08.05.2018.
 */
class PathUtil {
    companion object {
        fun rootFolderToPath(storage: StorageEntity, rootPath: Path) : Path {
            return Path(
                    name = storage.rootFolder!!,
                    fullpath = "/" + storage.rootFolder!!,
                    isDirectory = true
            )
        }

        fun rootFolderFromStorage(storage: StorageEntity) : Path {
            return Path(
                    name = storage.rootFolder!!,
                    fullpath = storage.rootFolder!!,
                    isDirectory = true
            )
        }
    }
}