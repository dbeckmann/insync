package com.dbeckmann.domain.util

/**
 * Created by daniel on 13.05.2018.
 */
class DateUtil {
    companion object {
        fun getDay(timestamp: Long) : Long {
            return timestamp - timestamp % (60 * 60 * 24)
        }
    }
}