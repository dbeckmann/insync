package com.dbeckmann.domain.usecase.storage

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 07.05.2018.
 */
class FindStorageById(
        private val repository: StorageRepository,
        executor: UseCaseExecutor) : UseCase<StorageEntity, Long>(executor) {

    override fun buildUseCase(params: Long): Executor<StorageEntity> {
        return repository.findById(params)
    }
}