package com.dbeckmann.domain.usecase.storage

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 03.05.2018.
 */
class InsertOrUpdateStorage(
        private val repository: StorageRepository,
        executor: UseCaseExecutor
) : UseCase<StorageEntity, StorageEntity>(executor) {

    override fun buildUseCase(params: StorageEntity): Executor<StorageEntity> {
        return repository.insertOrUpdate(params)
    }
}