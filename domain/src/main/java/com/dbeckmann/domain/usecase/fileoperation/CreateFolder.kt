package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 02.05.2018.
 */
class CreateFolder(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
        ) : UseCase<Path, CreateFolderParameter>(executor){

    override fun buildUseCase(params: CreateFolderParameter): Executor<Path> {
        return fileOperationFactory.fromStorage(params.storage).createFolder(params.path, params.folderName)
    }
}

class CreateFolderParameter(
        val storage: StorageEntity,
        val path: Path,
        val folderName: String
)