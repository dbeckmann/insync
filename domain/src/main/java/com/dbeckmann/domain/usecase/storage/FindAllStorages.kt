package com.dbeckmann.domain.usecase.storage

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 05.05.2018.
 */
class FindAllStorages(
        private val repository: StorageRepository,
        executor: UseCaseExecutor
) : NoParamUseCase<List<StorageEntity>>(executor) {

    override fun buildUseCase(params: Unit): Executor<List<StorageEntity>> {
        return repository.findAll()
    }
}