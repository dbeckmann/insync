package com.dbeckmann.domain.usecase.storage

import com.dbeckmann.domain.repository.StorageRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class DeleteStorage(
        private val repository: StorageRepository,
        useCaseExecutor: UseCaseExecutor
) : UseCase<Unit, Long>(useCaseExecutor) {
    override fun buildUseCase(params: Long): Executor<Unit> {
        return repository.delete(params)
    }
}