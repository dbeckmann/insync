package com.dbeckmann.domain.usecase.backup

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 11.05.2018.
 */
class BackupFiles(
        private val repository: BackupRepository,
        executor: UseCaseExecutor
) : UseCase<Unit, BackupFilesParameter>(executor) {

    override fun buildUseCase(params: BackupFilesParameter): Executor<Unit> {
        return repository.backupFiles(params.storage, params.files)
    }
}

data class BackupFilesParameter(
        val storage: StorageEntity,
        val files: List<LocalFileEntity>
)