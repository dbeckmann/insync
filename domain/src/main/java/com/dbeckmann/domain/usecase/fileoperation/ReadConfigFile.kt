package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 02.05.2018.
 */
class ReadConfigFile(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<ByteArray, ReadConfigFileParameter>(executor) {

    override fun buildUseCase(params: ReadConfigFileParameter): Executor<ByteArray> {
        return fileOperationFactory.fromStorage(params.storage).readConfigFile(params.path)
    }
}

class ReadConfigFileParameter(
        val storage: StorageEntity,
        val path: Path
)