package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 02.05.2018.
 */
class GetStorageSpaceFromStorageEntity(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<StorageSpace, StorageEntity>(executor) {

    override fun buildUseCase(params: StorageEntity): Executor<StorageSpace> {
        return fileOperationFactory.fromStorage(params).getStorageSpace(params)
    }
}