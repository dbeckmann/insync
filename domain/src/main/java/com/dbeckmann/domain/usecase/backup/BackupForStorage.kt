package com.dbeckmann.domain.usecase.backup

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class BackupForStorage(
        private val repository: BackupRepository,
        useCaseExecutor: UseCaseExecutor
) : UseCase<Unit, StorageEntity>(useCaseExecutor) {
    override fun buildUseCase(params: StorageEntity): Executor<Unit> {
        return repository.backupForStorage(params)
    }
}