package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 13.05.2018.
 */
class FindFilesWithoutBackupForDate(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<List<LocalFileEntity>, FindFilesWithoutBackupForDate.FindFilesWithoutBackupForDateParameter>(executor) {

    override fun buildUseCase(params: FindFilesWithoutBackupForDateParameter): Executor<List<LocalFileEntity>> {
        return repository.findNotBackupedFilesForDate(params.storage, params.date)
    }

    data class FindFilesWithoutBackupForDateParameter(
            val storage: StorageEntity,
            val date: Long
    )
}
