package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 23.05.2018.
 */
class DeleteLocalFile(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<Unit, LocalFileEntity>(executor) {

    override fun buildUseCase(params: LocalFileEntity): Executor<Unit> {
        return repository.delete(params)
    }
}