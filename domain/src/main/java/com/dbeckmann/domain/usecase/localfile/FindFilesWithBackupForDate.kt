package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 13.05.2018.
 */
class FindFilesWithBackupForDate(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<List<LocalFileEntity>, FindFilesWithBackupForDate.FindFilesWithBackupForDateParameter>(executor) {

    override fun buildUseCase(params: FindFilesWithBackupForDateParameter): Executor<List<LocalFileEntity>> {
        return repository.findBackupedFilesByDate(params.storage, params.date)
    }

    data class FindFilesWithBackupForDateParameter(
            val storage: StorageEntity,
            val date: Long
    )
}
