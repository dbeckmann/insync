package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 23.05.2018.
 */
class DeleteBackupForStorage(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<Unit, StorageEntity>(executor) {

    override fun buildUseCase(params: StorageEntity): Executor<Unit> {
        return repository.deleteBackupForStorage(params)
    }
}