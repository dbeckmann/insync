package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 11.05.2018.
 */
class FindAllLocalFile(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : NoParamUseCase<List<LocalFileEntity>>(executor) {

    override fun buildUseCase(params: Unit): Executor<List<LocalFileEntity>> {
        return repository.findAll()
    }
}