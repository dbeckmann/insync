package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 13.05.2018.
 */
class FindDatesWithoutBackup(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<List<Long>, FindDatesWithoutBackup.FindDatesWithoutBackupParameter>(executor) {

    override fun buildUseCase(params: FindDatesWithoutBackupParameter): Executor<List<Long>> {
        return repository.findNotBackupedDates(params.storage, params.limit)
    }

    data class FindDatesWithoutBackupParameter(
            val storage: StorageEntity,
            val limit: Int
    )
}
