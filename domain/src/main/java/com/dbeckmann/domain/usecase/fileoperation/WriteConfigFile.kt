package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 02.05.2018.
 */
class WriteConfigFile(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<Unit, WriteConfigFileParameter>(executor) {

    override fun buildUseCase(params: WriteConfigFileParameter): Executor<Unit> {
        return fileOperationFactory.fromStorage(params.storage).writeConfigFile(params.path, params.data)
    }
}

class WriteConfigFileParameter(
        val storage: StorageEntity,
        val path: Path,
        val data: ByteArray
)