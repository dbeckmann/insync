package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 01.05.2018.
 */
class ListPath(
    private val fileOperationFactory: StorageFileOperationFactory,
    private val executor: UseCaseExecutor
        ) : UseCase<List<Path>, ListPathParameter>(executor) {

    override fun buildUseCase(params: ListPathParameter): Executor<List<Path>> {
        return fileOperationFactory.fromStorage(params.storage).listPath(params.path)
    }
}

class ListPathParameter(
        val storage: StorageEntity,
        val path: Path
)