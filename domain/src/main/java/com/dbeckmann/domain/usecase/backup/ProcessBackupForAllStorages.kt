package com.dbeckmann.domain.usecase.backup

import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class ProcessBackupForAllStorages(
    val repository: BackupRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<Unit>(useCaseExecutor) {

    override fun buildUseCase(params: Unit): Executor<Unit> {
        return repository.processBackupForAllStorages()
    }
}