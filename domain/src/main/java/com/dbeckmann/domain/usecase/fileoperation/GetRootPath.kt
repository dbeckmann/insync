package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 01.05.2018.
 */
class GetRootPath(
    private val fileOperationFactory: StorageFileOperationFactory,
    private val executor: UseCaseExecutor
        ) : UseCase<Path, StorageEntity>(executor) {

    override fun buildUseCase(params: StorageEntity): Executor<Path> {
        return object : Executor<Path> {
            override fun execute(callback: ExecutorCallback<Path>) {
                callback.onSuccess(fileOperationFactory.fromStorage(params).getRootPath())
            }
        }
    }
}