package com.dbeckmann.domain.usecase.storage

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 11.05.2018.
 */
class AddBackup(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<LocalFileEntity, AddBackupParameter>(executor) {

    override fun buildUseCase(params: AddBackupParameter): Executor<LocalFileEntity> {
        return repository.addBackup(params.storage, params.localFile)
    }
}

data class AddBackupParameter(
        val storage: StorageEntity,
        val localFile: LocalFileEntity
)