package com.dbeckmann.domain.usecase.backup

import com.dbeckmann.domain.repository.BackupRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 10.05.2018.
 */
class IndexFiles(
        private var repository: BackupRepository,
        executor: UseCaseExecutor
) : NoParamUseCase<Unit>(executor) {

    override fun buildUseCase(params: Unit): Executor<Unit> {
        return repository.indexFiles()
    }
}