package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 10.05.2018.
 */
class FindLocalFileByInternalId (
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<LocalFileEntity, String>(executor) {
    override fun buildUseCase(params: String): Executor<LocalFileEntity> {
        return repository.findByInternalId(params)
    }
}