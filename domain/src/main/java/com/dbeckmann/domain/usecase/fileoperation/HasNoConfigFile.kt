package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 03.05.2018.
 */
class HasNoConfigFile(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<Unit, HasNoConfigFileParameter>(executor) {

    override fun buildUseCase(params: HasNoConfigFileParameter): Executor<Unit> {
        return fileOperationFactory.fromStorage(params.storage).hasNoConfigFile(params.path)
    }
}

class HasNoConfigFileParameter(
        val storage: StorageEntity,
        val path: Path
)