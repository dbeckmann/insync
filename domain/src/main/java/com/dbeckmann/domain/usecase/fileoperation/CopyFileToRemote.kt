package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import java.io.File

/**
 * Created by daniel on 11.05.2018.
 */
class CopyFileToRemote(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<Path, CopyFileToRemoteParameter>(executor) {

    override fun buildUseCase(params: CopyFileToRemoteParameter): Executor<Path> {
        return fileOperationFactory.fromStorage(params.storage).copyFileToRemote(params.targetPath, params.file)
    }
}

data class CopyFileToRemoteParameter(
        val storage: StorageEntity,
        val targetPath: Path,
        val file: File
)