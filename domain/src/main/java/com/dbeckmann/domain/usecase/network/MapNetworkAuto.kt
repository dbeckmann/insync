package com.dbeckmann.domain.usecase.network

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.repository.DiscoveryRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 22.04.2018.
 */
class MapNetworkAuto(
        private val repository: DiscoveryRepository,
        executor: UseCaseExecutor)
    : NoParamUseCase<List<Host>>(executor) {

    override fun buildUseCase(params: Unit): Executor<List<Host>> {
        return repository.mapNetwork()
    }
}