package com.dbeckmann.domain.usecase.network

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.repository.DiscoveryRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 25.04.2018.
 */
class AuthUserWithCredentials(
        private val repository: DiscoveryRepository,
        executor: UseCaseExecutor
        ) : UseCase<Unit, UserCredentials>(executor) {

    override fun buildUseCase(params: UserCredentials): Executor<Unit> {
        return repository.authenticate(params.host, params.username, params.auth)
    }
}

data class UserCredentials(
        val host: Host,
        val username: String,
        val auth: String?
)

