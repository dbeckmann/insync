package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.LocalFileFilter
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 22.05.2018.
 */
class FilterLocalFile(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<List<LocalFileEntity>, LocalFileFilter>(executor) {

    override fun buildUseCase(params: LocalFileFilter): Executor<List<LocalFileEntity>> {
        return repository.filter(params)
    }
}