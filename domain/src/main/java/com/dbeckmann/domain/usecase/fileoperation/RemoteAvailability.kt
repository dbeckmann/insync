package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class RemoteAvailability(
        private val fileOperationFactory: StorageFileOperationFactory,
        executor: UseCaseExecutor
) : UseCase<Unit, StorageEntity>(executor) {
    override fun buildUseCase(params: StorageEntity): Executor<Unit> {
        return fileOperationFactory.fromStorage(params).isAvailable()
    }
}