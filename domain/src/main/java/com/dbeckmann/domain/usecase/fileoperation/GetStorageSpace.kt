package com.dbeckmann.domain.usecase.fileoperation

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.domain.repository.StorageFileOperationFactory
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 02.05.2018.
 */
class GetStorageSpace(
    private val fileOperationFactory: StorageFileOperationFactory,
    executor: UseCaseExecutor
) : UseCase<StorageSpace, GetStorageSpaceParameter>(executor) {

    override fun buildUseCase(params: GetStorageSpaceParameter): Executor<StorageSpace> {
        return fileOperationFactory.fromStorage(params.storage).getStorageSpace(params.path)
    }
}

class GetStorageSpaceParameter(
        val storage: StorageEntity,
        val path: Path
)