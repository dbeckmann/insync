package com.dbeckmann.domain.usecase.localfile

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.LocalFileRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 11.05.2018.
 */
class FindByStorageId(
        private val repository: LocalFileRepository,
        executor: UseCaseExecutor
) : UseCase<List<LocalFileEntity>, StorageEntity>(executor) {

    override fun buildUseCase(params: StorageEntity): Executor<List<LocalFileEntity>> {
        return repository.findByStorageId(params)
    }
}