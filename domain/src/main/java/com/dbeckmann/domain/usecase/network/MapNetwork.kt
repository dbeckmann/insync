package com.dbeckmann.domain.usecase.network

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.repository.DiscoveryRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

/**
 * Created by daniel on 22.04.2018.
 */
class MapNetwork(
        private val repository: DiscoveryRepository,
        executor: UseCaseExecutor)
    : UseCase<List<Host>, String>(executor) {

    override fun buildUseCase(params: String): Executor<List<Host>> {
        return repository.mapNetwork(params)
    }
}