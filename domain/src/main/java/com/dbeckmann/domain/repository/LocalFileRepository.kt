package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.LocalFileFilter
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.Executor

/**
 * Created by daniel on 10.05.2018.
 */
interface LocalFileRepository {
    fun insertOrUpdate(localFile: LocalFileEntity) : Executor<LocalFileEntity>
    fun delete(localFile: LocalFileEntity) : Executor<Unit>
    fun findAll() : Executor<List<LocalFileEntity>>
    fun findByInternalId(internalId: String) : Executor<LocalFileEntity>
    fun findNotBackupedFiles(storage: StorageEntity) : Executor<List<LocalFileEntity>>
    fun findNotBackupedFiles(storage: StorageEntity, packageNames: List<String>) : Executor<List<LocalFileEntity>>
    fun findNotBackupedFilesForDate(storage: StorageEntity, date: Long) : Executor<List<LocalFileEntity>>
    fun findNotBackupedDates(storage: StorageEntity, limit: Int) : Executor<List<Long>>
    fun addBackup(storage: StorageEntity, localFile: LocalFileEntity) : Executor<LocalFileEntity>
    fun findAllSetBackupFlag(storage: StorageEntity) : Executor<List<LocalFileEntity>>
    fun findByStorageId(storage: StorageEntity) : Executor<List<LocalFileEntity>>
    fun findBackupedFilesByDate(storage: StorageEntity, date: Long) : Executor<List<LocalFileEntity>>
    fun filter(filter: LocalFileFilter) : Executor<List<LocalFileEntity>>
    fun deleteBackupForStorage(storage: StorageEntity) : Executor<Unit>
}