package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.repository.FileOperationRepository

/**
 * Created by daniel on 01.05.2018.
 */
interface StorageFileOperationFactory {
    fun fromStorage(storage: StorageEntity) : FileOperationRepository
}