package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.Executor

/**
 * Created by daniel on 01.05.2018.
 */
interface StorageRepository {
    fun insertOrUpdate(storage: StorageEntity) : Executor<StorageEntity>
    fun findById(id: Long) : Executor<StorageEntity>
    fun findAll() : Executor<List<StorageEntity>>
    fun delete(id: Long) : Executor<Unit>
}