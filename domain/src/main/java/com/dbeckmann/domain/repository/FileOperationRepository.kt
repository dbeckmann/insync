package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.Path
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.domain.model.StorageSpace
import com.dbeckmann.interactor.usecase.Executor
import java.io.File

/**
 * Created by daniel on 30.04.2018.
 */
interface FileOperationRepository {
    fun getRootPath() : Path
    fun getIdentifier() : String
    fun getStorageSpace(path: Path) : Executor<StorageSpace>
    fun getStorageSpace(storage: StorageEntity) : Executor<StorageSpace>
    fun listPath(path: Path) : Executor<List<Path>>
    fun createFolder(targetPath: Path, folderName: String) : Executor<Path>
    fun writeConfigFile(path: Path, data: ByteArray) : Executor<Unit>
    fun readConfigFile(path: Path) : Executor<ByteArray>
    fun hasNoConfigFile(path: Path) : Executor<Unit>
    fun copyFileToRemote(targetPath: Path, file: File) : Executor<Path>
    fun isAvailable() : Executor<Unit>
}