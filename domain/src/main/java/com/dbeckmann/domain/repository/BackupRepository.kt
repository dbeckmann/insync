package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.LocalFileEntity
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.Executor

/**
 * Created by daniel on 10.05.2018.
 */
interface BackupRepository {
    fun indexFiles(): Executor<Unit>
    fun findLocalFilesToBackup(storage: StorageEntity): Executor<List<LocalFileEntity>>
    fun findLocalFilesToBackup(storage: StorageEntity, packages: List<String>): Executor<List<LocalFileEntity>>
    fun backupFiles(storage: StorageEntity, files: List<LocalFileEntity>): Executor<Unit>
    fun backupForStorage(storage: StorageEntity): Executor<Unit>
    fun processBackupForAllStorages(): Executor<Unit>
    fun processBackupForStorage(storage: StorageEntity) : Executor<Unit>
}