package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.Host
import com.dbeckmann.domain.model.StorageEntity
import com.dbeckmann.interactor.usecase.Executor

/**
 * Created by daniel on 22.04.2018.
 */
interface DiscoveryRepository {
    fun mapNetwork(ipRange: String) : Executor<List<Host>>
    fun mapNetwork() : Executor<List<Host>>
    fun resolveHosts(storages: List<StorageEntity>) : Executor<List<Host>>
    fun authenticate(storage: StorageEntity) : Executor<Unit>
    fun authenticate(host: Host, username: String, auth: String?) : Executor<Unit>
}