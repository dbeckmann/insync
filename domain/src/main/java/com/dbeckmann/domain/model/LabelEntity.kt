package com.dbeckmann.domain.model

open class LabelEntity(
        val labels: List<String>
)