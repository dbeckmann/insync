package com.dbeckmann.domain.model

/**
 * Created by daniel on 30.04.2018.
 */
open class StorageSpace(
    val freeSpace: Long,
    val maxSpace: Long
)