package com.dbeckmann.domain.model

/**
 * Created by daniel on 22.04.2018.
 */
open class LocalFileEntity(
        var id: Long? = null,
        val internalId: String,
        val filePath: String,
        val type: Int = TYPE_UNKNOWN,
        val day: Long = 0,
        val addedAt: Long = 0,
        val modifiedAt: Long = 0,
        val mimeType: String = "",
        val hash: String? = null,
        val size: Long = 0,
        val appPackageName: String? = null,
        var neverBackup: Boolean = false,
        var storage: StorageEntity? = null,
        var hasBackup: Boolean = false,
        var labels: LabelEntity? = null
) {
    companion object {
        const val TYPE_UNKNOWN = -1
        const val TYPE_IMAGE = 1
        const val TYPE_VIDEO = 3
    }
}