package com.dbeckmann.domain.model

/**
 * Created by daniel on 22.05.2018.
 */
open class LocalFileFilter(
        val storage: StorageEntity,
        val isBackupable: Boolean?,
        val appPackageNames: List<String>?,
        val hasBackup: Boolean?,
        val mediaType: Int?
) {
}