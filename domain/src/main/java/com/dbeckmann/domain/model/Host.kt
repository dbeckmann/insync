package com.dbeckmann.domain.model

/**
 * Created by daniel on 22.04.2018.
 */
open class Host(
        val ipAddress: String,
        val hostName: String,
        val macAddress: String? = null,
        val isReachable: Boolean,
        var isSecured: Boolean,
        var username: String? = null,
        var auth: String? = null

)