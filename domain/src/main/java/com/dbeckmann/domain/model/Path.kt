package com.dbeckmann.domain.model

/**
 * Created by daniel on 22.04.2018.
 */
open class Path (
        val name: String,
        val fullpath: String,
        val parentPath: Path? = null,
        val isDirectory: Boolean = true,
        val isSecured: Boolean = true,
        val size: Long = 0,
        val modifiedAt: Long = 0,
        val createdAt: Long = 0,
        val isWritable: Boolean = true,
        val isReadable: Boolean = true
)