package com.dbeckmann.domain.model

open class StorageSettingEntity(
    var isOnlyHomeWifi: Boolean,
    var isOnlyWifi: Boolean,
    var isOnlyCharging: Boolean,
    var backupMode: Int
) {

    companion object {
        const val AUTO_BACKUP_MODE_NEVER = 0
        const val AUTO_BACKUP_MODE_ALWAYS = 1
        const val AUTO_BACKUP_MODE_NOTIFY_CONFIRM = 2
        const val AUTO_BACKUP_MODE_NOTIFY = 3
    }
}

