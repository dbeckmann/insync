package com.dbeckmann.domain.model

/**
 * Created by daniel on 22.04.2018.
 */
open class StorageEntity(
    var id: Long? = null,
    var displayName: String,
    var host: String,
    var connectionAddress: String,
    var hostIpAddress: String,
    var hostMacAddress: String? = null,
    var homeWifiSsid: String? = null,
    var homeWifiBssid: String? = null,
    var username: String? = null,
    var auth: String? = null,
    var rootFolder: String? = null,
    var cacheMaxSpace: Long = 0,
    var cacheUsedSpace: Long = 0,
    var cacheUsedBackupSpace: Long = 0,
    var lastBackupAt: Long = 0,
    var isEnabled: Boolean = true,
    var driverIdentifier: String,
    var setting: StorageSettingEntity? = null
)