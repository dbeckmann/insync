package com.dbeckmann.interactor.usecase

/**
 * Created by daniel on 25.02.2018.
 */
interface UseCaseExecutor {
    fun<Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>)
    fun<Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>)
    fun stop()
    fun isDestroyed() : Boolean
}