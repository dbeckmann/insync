package com.dbeckmann.interactor.usecase

/**
 * Created by daniel on 25.02.2018.
 */
abstract class NoParamUseCase<Result>(useCaseExecutor: UseCaseExecutor) : UseCase<Result, Unit>(useCaseExecutor) {

    fun execute(callback: UseCaseCallback<Result>) {
        execute(Unit, callback)
    }

    fun execute() {
        execute(Unit, UseCaseCallback(
                {
                    // do nothing
                },
                {
                    handleError(it)
                })
        )
    }

    fun executeBlocking(callback: UseCaseCallback<Result>) {
        executeBlocking(Unit, callback)
    }

    fun execute(onSuccess: (Result) -> Unit) {
        execute(UseCaseCallback({ onSuccess(it) }, { handleError(it) })
        )
    }

    fun execute(onSuccess: (Result) -> Unit, onError: (Throwable) -> Unit) {
        execute(UseCaseCallback({ onSuccess(it) }, { onError(it) }))
    }

    fun executeBlocking(onSuccess: (Result) -> Unit) {
        executeBlocking(UseCaseCallback({ onSuccess(it) }, { handleError(it) })
        )
    }

    fun executeBlocking(onSuccess: (Result) -> Unit, onError: (Throwable) -> Unit) {
        executeBlocking(UseCaseCallback({ onSuccess(it) }, { onError(it) })
        )
    }
}