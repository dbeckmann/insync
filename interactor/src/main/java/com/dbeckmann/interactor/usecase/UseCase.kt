package com.dbeckmann.interactor.usecase

/**
 * Created by daniel on 24.02.2018.
 */
abstract class UseCase<Result, in Params> (val useCaseExecutor: UseCaseExecutor) {

    abstract fun buildUseCase(params: Params) : Executor<Result>

    fun execute(params: Params, callback: UseCaseCallback<Result>) {
        useCaseExecutor.run(buildUseCase(params), callback)
    }

    fun execute(params: Params) {
        execute(params, UseCaseCallback(
                {
                    // do nothing
                },
                {
                    handleError(it)
                })
        )
    }

    fun executeBlocking(params: Params, callback: UseCaseCallback<Result>) {
        useCaseExecutor.runBlocking(buildUseCase(params), callback)
    }

    fun executeBlocking(params: Params) {
        executeBlocking(params, UseCaseCallback(
                {
                    // do nothing
                },
                {
                    handleError(it)
                })
        )
    }

    fun execute(params: Params, onSuccess: (Result) -> Unit, onError: (Throwable) -> Unit) {
        execute(params, UseCaseCallback({ onSuccess(it) }, { onError(it) })
        )
    }

    fun execute(params: Params, onSuccess: (Result) -> Unit) {
        execute(params, UseCaseCallback({ onSuccess(it) }, { handleError(it) })
        )
    }

    fun executeBlocking(params: Params, onSuccess: (Result) -> Unit, onError: (Throwable) -> Unit) {
        executeBlocking(params, UseCaseCallback({ onSuccess(it) }, { onError(it) }))
    }

    fun executeBlocking(params: Params, onSuccess: (Result) -> Unit) {
        executeBlocking(params, UseCaseCallback({ onSuccess(it) }, { handleError(it) }))
    }

    protected fun handleError(error: Throwable) {
        println("execute failed with $error")
    }
}

interface UseCaseCallback<in Result> {
    fun onSuccess(result: Result)
    fun onError(throwable: Throwable)

    companion object {
        operator fun <Result> invoke(success: (Result) -> Unit, error: (Throwable) -> Unit) = object : UseCaseCallback<Result> {
            override fun onSuccess(result: Result) = success(result)
            override fun onError(throwable: Throwable) = error(throwable)
        }
    }
}